<?php
//session_start();
?>
<!DOCTYPE html>
<html <?php language_attributes(); ?>>
<head>
    <meta charset="<?php bloginfo('charset'); ?>">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title><? wp_title() ?></title>
    <link rel="icon" type="image/x-icon" href="<? echo get_template_directory_uri() ?>/favicon.ico"/>
    <link href='https://fonts.googleapis.com/css?family=Open+Sans:400,300,600,700&subset=latin,cyrillic'
          rel='stylesheet'
          type='text/css'>


    <? wp_head() ?>
    <script>
        var ajaxurl = '<?php echo admin_url('admin-ajax.php'); ?>';
    </script>
    <meta name="globalsign-domain-verification" content="-vkOkI-9uHbtx-0dUTJCMico3SWSLUtDT9Nz4BQ2fD"/>
    <link rel="manifest" href="/manifest.json">
    <script src="https://cdn.onesignal.com/sdks/OneSignalSDK.js" async></script>
    <script>
        var OneSignal = window.OneSignal || [];
        OneSignal.push(["init", {
            appId: "6ae3973f-5674-45c3-8838-49da2e89351c",
            autoRegister: false,
            persistNotification: false,
            welcomeNotification: {
                disable: true
            },
            notifyButton: {
                enable: true, /* Required to use the notify button */
                size: 'medium', /* One of 'small', 'medium', or 'large' */
                theme: 'default', /* One of 'default' (red-white) or 'inverse" (white-red) */
                position: 'bottom-left', /* Either 'bottom-left' or 'bottom-right' */
                prenotify: true, /* Show an icon with 1 unread message for first-time site visitors */
                showCredit: false, /* Hide the OneSignal logo */
                text: {
                    'tip.state.unsubscribed': '<?=__('Subscribe to notifications', 'onePush')?>',
                    'tip.state.subscribed': '<?=__('Youre subscribed to notifications', 'onePush')?>',
                    'tip.state.blocked': '<?=__('Youve blocked notifications', 'onePush')?>',
                    'message.prenotify': '<?=__('Click to subscribe to notifications', 'onePush')?>',
                    'message.action.subscribed': '<?=__('Thanks for subscribing!', 'onePush')?>',
                    'message.action.resubscribed': '<?=__('Youre subscribed to notifications', 'onePush')?>',
                    'message.action.unsubscribed': '<?=__('You wont receive notifications again', 'onePush')?>',
                    'dialog.main.title': '<?=__('Manage Site Notifications', 'onePush')?>',
                    'dialog.main.button.subscribe': '<?=__('SUBSCRIBE', 'onePush')?>',
                    'dialog.main.button.unsubscribe': '<?=__('UNSUBSCRIBE', 'onePush')?>',
                    'dialog.blocked.title': '<?=__('Unblock Notifications', 'onePush')?>',
                    'dialog.blocked.message': '<?=__('Follow these instructions to allow notifications:', 'onePush')?>'
                }
            },
            promptOptions: {
                /* Change bold title, limited to 30 characters */
                siteName: '<?=__('OneSignal Documentation', 'onePush')?>',
                /* Subtitle, limited to 90 characters */
                actionMessage: '<?=__('Wed like to show you notifications for the latest news and updates.', 'onePush')?>',
                /* Example notification title */
                exampleNotificationTitle: '<?=__('Example notification', 'onePush')?>',
                /* Example notification message */
                exampleNotificationMessage: '<?=__('This is an example notification', 'onePush')?>',
                /* Text below example notification, limited to 50 characters */
                exampleNotificationCaption: '<?=__('You can unsubscribe anytime', 'onePush')?>',
                /* Accept button text, limited to 15 characters */
                acceptButtonText: '<?=__('ALLOW', 'onePush')?>',
                /* Cancel button text, limited to 15 characters */
                cancelButtonText: '<?=__('NO THANKS', 'onePush')?>'
            }
        }]);

    </script>
    <!--Fatchilli-->
    <!--Google GPT/ADM code -->
    <script type="text/javascript" async="async" src="https://securepubads.g.doubleclick.net/tag/js/gpt.js"></script>
    <script type="text/javascript">
        window.googletag = window.googletag || { cmd: [] };
        window.googletag.cmd.push(function () {
            window.googletag.pubads().enableSingleRequest();
        });
    </script>

    <!--Site config -->
    <script type="text/javascript" async="async" src="https://protagcdn.com/s/balticlivecam.com/site.js"></script>
    <script type="text/javascript">
        window.protag = window.protag || { cmd: [] };
        window.protag.config = { s:'balticlivecam.com', childADM: '96109881', l: 'FbM3ys2m' };
        window.protag.cmd.push(function () {
            window.protag.pageInit();
        });
    </script>

</head>
<body>
<?php echo do_shortcode("[pro_ad_display_adzone id=68738]"); ?>
<!-- Preloader -->
<!--<div class="block-for-preloader">
    <div class="preloader loading">
        <span class="slice"></span>
        <span class="slice"></span>
        <span class="slice"></span>
        <span class="slice"></span>
        <span class="slice"></span>
        <span class="slice"></span>
    </div>
</div>-->
<?php
/*global $wpdb;
$user_date = $wpdb->get_var('SELECT time_left FROM user_time WHERE user_id = "'.$_SESSION['user-id'].'"');
if($user_date){
    date_default_timezone_set('Europe/Riga');

    $left_date = date_create($user_date);
    $today = date_create(date('Y-m-d H:i:s'));
    $interval = date_diff($left_date, $today);

    $true_date = $interval->format('%R%');
    if($true_date == '-'){
        */?><!--
        <style>
            .paszone_container{
                display: none!important;
            }
        </style>
        --><?php
/*    }
}*/





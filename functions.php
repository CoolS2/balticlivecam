<?php

/* wp-contact-from-7 */
define('WPCF7_AUTOP', false);
define('BLC_DIR', untrailingslashit(dirname(__FILE__)));

// ---------------------------------------- ADDING STYLES AND SCRIPTS -------------------------------------------------------------------------------------

add_action('wp_enqueue_scripts', 'theme_enqueue_styles');
function theme_enqueue_styles()
{

    wp_deregister_script('wp-embed');




    wp_enqueue_style("themer_reset", get_template_directory_uri() . '/css/reset.css',array(), $ver = '0.1');
    wp_enqueue_style("theme_bootstrap_style", get_template_directory_uri() . '/css/bootstrap.min.css',array(), $ver = '3.3.6');
    wp_enqueue_style("theme_main_style1", get_template_directory_uri() . '/css/core.css',array(), $ver = '0.3');

    wp_enqueue_style("theme_masterslider", get_template_directory_uri() . '/js/masterslider/style/masterslider.css', $ver = '0.1');
    wp_enqueue_style("theme_masterslider1", get_template_directory_uri() . '/js/masterslider/skins/default/style.css', $ver = '0.1');
    wp_enqueue_style("theme_masterslider2", get_template_directory_uri() . '/css/ms-caro3d.css', $ver = '0.1');





    //FOOTER SCRIPTS
    wp_enqueue_script("theme_jquery", get_template_directory_uri() . '/js/jquery.min.js', '', $ver = '3', $in_footer = true);
    wp_enqueue_script("theme_bootstrap", get_template_directory_uri() . '/js/bootstrap.min.js', '', $ver = '3.3.7', $in_footer = true);
    wp_enqueue_script("theme_jquery_easing", get_template_directory_uri() . '/js/masterslider/jquery.easing.min.js', '', $ver = '4', $in_footer = true);
    wp_enqueue_script("theme_masterslider", get_template_directory_uri() . '/js/masterslider/masterslider.min.js', '', $ver = '1', $in_footer = true);
    wp_enqueue_script("theme_jquery_script", get_template_directory_uri() . '/js/script.js', '', $ver = '1', $in_footer = true);


}


/// ----------------------------------------------- MENUS AND WIDGETS --------------------------------------------------------------------------


function register_my_menu()
{
    register_nav_menu('header-menu', __('Header Menu'));
    register_nav_menu('footer-menu', __('Footer Menu'));


    register_sidebar(array(
        'name' => 'Social Widget',
        'id' => 'social_widget',
        'before_widget' => '',
        'after_widget' => '',
        'before_title' => '',
        'after_title' => '',
    ));


}

add_action('init', 'register_my_menu');


define('UPLOADS', 'images');

add_theme_support('post-thumbnails');
add_post_type_support('page', 'excerpt');


//  -------------------------------------------------- REMOVE Unnecessary Data From HEADER ------------------------------------------------------------

remove_action('wp_head', 'rsd_link');
remove_action('wp_head', 'wp_generator');
remove_action('wp_head', 'rsd_link');
remove_action('wp_head', 'wp_shortlink_wp_head');
remove_action('wp_head', 'wlwmanifest_link');
remove_action('wp_head', 'rest_output_link_wp_head');
remove_action('wp_head', 'wp_oembed_add_discovery_links');
remove_action('template_redirect', 'rest_output_link_header', 11, 0);

function disable_wp_emojicons()
{

    // all actions related to emojis
    remove_action('admin_print_styles', 'print_emoji_styles');
    remove_action('wp_head', 'print_emoji_detection_script', 7);
    remove_action('admin_print_scripts', 'print_emoji_detection_script');
    remove_action('wp_print_styles', 'print_emoji_styles');
    remove_filter('wp_mail', 'wp_staticize_emoji_for_email');
    remove_filter('the_content_feed', 'wp_staticize_emoji');
    remove_filter('comment_text_rss', 'wp_staticize_emoji');

    // filter to remove TinyMCE emojis
    //   add_filter( 'tiny_mce_plugins', 'disable_emojicons_tinymce' );
}




add_action('init', 'disable_wp_emojicons');


foreach (glob(BLC_DIR."/inc/*.php") as $filename) {
    include_once $filename;
}

  // SIDEBARs

function arphabet_widgets_init() {


    register_sidebar( array(
            'name'          => 'Side bar',
            'id'            => 'sidebar',
            'before_widget' => '<div class="widget_blog">',
            'after_widget'  => '</div>',
            'before_title'  => '<h2 class="rounded">',
            'after_title'   => '</h2>',
    ) );


}


add_action( 'widgets_init', 'arphabet_widgets_init' );
add_filter('widget_text', 'do_shortcode');


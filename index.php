<?php
function add_this_script_header()
{ ?>
    <link rel="stylesheet" href="<?php echo get_template_directory_uri() ?>/css/slick.css">
<?php }


add_action('wp_head', 'add_this_script_header', 20);


function add_this_script_footer()
{ ?>
    <script src="<?php echo get_template_directory_uri() ?>/js/slick.min.js"></script>

    <script>
        $(document).ready(function () {
            $('.slider-clients').slick({
                slidesToShow: 4,
                slidesToScroll: 1,
                autoplay: true,
                autoplaySpeed: 2000,
                responsive: [
                    {
                        breakpoint: 768,
                        settings: {
                            slidesToShow: 2
                        }
                    }
                ]
            });
        });
    </script>


<?php }

add_action('wp_footer', 'add_this_script_footer', 20);


get_header();

?>
    <div class="site main">
        <?php get_template_part("partials/main/header") ?>
        <aside class="left follow">
            <div class="left-ads ads-g-a">

                <?php echo do_shortcode("[pro_ad_display_adzone id=8284]"); //LEFT PANEL ?>
            </div>
        </aside>

        <aside class="right follow">
            <div class="right-ads ads-g-a">
                <?php echo do_shortcode("[pro_ad_display_adzone id=8285]"); //RIGHT PANEL ?>
            </div>
        </aside>
        <div id="content" class="site-content" role="main">
            <div class="container">
                <div style="margin-top:10px;" class="ads-g-a">
                    <?php echo do_shortcode("[pro_ad_display_adzone id=7738]"); ?>
                </div>
                <?php get_template_part("partials/main/top_cameras") ?>
                <div class="ads-g-a">
                    <?php echo do_shortcode("[pro_ad_display_adzone id=7662]"); ?>
                </div>
            </div>
            <div class="bacground-home">
                <div class="container">
                    <?php get_template_part("partials/main/new_cameras") ?>
                    <div class="ads-g-a">
                        <?php echo do_shortcode("[pro_ad_display_adzone id=7663]"); ?>
                    </div>
                </div>
            </div>
            <div class="container">
                <?php //get_template_part("partials/main/map") ?>
                    <!--    <div class="ads-g-a">
                    <?php /*echo do_shortcode("[pro_ad_display_adzone id=7664]"); */?>
                        </div>-->
            </div>

            <!--<div class="main_category " style="margin-top:50px;">
                <h2>BUSINESS SOLUTIONS</h2>
                <img src="https://cdn.balticlivecam.com/images/acb_logo.png" alt="acb" style="display: block;margin:0 auto;padding: 20px;width: 250px;">
                <div class="main_business">
                    <img src="<?/* echo get_template_directory_uri() */?>/images/big_business.png"/>
                    <div class="message">
                        <h4>make the view work for you</h4>

                    </div>
                </div>
            </div>-->

            <div class="bacground-home">
                <div class="container">
                <?php //get_template_part("partials/main/business") ?>
                <div class="ads-g-a">
                    <?php echo do_shortcode("[pro_ad_display_adzone id=7665]"); ?>
                </div>
                <?php get_template_part("partials/main/blog"); ?>

            </div>
            </div>
            <div class="container">
                <?php echo blc_partners_func(); ?>
            </div>
        </div>

        <?php get_template_part("partials/main/footer"); ?>
    </div>

<?php
get_footer();


<?php
$languages = apply_filters('wpml_active_languages', null, array('skip_missing' => '0'));
$my_current_lang = apply_filters('wpml_current_language', NULL);
if($_SESSION['user-id']){
    $login_style = 'logged';
}else{
    $login_style='';
}
?>
<header>
    <nav class="navbar navbar-static-top navbar-black">
        <div class="container">
            <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar"
                    aria-expanded="false" aria-controls="navbar">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
            <a class="navbar-brand" href="/"><img src="<?php echo get_template_directory_uri(); ?>/images/logo.png"/>
            </a>
            <div id="navbar" class="navbar-collapse collapse navbar-right">
                <div class="only_mobile">
                    <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar"
                            aria-expanded="false" aria-controls="navbar">
                        <span class="sr-only">Toggle navigation</span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button>
                </div>
                <div class="social-header only_desktop">

                    <ul class="white-icons">
                        <?php get_template_part("partials/sociallinks") ?>
                    </ul>

                </div>
                <div class="app_nav only_desktop">

                    <a href="<?php echo get_permalink(icl_object_id(4830, 'page', true)); ?>"><p>APP</p></a>
                </div>

                <!--<div class="login-button-header only_desktop <?/*=$login_style*/?>">
                    <a href="javascript:"><?/*=_e('Login')*/?></a>
                </div>-->

                <div class="change-lang">
                    <a href="javascript:">
                        <span><?php echo ICL_LANGUAGE_NAME; ?></span>
                    </a>

                    <ul class="dropdown-lang" style="display:none;">
                        <?php
                        foreach ($languages as $lang_key => $language) { ?>
                            <li><a <?php if ($my_current_lang == $language["language_code"]) {
                                    echo 'class="active"';
                                } ?>
                                    href="<?php echo $language["url"] ?>"><?php echo $language["native_name"] ?>  </a>
                            </li>
                            <?php
                        }
                        ?>
                    </ul>
                </div>

                <div class="search-block-form">
                    <form role="search" method="get" class="search-form" action="<?php echo home_url( '/' ); ?>">

                        <input type="search" class="search-field"
                               placeholder="<?=_e('Search...', 'blc')?>"
                               value="<?php echo get_search_query() ?>" name="s"/>
                        <button><i class="fa fa-search" aria-hidden="true"></i></button>

                    </form>
                </div>


                <? wp_nav_menu(array(
                        'menu' => 'primary',
                        'theme_location' => 'header-menu',
                        'depth' => 2,
                        'container' => 'ul',
                        'container_class' => 'nav navbar-nav',
                        'menu_class' => 'nav navbar-nav',
                        'fallback_cb' => 'wp_bootstrap_navwalker::fallback',
                        'walker' => new wp_bootstrap_navwalker())
                );

                ?>

                <div class="search-block-form only_mobile">
                    <form role="search" method="get" class="search-form" action="<?php echo home_url( '/' ); ?>">

                        <input type="search" class="search-field"
                               placeholder="<?=_e('Search...', 'blc')?>"
                               value="<?php echo get_search_query() ?>" name="s"/>
                        <button><i class="fa fa-search" aria-hidden="true"></i></button>

                    </form>
                </div>

                <div class="social-header only_mobile">

                    <ul class="white-icons">
                        <?php get_template_part("partials/sociallinks") ?>
                    </ul>

                </div>
                <div class="app_nav  only_mobile">
                    <a href="<?php echo get_permalink(icl_object_id(4830, 'page', true)); ?>"><p style="background: #fff; color: #565656;">APP</p></a>
                </div>
                <!--<div class="login-button-header only_mobile <?/*=$login_style*/?>">
                    <a href="javascript:"><?/*=_e('Login')*/?></a>
                </div>-->

                <? // wp_nav_menu(array('theme_location' => 'header-menu', 'container' => 'ul', 'container_class' => 'nav navbar-nav', 'menu_class' => 'nav navbar-nav')) ?>
            </div>
        </div>
    </nav>
</header>
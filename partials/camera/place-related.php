<div class="ms-caro3d-template ms-caro3d-wave">
    <div class="master-slider ms-skin-default main-sliders">
        <?php
        global $post;
        $parent_id = $post->post_parent;
        $curid = get_the_ID();
        // WP_Query arguments
        $args = array(
            'post_type' => array('page'),
            'post_status' => array('Publish'),
            'posts_per_page' => '9',
            'post_parent' => $parent_id,
            'post__not_in' => array($curid),
            'orderby' => 'rand',
            'meta_query' => array(
                array(
                    'key' => '_wp_page_template',
                    'value' => 'camera.php',
                    'compare' => '=',
                ),
            ),
        );
        // The Query
        $query = new WP_Query($args);
        $count_of_posts = $query->post_count;
        if ($count_of_posts == 9) {
            while ($query->have_posts()) : $query->the_post();
                include(locate_template('partials/camera/place-related-child.php', false, false));
            endwhile;
            wp_reset_postdata();
        } else {
            $id_s = array($curid);
            while ($query->have_posts()) : $query->the_post();
                array_push($id_s, $post->ID);
                include(locate_template('partials/camera/place-related-child.php', false, false));
            endwhile;

            wp_reset_postdata();

            $city_post = get_post($post->post_parent);
            $country_post = $city_post->post_parent;
            $counting = 9 - $count_of_posts;

            $args = array(
                'post_type' => array('page'),
                'post_status' => array('Publish'),
                'post_parent' => $country_post,
                'exclude' => $city_post->ID,
                'fields' => 'ids',
                'orderby' => 'rand',
            );

            $post_ids = get_posts(
                $args
            );
            $args = array(
                'post_type' => array('page'),
                'post_status' => array('Publish'),
                'posts_per_page' => $counting,
                'post_parent__in' => $post_ids,
                'exclude' => $parent_id,
                'post__not_in' => $id_s,
                'orderby' => 'rand',
                'meta_query' => array(
                    array(
                        'key' => '_wp_page_template',
                        'value' => 'camera.php',
                        'compare' => '=',
                    ),
                ),
            );

            $query = new WP_Query($args);
            $count_of_posts = $query->post_count;
            $counting -= $count_of_posts;
            while ($query->have_posts()) : $query->the_post(); ?>

                <?php
                include(locate_template('partials/camera/place-related-child.php', false, false));
            endwhile;
            wp_reset_postdata();


            $args = array(
                'post_type' => array('page'),
                'post_status' => array('Publish'),
                'posts_per_page' => $counting,
                'exclude' => $parent_id,
                'post__not_in' => $id_s,
                'orderby' => 'rand',
                'meta_query' => array(
                    array(
                        'key' => '_wp_page_template',
                        'value' => 'camera.php',
                        'compare' => '=',
                    ),
                ),
            );
            $query = new WP_Query($args);
            while ($query->have_posts()) : $query->the_post(); ?>

                <?php
                include(locate_template('partials/camera/place-related-child.php', false, false));
            endwhile;
            wp_reset_postdata();
        }
        ?>
    </div>
</div>
<?php $partner = get_post_meta($post->ID, 'blc_camera_partner', true);
$thumbnail = get_the_post_thumbnail_url($partner, "full");
$url = esc_url(get_post_meta($partner, "partner_url", true));
if($url){
?>


<a href="<? echo $url ?>" target="_blank" style="display: none !important;" class="fp-sponsor">
    <div class="fp-sponsor-minute"><img class="fp-sponsor-img" src="<? echo $thumbnail; ?>"></div>
</a>

<?php }

$partner2 = get_post_meta($post->ID, 'blc_camera_partner2', true);
$thumbnail2 = get_the_post_thumbnail_url($partner2, "full");
$url2 = esc_url(get_post_meta($partner2, "partner_url", true));
if($url2){
    ?>


    <a href="<? echo $url2 ?>" target="_blank" style="display: none !important;" class="fp-sponsor fp-sponsor-bottom">
        <div class="fp-sponsor-minute"><img class="fp-sponsor-img" src="<? echo $thumbnail2; ?>"></div>
    </a>

<?php }
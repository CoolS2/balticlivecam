<?php
$name = get_post_meta($post->ID, 'blc_camera_id', true);
$type = get_post_meta($post->ID, 'blc_camera_notour', true);
function player_init() {?>

        <script>
            $('#player_load').load(ajaxurl, data, function () {


                /*player.on('ready', function () {
                    var timeout = 5;
                    $.wait = function (callback, seconds) {
                        return window.setTimeout(callback, seconds * 1000);
                    };
                    $.wait(function () {
                        $('.controls').show()
                    }, timeout);
                    $.wait(function () {
                        $('.recall-button').hide()
                    }, timeout);
                });*/


                /*$(".close-button").on('click',
                    function () {
                        $('#ad').hide();
                        $('.recall-button').show();
                    });
                $(".recall-button").on('click',
                    function () {
                        $('.recall-button').hide();
                        $('#ad').show();
                    });*/
            });

            /*function sendStatistic(param, name){
                $.ajax({
                    url: ajaxurl,
                    method: "POST",
                    data: {action:'send_statistics', param: param, name: name},
                    dataType: "html",
                    success: function () {}
                });

                ga('send', {
                    hitType: 'event',
                    eventCategory: 'Ads-video-statistic',
                    eventAction: param,
                    eventLabel: name
                });
            }*/

        </script>
    <script src="<?php echo get_template_directory_uri() ?>/videojs/video-options.js"></script>
    <script src="<?php echo get_template_directory_uri() ?>/videojs/v7.4.2/video.min.js"></script>
    <script src="<?php echo get_template_directory_uri() ?>/videojs/plugins/videojs-contrib-hls/videojs-contrib-hls.min.js"></script>
    <script src="//imasdk.googleapis.com/js/sdkloader/ima3.js"></script>
    <script src="<?php echo get_template_directory_uri() ?>/videojs/plugins/videojs-contrib-ads/videojs-contrib-ads.min.js"></script>
    <script src="<?php echo get_template_directory_uri() ?>/videojs/plugins/videojs-ima/videojs.ima.js"></script>

<?php
}
add_action('wp_footer', 'player_init', 20);
?>

        <?php
            $poster = $name;
            if(!$poster){
                $poster = get_the_post_thumbnail_url(get_the_ID(),'full');
            }else{
                $poster = 'https://thumbs.balticlivecam.com/'.$poster.'.jpg';
            }
        ?>
    <div class="video">

        <?php
            if(get_post_meta($post->ID, 'camera_overlay', true) != ''){
                echo '<img class="camera-overlay" src="'.get_post_meta($post->ID, 'camera_overlay', true).'" alt="camera overlay">';
            }
        ?>

        <?php if(get_post_meta($post->ID, 'blc_camera_notour', true) != 3): ?>
        <video playsInline id="my-video" class="video-js vjs-16-9 vjs-fluid blc-js" controls poster="<?php echo $poster ?>"></video>
        <?php else: ?>
            <?php echo get_post_meta($post->ID, 'blc_iframe_code', true); ?>
        <?php endif; ?>

        <a target="_blank"
           style="display: none;"
           href="<?php echo get_permalink($post->ID); ?>/" class="fp-logo"><img
                src="<?php echo get_template_directory_uri(); ?>/images/logo.png"></a>


        <?php
        get_template_part("partials/camera/place", 'partner');
        ?>
        <?php
        get_template_part("partials/camera/place", 'lefttop');
        ?>
        <?php
        //get_template_part("partials/camera/place", 'small-video');
        ?>
        <?php
        global $embed;

        if ($embed == true) {
            get_template_part("partials/camera/place", 'ads-embed');
        } else {
            //if(!get_post_meta($post->ID, 'blc_ads_toggle', true)){
                get_template_part("partials/camera/place", 'ads');
            //}
        }
        $ads = $_GET['ads'];
        if($ads != 'false'){
            $ads = 'true';
        }
        ?>

    </div>




    <div id="player_load">
    </div>


    <script>
        var player;

        var data = {
            action: 'auth_token',
            id: <?php echo $post->ID;?>,
            embed:<?php echo $embed == true ? "1" : "0"; ?>,
            main_referer: document.referrer
        };

        jQuery(function ($) {
            var videoType = '<?php echo get_post_meta(get_the_ID(), 'blc_camera_notour', true); ?>';

            setTimeout(function() {
                var isAdsBlock = getComputedStyle(document.getElementById("detect-ads"))["display"] === "none";

                if ( isAdsBlock ) {
                    if(videoType === "2") {
                        var streamText = '<?php _e("Please, deactivate Adblock to watch the stream and press F5 to refresh the page.", "blc") ?>'
                        $('.video').html('<div class="stream-error-message">' + streamText + '</div>');
                    }
                }
            }, 5000);

        })

    </script>


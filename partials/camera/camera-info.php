<?php
function add_this_weather_footer()
{
    global $weather_id;
    ?>

    <script>
        var data_weather = {
            action: 'weather',
            id: <?php echo $weather_id;?>
        };
        $('#weather').load(ajaxurl, data_weather, function (response) {

        });
    </script>
<?php }

add_action('wp_footer', 'add_this_weather_footer', 20);
?>
<div class="info-camera">
    <div class="name-camera">
        <h1><?php the_title(); ?> </h1>
        <span>
                        <?php echo $city_post->post_title; ?> , <?php echo $country_post->post_title; ?>
                    </span>
    </div>



    <div id="weather">
    </div>
    <div class="open-weather-block">
        <button class="open-weather">Full weather</button>
    </div>

</div>
<?php
$actual_link = "http://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]";
global $post;
$id = $post->ID;

$image_url = "https://thumbs.balticlivecam.com/" . get_post_meta($post->ID, 'blc_camera_id', true) . "_sm.png";
$type = get_post_meta($post->ID, "blc_camera_notour", true);
if ($type == 2) {
    $image_url = get_post_meta($post->ID, 'blc_camera_id', true);
}
?>
<?php
function add_this_share()
{
    ?>
    <script>
        $(function () {

            Share = {
                me: function (el) {
                    Share.popup(el.href);
                    return false;
                },
                popup: function (url) {
                    window.open(url, '', 'toolbar=0,status=0,width=626,height=436');
                }
            };

        });
    </script>
<?php }

add_action('wp_footer', 'add_this_share', 20);
?>


<div class="share-vert-buttons">
    <a class="share-btn-fb"
       href="http://www.facebook.com/sharer/sharer.php?s=100&p%5Btitle%5D=<?= get_the_title($id) ?>&p%5Bsummary%5D=<?= get_the_excerpt($id) ?>&p%5Burl%5D=<?= $actual_link ?>&p%5Bimages%5D%5B0%5D=<?= $image_url; ?>"
       target="_blank" onclick="return Share.me(this);">
        <div class="facebook share-vert-block">
            <i class="fa fa-facebook" aria-hidden="true"></i>
        </div>
    </a>
    <a href="http://vk.com/share.php?url=<?= $actual_link ?>&title=<?= get_the_title($id) ?>&description=<?= get_the_excerpt($id) ?>&image=<?= $image_url ?>&noparse=true"
       target="_blank" onclick="return Share.me(this);">
        <div class="vk share-vert-block">
            <i class="fa fa-vk" aria-hidden="true"></i>
        </div>
    </a>
    <a href="https://twitter.com/intent/tweet?original_referer=http%3A%2F%2Ffiddle.jshell.net%2F_display%2F&text=<?= get_the_title($id) ?>&url=<?= $actual_link ?>"
       target="_blank" onclick="return Share.me(this)">
        <div class="twitter share-vert-block">
            <i class="fa fa-twitter" aria-hidden="true"></i>
        </div>
    </a>
    <a href="https://ru.pinterest.com/pin/create/button/?url=<?= $actual_link ?>&media=<?= $image_url ?>&description=<?= get_the_excerpt($id) ?>"
       target="_blank" onclick="return Share.me(this)">
        <div class="pinterest share-vert-block">
            <i class="fa fa-pinterest-p" aria-hidden="true"></i>
        </div>
    </a>
    <a href="https://plus.google.com/share?url=<?= $actual_link ?>" onclick="return Share.me(this)">
        <div class="google_plus share-vert-block">
            <i class="fa fa-google-plus" aria-hidden="true"></i>
        </div>
    </a>
</div>


<div class="ms-slide">
    <div class="play_button">
        <a href="<?php the_permalink(); ?>">
            <img
                src="<? echo get_template_directory_uri() ?>/images/icon-play-small.png"
                alt="open"/>
            <h3><?php echo $post->post_title ?></h3>

        </a>
    </div>
    <?php
    $image_url = "https://thumbs.balticlivecam.com/" . get_post_meta($post->ID, 'blc_camera_id', true) . "_sm.jpg";
    if(get_post_meta($post->ID, 'blc_camera_id', true) == ''){
        $image_url = get_post_meta($post->ID, 'blc_image_url', true);
    }

    $type = get_post_meta($post->ID, "blc_camera_notour", true);
    if ($type == 2) {
        $image_url = wp_get_attachment_image_src(get_post_thumbnail_id($post->ID), 'medium')[0];
    }
    if(!$image_url){
        $image_url = get_post_meta($post->ID, 'blc_camera_id', true);
    }
    if($image_url == ''){
        $image_url = get_template_directory_uri().'/images/img_not_available.jpg';
    }
    ?>
    <img src="<?php echo $image_url; ?>" alt=""/>
</div>

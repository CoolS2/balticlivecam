<?php
function player_css()
{ ?>

    <link href="<?php echo get_template_directory_uri() ?>/videojs/plugins/videojs-ima/videojs.ima.css"
          rel="stylesheet">
    <link href="<?php echo get_template_directory_uri() ?>/videojs/v7.4.2/video-js.min.css" rel="stylesheet">
    <link href="<?php echo get_template_directory_uri() ?>/videojs/blc-js.css?ver=3" rel="stylesheet">
    <link href="<?php echo get_template_directory_uri() ?>/videojs/plugins/videojs-resolution-switcher/videojs-resolution-switcher.css"
          rel="stylesheet">
    <link href="<?php echo get_template_directory_uri() ?>/videojs/plugins/videojs-contrib-ads/videojs-contrib-ads.css"
          rel="stylesheet">
    <link href="<?php echo get_template_directory_uri() ?>/css/player.css?ver=1" rel="stylesheet">

<?php }

add_action('wp_head', 'player_css', 20);

global $embed;
$embed = false;

global $weather_id;
$city_post = get_post($post->post_parent);
$country_post = get_post($city_post->post_parent);

$country_post_id = $country_post->ID;
$country_post_en = icl_object_id($country_post_id, 'page', false, 'en');
global $country_title_en;
$country_title_en = get_the_title($country_post_en);

$weather_id = get_post_meta($city_post->ID, 'blc_city_id', true);
$children = get_pages('child_of=' . $city_post->ID);
$stream_pub = get_post_meta($post->ID, 'text_under_camera', true);
if (count($children) == 1) {
    $back_url = get_permalink($country_post->ID);
} else {
    $back_url = get_permalink($city_post->ID);
}


function add_this_weather_data_footer()
{
    global $weather_id;
    ?>

    <script src="<?php echo get_template_directory_uri() ?>/js/slick.min.js"></script>
    <script>
        var d = new Date();
        var timezone = d.getTimezoneOffset();
        $.ajax({
            url: ajaxurl,
            method: "POST",
            data: {'action': 'weatherblock', 'weather_id': '<?=$weather_id?>'},
            dataType: "html",
            success: function (data) {
                $('.weather_block_page').html(data);
            },
            error: function (jqXHR, textStatus, errorThrown) {
                $loader.html(jqXHR + " :: " + textStatus + " :: " + errorThrown);
            }
        });
        $('.find-mistake h4').on('click', function () {
            $('.mistake-text').toggle(300);
        });
        $('.mistake-text button').on('click', function () {
            if ($(this).hasClass('not-working-camera')) {
                var text_area = 'Camera doesnt work';
            } else {
                var text_area = $('.mistake-text textarea').val();
            }

            var from_bots = $('#from_bots').val();
            var full_url = window.location.href;
            var email = $('.email').val();
            if (from_bots == '') {
                if (text_area != '') {
                    var ajaxurl = '<?php echo admin_url('admin-ajax.php'); ?>';
                    $.ajax({
                        url: ajaxurl,
                        method: "POST",
                        data: {action: 'send_mistake', text_area: text_area, full_url: full_url, email: email},
                        dataType: "html",
                        success: function (data) {
                            if (data == 'ok') {
                                $('.mistake-text').hide(300);
                                $('.find-mistake').html('<h4><?=_e('Your message has been sent', 'blc')?></h4>').css({
                                    'color': '#00a652',
                                    'border-color': '#00a652'
                                });
                                setTimeout(function () {
                                    $('.find-mistake').hide();
                                }, 3000);
                                $('.mistake-text textarea').val('');
                            } else if (data == 'error') {
                                $('.mistake-text button').html('<?=_e('Error, try later', 'blc')?>').css('background', '#d62827');
                            }
                        },
                        error: function (jqXHR, textStatus, errorThrown) {
                            $loader.html(jqXHR + " :: " + textStatus + " :: " + errorThrown);
                        }
                    });
                }
            }
        });
    </script>
<?php }

add_action('wp_footer', 'add_this_weather_data_footer', 20);

$country_args = array(
    'post_type' => 'page',
    'nopaging' => true,
    'orderby' => 'id',
    'order' => 'ASC',
    'meta_query' => array(
        array(
            'key' => '_wp_page_template',
            'value' => 'country.php',
            'compare' => 'LIKE',
        ),
    ),
);

$country_query = new WP_Query($country_args);
$city_args = array(
    'post_type' => 'page',
    'nopaging' => true,
    'post_parent' => $country_post->ID,
    'orderby' => 'id',
    'order' => 'ASC',
    'meta_query' => array(
        array(
            'key' => '_wp_page_template',
            'value' => 'City.php',
            'compare' => '=',
        ),
    ),
);

// The Query
$city_query = new WP_Query($city_args);

$id = icl_object_id($post->ID, 'page', false, 'en');

//Get Gallery
$args = array(
    'post_type' => array('camera_images'),
    'meta_query' => array(
        array(
            'key' => 'camera_id',
            'value' => $id,
            'compare' => '=',
            'type' => 'NUMERIC',
        ),
    ),
);

global $gallery;
$gallery = new WP_Query($args);
//GET TIMELAPSE
$args = array(
    'post_type' => array('camera_timelapse'),
    'meta_query' => array(
        array(
            'key' => 'camera_id',
            'value' => $id,
            'compare' => '=',
            'type' => 'NUMERIC',
        ),
    ),
);
global $timelapse;
$timelapse = new WP_Query($args);
?>


<?php get_header(); ?>


<div class="site">

    <?php get_template_part("partials/nav"); ?>
    <aside class="left follow">
        <div class="left-ads ads-g-a">
            <?php echo do_shortcode("[pro_ad_display_adzone id=7659]"); //LEFT PANEL ?>
        </div>
    </aside>

    <aside class="right follow">
        <div class="right-ads ads-g-a">
            <?php echo do_shortcode("[pro_ad_display_adzone id=7661]"); //RIGHT PANEL ?>
        </div>
    </aside>
    <div id="content" class="site-content" role="main">
        <div class="container">
            <div style="margin-top:10px;" class="ads-g-a">
                <?php echo do_shortcode("[pro_ad_display_adzone id=7738]"); ?>
            </div>
            <?php
            include(locate_template('partials/camera/camera-info.php', false, false));
            ?>
            <div style="display:block;position: relative;overflow: hidden;">
                <div class="button_weather">

                    <div class="weather_block_page">
                    </div>
                </div>
            </div>
            <div class="description-cameras">
                <?php
//                the_content();
//                $cont = get_the_content();
//                $cont = explode('<p>', $cont);
//                for ($i = 0; $i < count($cont); $i++) {
//                    if ($i <= 2) {
//                        echo '<p>' . strip_tags($cont[$i], '<br><strong><b><h2><h3><h4><h5><ul><li><a><i><u><em><span><img>') . '</p>';
//                    }
//                }
                ?>
            </div>

            <?php
            $cur_page_id = icl_object_id($country_post->ID, 'page', true, 'en');
            $id = apply_filters( 'wpml_object_id', $country_post->ID, 'page', FALSE, 'en' );
           ?>


            <div style="position:relative;">
                <?php get_template_part("partials/camera/place", "video"); ?>

                <?php if ($stream_pub != "" && $stream_pub != null) { ?>

                    <?php
                    if (strpos($stream_pub, 'visitrhodes') !== false):
                    $partner = '33863';
                    $thumbnail = get_the_post_thumbnail_url($partner, "full");
                    $url = esc_url(get_post_meta($partner, "partner_url", true));
                    if($url):
                        ?>
                        <a href="<? echo $url ?>" target="_blank" style="display: block;max-width: 175px;padding: 15px;">
                            <img class="fp-sponsor-img" src="<? echo $thumbnail; ?>">
                        </a>
                    <?php endif; ?>
                    <?php endif; ?>
                    <div>
                        <?php echo $stream_pub; ?>
                    </div>
                <?php } ?>
                <? include(locate_template('partials/camera/camera-change.php', false, false)); ?>

                <? include(locate_template('partials/camera/camera-share.php', false, false)); ?>

            </div>

            <div style="display: none;" class="camera-news">
                <a class="btn-blog" id="get_news"><? echo __('Show News', 'blc') ?></a>
            </div>

            <div class="description-cameras">

                <script async src="//pagead2.googlesyndication.com/pagead/js/adsbygoogle.js"></script>
                <ins class="adsbygoogle"
                     style="display:block"
                     data-ad-format="autorelaxed"
                     data-ad-client="ca-pub-9961005829441935"
                     data-ad-slot="1286183948"></ins>
                <script>
                    (adsbygoogle = window.adsbygoogle || []).push({});
                </script>

                <?php
                $id = icl_object_id($country_post->ID, 'page', false, 'en');
                if (($id == '92') || ($id == '113') || ($id == '242')) {
                    ?>
                    <div class="camera-acb-banners row">
                        <div class="col-md-3"><a href="http://www.8cbr.lv" target="_blank"><img
                                        src="https://balticlivecam.com/images/295x155_8.cbr_winter.jpg" alt="acb"></a></div>
                        <div class="col-md-3"><a href="http://www.celiuntilti.lv" target="_blank"><img
                                        src="https://balticlivecam.com/images/295x155_celi_un_tilti_winter.jpg" alt="acb"></a></div>
                        <div class="col-md-3"><a href="http://www.saleniekubloks.lv" target="_blank"><img
                                        src="https://balticlivecam.com/images/SB_295x155.jpg" alt="acb"></a></div>
                        <div class="col-md-3"><a href="http://www.acb-betons.lv" target="_blank"><img
                                        src="https://balticlivecam.com/images/295x155_acb_winter.jpg" alt="acb"></a></div>
                    </div>
                    <?php
                }
                ?>

                <div class="ads-g-a" style="padding: 30px 0;">
                    <?php echo do_shortcode("[pro_ad_display_adzone id=30391]"); ?>
                </div>

                <?php
                the_content();
//                $cont = get_the_content();
//                $cont = explode('<p>', $cont);
//                for ($i = 0; $i < count($cont); $i++) {
//                    if ($i > 2) {
//                        echo '<p>' . strip_tags($cont[$i], '<br><strong><b><h2><h3><h4><h5><ul><li><a><i><u><em><span><img>') . '</p>';
//                    }
//                }
                ?>
                <? get_template_part("partials/index/part", 'likes'); ?>
                <div class="ads-g-a">
                    <?php echo do_shortcode("[pro_ad_display_adzone id=7665]"); ?>
                </div>


            </div>

            <? //get_template_part("partials/camera/place", "related"); ?>
            <?php
            $cur_page_id = icl_object_id($country_post->ID, 'page', true, 'en');
            //$id = apply_filters( 'wpml_object_id', $country_post->ID, 'page', FALSE, 'en' );
            if (($cur_page_id == '92') || ($cur_page_id == '113') || ($cur_page_id == '242')) {
                ?>
                <div class="img-partners">
                    <img src="https://balticlivecam.com/images/acb_logo.png" alt="acb" class="cam-head-img">
                    <img src="https://balticlivecam.com/images/ACB-GRUPA-ACB-BETONS-logo.jpg" alt="acb"
                         class="cam-head-img">
                    <img src="https://balticlivecam.com/images/ACB-GRUPA-SALENIEKU-BLOKS-logo.jpg" alt="acb"
                         class="cam-head-img">
                    <img src="https://balticlivecam.com/images/ACB-GRUPA-CELI-UN-TILTI-logo.jpg" alt="acb"
                         class="cam-head-img">
                    <img src="https://balticlivecam.com/images/8cbr.png" alt="acb" class="cam-head-img">
                </div>
                <?php
            }
            ?>
        </div>


        <div class="news-right-block">
            <div class="relative-block">
                <div class='news-arrow-left news-arrow-right'></div>
                <div class="news-top"></div>
            </div>
        </div>

    </div>
    <?php get_template_part("partials/main/footer"); ?>
</div>
<?php
get_footer();
?>


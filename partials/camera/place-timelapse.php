<?php
/**
 * Created by PhpStorm.
 * User: Admin
 * Date: 29.08.2016
 * Time: 14:59
 */
global $timelapse;
$id = icl_object_id($post->ID,'page',false, 'en');
if (isset($timelapse) && $timelapse->have_posts()) {
    ?>
    <div id="timelapse" class="slider-wrapper" style="display: none;">
        <?php
        $count = $timelapse->post_count;   
    ?>
 
            <script>
            
                $.ajax({
                url: ajaxurl,
                method: "POST",
                data: {'action': 'timelapse_token', 'id_en': '<?=$id?>'},
                dataType: "html",
                success: function (data) {
          
                    obj = JSON.parse(data);
                    $.each(obj, function(key,value){
                        $('.images_lapse').append('<div class="one_timelapse"><img data-url="'+value["name"]+'" src="'+value["img"]+'" alt="img"></div>'); 
                    });
                    if(<?=$count?> > 3){
                    stick();
                }
                },
                    error: function (jqXHR, textStatus, errorThrown) {
                    $loader.html(jqXHR + " :: " + textStatus + " :: " + errorThrown);
                }
            });

                function stick(){
                    $('.lapse-arrow').show();
                    if($(window).width() > 500){
                    
                $('.images_lapse').slick({
                  infinite: true,
                  slidesToShow: 3,
                  nextArrow: $('.slick-next'),
                  prevArrow: $('.slick-prev'),
                });

                }else{

                    $('.images_lapse').slick({
                      infinite: true,
                      slidesToShow: 1,
                      nextArrow: $('.slick-next'),
                      prevArrow: $('.slick-prev'),
                    });

                }
                    
                    }
               


            $(document).on('click', '.one_timelapse', function(){
                var this_url = $(this).find('img').data('url');
                loadUrl(this_url)
            });
            function loadUrl(name){
                    $('.fp-logo').css('left', -999+'px');
                    $('.snapshot').hide();

                    $.ajax({
                url: ajaxurl,
                method: "POST",
                data: {'action': 'single_timelapse', 'name': name, 'id_en': '<?=$id?>'},
                dataType: "html",
                success: function (data) {
     
                    flowplayer(0).load({
                         sources: [
                           { type: "application/x-mpegurl",  src: data }
                        ]
                      });
                },
                error: function (jqXHR, textStatus, errorThrown) {
                    $loader.html(jqXHR + " :: " + textStatus + " :: " + errorThrown);
                }
            });

                   };

            </script>

             
    </div>
<?php } ?>
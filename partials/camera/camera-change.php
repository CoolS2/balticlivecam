<div class="section-after-camera">
    <div class="section-choose-cameras col-sm-3">
        <span> <?php _e("Choose country", "blc") ?></span>
        <div class="selected-country-cameras" data-child="choose_country"><?php echo $country_post->post_title; ?><span
                class="arrow-selected-cameras"></span></div>

    </div>

    <div class="section-choose-cameras  col-sm-3 col-xs-12">
        <span><?php _e("Choose city", "blc") ?></span>
        <div class="selected-country-cameras" data-child="choose_city"><?php echo $city_post->post_title; ?><span
                class="arrow-selected-cameras"></span></div>

    </div>

    <div class="show-normal  col-sm-3 col-xs-12">
        <a href="<?php echo $back_url; ?>" class="btn-back-cameras"><?php _e("Back", "blc") ?></a>
    </div>

</div>

<div class="find-mistake">
    <h4 class="btn-back-cameras"><?=_e('Report a mistake', "blc")?><span>!</span></h4>
</div>

<div class="section-block-cameras col-sm-3 col-xs-12" id="choose_country">
    <?php
    // WP_Query arguments

    // The Loop
    if ($country_query->have_posts()) {
        while ($country_query->have_posts()) {
            $country_query->the_post();
            // do something
            ?>
            <a href="<?php the_permalink(); ?>"><?php the_title() ?></a>
            <?php

        }
    } else {
        // no posts found
    }
    // Restore original Post Data
    wp_reset_postdata();
    // The Query
    ?>
</div>
<div class="section-block-cameras col-sm-3 col-xs-12" style="left:25%" id="choose_city">
    <?php
    // WP_Query arguments

    // The Loop
    if ($city_query->have_posts()) {
        while ($city_query->have_posts()) {
            $city_query->the_post();
            $children = get_pages('child_of=' . $post->ID);
            if (count($children) == 1) {
                $url = get_page_link($children[0]->ID);
            } else {
                $url = get_permalink();
            }
            // do something
            ?>
            <a href="<?php echo $url; ?>"><?php the_title() ?></a>
            <?php
        }
    } else {
        // no posts found
    }
    // Restore original Post Data
    wp_reset_postdata();
    // The Query
    ?>
</div>

<div class="mistake-text">
    <div class="row">
        <div class="col-md-6">
            <textarea name="mistake-text" cols="30" rows="10"></textarea>
        </div>
        <div class="col-md-6">
            <input type="email" name="email" class="email" placeholder="<?=_e('E-mail')?>">
            <input type="hidden" id="from_bots">
            <button class="btn-back-cameras not-working-camera"><?=_e("Camera doesn't work", "blc")?></button>
            <button class="btn-back-cameras"><?=_e('Send', "blc")?></button>
        </div>
    </div>

</div>



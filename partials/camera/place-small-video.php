<?php
$args2 = array(
    'post_type' => 'page',
    'post_status' => 'publish',
    'posts_per_page' => '1',
    'post__not_in' => array($post->ID),
    'orderby' => 'rand',
    'meta_query' => array(
        array(
            'key' => '_wp_page_template',
            'value' => 'camera.php',
            'compare' => '=',
        ),
    ),
);
$query2 = new WP_Query($args2);
    while ($query2->have_posts()) :
        $query2->the_post();
        $rand_post_id = $post->ID;
    endwhile;
    wp_reset_postdata();
        ?>

        <script>
            jQuery(function () {
                if ($(window).width() > 728) {
                    $('#player_load2').load(ajaxurl, data2, function (response) {
                        LoadCameraMsgs2();
                        $('.small-video-left i').on('click', function () {
                            $('.small-video-left').hide();
                        });
                    });
                }else{
                    $('.small-video-left').hide();
                }



            function LoadCameraMsgs2() {

                player2.on("load", function (e, api, video) {
                    clearInterval(timer);
                }).on("error", function (e, api, err) {
                    $('.small-video-left').hide();
                    var delay = 5;
                    if (err.code === 4 || err.code === 9) {
                        timer = setInterval(function () {
                            delay -= 1;
                            if (!delay) {
                                clearInterval(timer);
                                api.error = api.loading = false;
                                container.className = container.className.replace(/ *is-error */, "");
                                api.load(api.conf.clip);
                                $('.small-video-left').show();
                            }
                        }, 1000);
                    }
                });
                player2.on("pause", function (e, api) {
                    api.play();
                });

            }
            });
        </script>

<div class="small-video-left">
    <i class="fa fa-times-circle-o" aria-hidden="true"></i>
<a href="" id="link_to_camera" target="_blank">
    <div class="block_for_v2">

        <div id="live2" style="background-color:#777; background-image:url(https://thumbs.balticlivecam.com/<?php echo $name ?>.png);">

        </div>
    </div>
</a>
</div>

    <div id="player_load2">
    </div>


    <script>
        var container2 = document.getElementById("live2"),
            timer2, player2;

        var data2 = {
            action: 'auth_token_small',
            id: <?php echo $rand_post_id;?>
        };


    </script>
<style>
    #live{
        overflow: inherit!important;
    }
    .small-video-left{
        position: absolute;
        top: 130px;
        right: -70px;
        width: 275px;
        z-index: 9;
    }
    .small-video-left i{
        top: -7px;
        right: -7px;
        position: absolute;
        z-index: 1001;
        color: #b9ebff;
        font-size: 25px;
        width: 20px;
        height: 20px;
        line-height: 0.9;
        cursor: pointer;
        border-radius: 100%;
        background: #fff;
    }
    #live2{
        border: 3px solid #b9ebff;
    }
    #live2 .fp-message, #live2 .fp-ui .fp-controls, .block_for_v2 a{
        opacity: 0!important;
    }
</style>
<?php global $post; ?>
<?php $adsOff = get_post_meta($post->ID, 'blc_camera_embed', true); ?>
<?php if(!$adsOff): ?>
<?php exit; ?>
<?php endif; ?>

<!DOCTYPE html>
<html <?php language_attributes(); ?>>
<head>
    <meta charset="<?php bloginfo('charset'); ?>">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title><? wp_title() ?></title>
    <link href='https://fonts.googleapis.com/css?family=Open+Sans:400,300,700&subset=latin,cyrillic' rel='stylesheet'
          type='text/css'>
    <link rel="icon" type="image/x-icon" href="<? echo get_template_directory_uri() ?>/favicon.ico"/>
    <link rel="canonical" href="<?php echo get_permalink();?>"/>
    <link href="<?php echo get_template_directory_uri() ?>/videojs/plugins/videojs-ima/videojs.ima.css" rel="stylesheet">
    <link href="<?php echo get_template_directory_uri() ?>/videojs/video-js.min.css" rel="stylesheet">
    <link href="<?php echo get_template_directory_uri() ?>/videojs/blc-js.css?ver=2" rel="stylesheet">
    <link href="<?php echo get_template_directory_uri() ?>/videojs/plugins/videojs-resolution-switcher/videojs-resolution-switcher.css" rel="stylesheet">
    <link href="<?php echo get_template_directory_uri() ?>/videojs/plugins/videojs-contrib-ads/videojs-contrib-ads.css" rel="stylesheet">
    <link href="<?php echo get_template_directory_uri() ?>/css/player.css?ver=1" rel="stylesheet">

    <!--<script type='text/javascript'>
        (function (w) {
            var d = document, h = d.getElementsByTagName('head')[0], j = d.createElement('script'), k = d.createElement('script');
            j.setAttribute('src', '//cdn.adsoptimal.com/advertisement/settings/30644.js');
            k.setAttribute('src', '//cdn.adsoptimal.com/advertisement/manual.js');
            h.appendChild(j);
            h.appendChild(k);
        })(window);
    </script>-->
    <!--<script async src="https://balticlivecam.com/wp-admin/admin-ajax.php?action=wppas_asyncjs"></script>-->

    <link rel="stylesheet" href="<? echo get_template_directory_uri() ?>/css/embed.css"/>
</head>
<body>
<script src="<? echo get_template_directory_uri() ?>/js/jquery.min.js"></script>

<script>
    var ajaxurl = '<?php echo admin_url('admin-ajax.php'); ?>';
</script>

    <?php
        global $embed;
        $embed = true;
    ?>
    <? get_template_part("partials/camera/place", "video"); ?>

<!-- Start Alexa Certify Javascript -->
<script type="text/javascript">
    _atrk_opts = { atrk_acct:"SOEVm1aMp4Z37i", domain:"balticlivecam.com",dynamic: true};
    (function() { var as = document.createElement('script'); as.type = 'text/javascript'; as.async = true; as.src = "https://d31qbv1cthcecs.cloudfront.net/atrk.js"; var s = document.getElementsByTagName('script')[0];s.parentNode.insertBefore(as, s); })();
</script>
<noscript><img src="https://d5nxst8fruw4z.cloudfront.net/atrk.gif?account=SOEVm1aMp4Z37i" style="display:none" height="1" width="1" alt="" /></noscript>
<!-- End Alexa Certify Javascript -->
<script>
    (function (i, s, o, g, r, a, m) {
        i['GoogleAnalyticsObject'] = r;
        i[r] = i[r] || function () {
                (i[r].q = i[r].q || []).push(arguments)
            }, i[r].l = 1 * new Date();
        a = s.createElement(o),
            m = s.getElementsByTagName(o)[0];
        a.async = 1;
        a.src = g;
        m.parentNode.insertBefore(a, m)
    })(window, document, 'script', '//www.google-analytics.com/analytics.js', 'ga');

    ga('create', 'UA-32676219-2', 'auto');
    ga('send', 'pageview');

    $('.fp-sponsor').on('click', function(){
        ga('send', {
            hitType: 'event',
            eventCategory: 'partner-embed',
            eventAction: 'click',
            eventLabel: 'embed'
        });
    });
    $('.fp-logo').on('click', function(){
        ga('send', {
            hitType: 'event',
            eventCategory: 'Logo',
            eventAction: 'Click',
            eventLabel: 'Logo'
        });
    });
    $('.image-container').on('click', function(){
        ga('send', {
            hitType: 'event',
            eventCategory: 'Embed',
            eventAction: 'Click',
            eventLabel: 'Advertise'
        });
    });

</script>

<script src="<?php echo get_template_directory_uri(); ?>/js/ads.js" type="text/javascript"></script>
<? wp_footer(); ?>
<?php get_template_part("partials/ads/adnet", "init");?>
</body>
</html>

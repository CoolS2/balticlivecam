<?php

//Create a new DOMDocument object
$dom = new DOMDocument("1.0");
$node = $dom->createElement("markers"); //Create new element node
$parnode = $dom->appendChild($node); //make the node show up

$args = array(
    'orderby' => 'id',
    'post_status' => array('Publish'),
    'post_type' => 'page',
    'meta_query' => array(
        array(
            'key' => '_wp_page_template',
            'value' => 'country.php',
            'compare' => '=',
        ),
    ),
);
// The Query
$query = new WP_Query($args);
if ($query->have_posts()) {

    while ($query->have_posts()) {
        $query->the_post();
        $link = get_permalink();
        $style = basename(parse_url($link, PHP_URL_PATH));
        $weather_id = get_post_meta($post->ID, 'blc_city_id', true);
        $markPosY = get_post_meta($post->ID, 'top', true);
        $markPosX = get_post_meta($post->ID, 'left', true);


        $node = $dom->createElement("marker");
        $newnode = $parnode->appendChild($node);
        $newnode->setAttribute("name", $post->post_title);
        $newnode->setAttribute("lat", $markPosY);
        $newnode->setAttribute("lng", $markPosX);
        $newnode->setAttribute('url', $link);

    }

}
?>
<div id="map"></div>
<script type="text/javascript">

    var data = '<?=$dom->saveXML($dom->documentElement)?>';
    var parentY = parseFloat('<?=$parentY?>');
    var parentX = parseFloat('<?=$parentX?>');

    var map;
    function initMap() {
        map = new google.maps.Map(document.getElementById('map'), {
            center: {lat: 58.222189, lng: 19.111080},
            zoom: 5,
            scrollwheel: false,
            maxZoom: 5,
            minZoom: 3,
        });

        $(data).find("marker").each(function () {
            var name = $(this).attr('name');
            var point = new google.maps.LatLng(parseFloat($(this).attr('lat')), parseFloat($(this).attr('lng')));
            var link = $(this).attr('url');
            var info = '<div class="marker-info"><ul><li><h3>' + name + '</h3></li><li><a href="' + link + '">link</a></li></ul></div>';


            create_marker(point, name, link, false, false, false);
        });


        function create_marker(MapPos, MapTitle, Link, InfoOpenDefault, DragAble, Removable) {

            //new marker
            var marker = new google.maps.Marker({
                position: MapPos,
                map: map,
                draggable: DragAble,
                title: MapTitle
            });

            marker.addListener('click', function () {
                location.href = Link;
            });
        }
    }


</script>
<script async defer
        src="https://maps.googleapis.com/maps/api/js?key=AIzaSyDSoSHpqN3wmSoHKWRQfulDykzzNT65fBA&callback=initMap">
</script>

<style>

    .marker-info h3 {
        text-align: center;
        font-size: 20px;
    }

    .tempa {
        display: table;
        padding: 5px 10px;
    }

    .temp_w {
        display: table-cell;
        vertical-align: middle;
    }

    .marker-info {
        min-width: 100px;
        overflow: hidden;
    }

    .tempa img {
        width: 45px;
        display: table-cell;
    }

</style>

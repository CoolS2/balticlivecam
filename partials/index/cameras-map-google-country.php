<?php
global $parentY, $parentX, $postid;
$parentY = get_post_meta($post->ID, 'top', true);
$parentX = get_post_meta($post->ID, 'left', true);
$postid = $post->ID;
?>
<div id="map" class="map_list_size"></div>


<?php function map_script_footer()
{ global $parentY, $parentX, $postid;
    ?>

    <script type="text/javascript">
        $(function () {
            $.ajax({
                url: ajaxurl,
                method: "POST",
                data: {
                    'action': 'weathermap', 'post_id': '<?= $postid ?>'
                },
                dataType: "html",
                success: function (data) {
                    doWork(data);

                },
                error: function (jqXHR, textStatus, errorThrown) {
                    $loader.html(jqXHR + " :: " + textStatus + " :: " + errorThrown);
                }
            });

            function doWork(data) {
                $(data).find("marker").each(function () {
                    var
                        name = $(this).attr('name');
                    var
                        point = new google.maps.LatLng(parseFloat($(this).attr('lat')), parseFloat($(this).attr('lng')));
                    var
                        temp = $(this).attr('temp');
                    var
                        tempc = $(this).attr('tempc');
                    var
                        link = $(this).attr('url');
                    var
                        thumb = $(this).attr('thumb');

                    var
                        info = '<div class="marker-info"><ul><li><h3>' + name + '</h3></li><li class="tempa"><img src="' + temp + '" alt="temp"><p class="temp_w"> ' + tempc + '°C</p></li><li><a href="' + link + '">Explore</a></li></ul></div>';

                    create_marker(point, name, info, false, false, false);
                });
            }

            function create_marker(MapPos, MapTitle, Info, InfoOpenDefault, DragAble, Removable) {
                //icon

                //new marker

                var
                    marker = new google.maps.Marker({
                        position: MapPos,
                        map: map,
                        draggable: DragAble,
                        title: MapTitle,
                    });

                var contentString = $(Info);

                var infowindow = new google.maps.InfoWindow();
                //set the content of infoWindow
                infowindow.setContent(contentString[0]);
                marker.addListener('click', function () {
                    infowindow.open(map, marker);
                });

            }
        });

        var parentY = parseFloat('<?=$parentY?>');
        var parentX = parseFloat('<?=$parentX?>');

        var map;

        function initMap() {
            map = new google.maps.Map(document.getElementById('map'), {
                center: {
                    lat: parentY, lng: parentX
                },
                zoom: 7,
                scrollwheel: false,
                maxZoom: 9,
                minZoom: 6,
            });
        }


    </script>
    <script async defer
            src="https://maps.googleapis.com/maps/api/js?key=AIzaSyDSoSHpqN3wmSoHKWRQfulDykzzNT65fBA&callback=initMap">
    </script>


<?php }

add_action('wp_footer', 'map_script_footer', 20);
?>

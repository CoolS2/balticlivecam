<div class="camera-item-block col-md-4">
    <?php
    $post_date = date_create(get_the_date('Y-m-d'));
    $today = date_create(date('Y-m-d'));
    $interval = date_diff($post_date, $today);
    $numb = $interval->format('%a%');
    $template = get_page_template_slug();
    if($numb < 14){
        $new_camera = '<img class="new-camera-img" src="'.get_template_directory_uri().'/images/new-camera.png" alt="new-camera">';
    }else{
        $new_camera = '';
    }
    $title = $post->post_title;
    if ($template == "City.php") {
        $title .= ' , ' . $country_post->post_title;
    }

    $image_url = "https://thumbs.balticlivecam.com/" . get_post_meta($post->ID, 'blc_camera_id', true) . "_sm.jpg";
    if(get_post_meta($post->ID, 'blc_camera_id', true) == ''){
        $image_url = get_post_meta($post->ID, 'blc_image_url', true);
    }

    $type = get_post_meta($post->ID, "blc_camera_notour", true);
    if ($type == 2) {
        $image_url = wp_get_attachment_image_src(get_post_thumbnail_id($post->ID), 'medium')[0];
    }
    if(!$image_url){
        $image_url = get_post_meta($post->ID, 'blc_camera_id', true);
    }
    if($image_url == ''){
        $image_url = wp_get_attachment_image_src(get_post_thumbnail_id($post->ID), 'medium')[0];
    }
    if($image_url == ''){
        $image_url = get_template_directory_uri().'/images/img_not_available.jpg';
    }

    $play_btn = '<img src="'.get_template_directory_uri().'/images/icon-play-small.png" alt="open">';

    $meta = get_post_meta($post->ID, 'under_construction', true);
    if($meta == '1'){
      $image_url = get_template_directory_uri().'/images/UConstruction.jpg';
      $play_btn = '';
      $url = 'javascript:';
      $title = '';
    }
    /**
     *  GET RANDOM IMAGE
     * */

        //echo wpdocs_echo_first_image($post->ID);
    ?>

    <div class="main-page-cams" style="background-image: url(<?=$image_url?>);">
        <a href="<?=$url?>">
        <?=$new_camera?>
        <div class="play_button">
                <?=$play_btn?>
                <h3><?php echo $title ?></h3>
        </div>
        </a>
    </div>


    <!--<div class="cont">
        <div>
            <div class="camera-action-block" style="<?/* echo $new_camera */?>">
                <div class="camera-block-background">
                    <a href="<?php /*echo $url; */?>"><img
                            src="<?/* echo get_template_directory_uri() */?>/images/icon-play-small.png" alt=""/>
                        <h6><?php /*echo $title; */?></h6>
                    </a>
                </div>
            </div>
            <?php /*if (($template == "City.php") || ($template == "country.php")) : */?>
                <img src="<?php /*echo $image_url */?>" alt="" class="camera-img-style"/>
            <?php /*elseif ($template == "user-content/camera-sub-category.php") : */?>
                <img src="<?php /*echo $image_url */?>" alt="" class="camera-img-style"/>
            <?php /*else: */?>
                <img src="<?php /*echo $image_url; */?>" alt="" class="camera-img-style"/>
            <?php /*endif; */?>
        </div>
    </div>-->

</div>

<?php
$args = array(
    'category_name' => 'firstpagecameras',
    'post_type' => 'page',
    'posts_per_page' => '6',
    'order' => 'DESC',
    'orderby'  => 'menu_order'
);
$query = new WP_Query($args);
$a = 0;
$rand = rand(4,6);
?>


<div class="main_category cams-blocks">
    <?php
        $top_cameras = __("Top cameras", "blc");
        $top_cameras_pieces = explode(' ', $top_cameras);
        //$top_cameras_first = preg_replace('/\W\w+\s*(\W*)$/', '$1', $top_cameras);
        $top_cameras_first = array_shift(array_values($top_cameras_pieces));
        $top_cameras_last_word = end($top_cameras_pieces);
    if (count($top_cameras_pieces) != 1) {
        $new_title = $top_cameras_first.'<span>'.$top_cameras_last_word.'</span>';
    }else{
        $new_title = '<span>'.$top_cameras_last_word.'</span>';
    }
    ?>
    <div class="title">
        <h3 class="animation-element"><?=$new_title?></h3>
    </div>

        <div class="row">
            <?php while ($query->have_posts()) : $query->the_post(); ?>
                <?php
                $image_url = "https://thumbs.balticlivecam.com/" . get_post_meta($post->ID, 'blc_camera_id', true) . "_sm.jpg";
                $type = get_post_meta($post->ID, "blc_camera_notour", true);
                if ($type == 2) {
                    $image_url = wp_get_attachment_image_src(get_post_thumbnail_id($post->ID), 'medium')[0];
                    if(!$image_url){
                        $image_url = get_post_meta($post->ID, 'blc_camera_id', true);
                    }
                }
                if($image_url == ''){
                    $image_url = get_template_directory_uri().'/images/img_not_available.jpg';
                }

                //if($a == $rand){
                    ?>
          <!--  <div class="col-md-4">
                <div class="main-page-cams">
                    <script async src="//pagead2.googlesyndication.com/pagead/js/adsbygoogle.js"></script>

                    <ins class="adsbygoogle"
                         style="display:block"
                         data-ad-client="ca-pub-9961005829441935"
                         data-ad-slot="9612729088"
                         data-ad-format="auto"></ins>
                    <script>
                        (adsbygoogle = window.adsbygoogle || []).push({});
                    </script>
                    </div>
                </div>-->
                    <?php
               // }else{
                    ?>
                    <div class="col-md-4">
                        <div class="main-page-cams" style="background-image: url(<?=$image_url?>);">
                            <a href="<?php the_permalink(); ?>">
                            <div class="play_button">
                                    <img src="<? echo get_template_directory_uri() ?>/images/icon-play-small.png" alt="open">
                                    <h3><?php echo $post->post_title ?></h3>
                            </div>
                            </a>
                        </div>
                    </div>
                        <?php
              //  }
                ?>

            <?php
            $a++;
              endwhile;
              wp_reset_postdata();
            ?>
        </div>

<!--    <div class="ms-caro3d-template ms-caro3d-wave">
        <div class="master-slider ms-skin-default main-sliders">
            <?php /*while ($query->have_posts()) : $query->the_post(); */?>
                <div class="ms-slide">
                    <?php /*if (1 > 1) { */?>
                        <video class="thevideo" loop preload="none">
                            <source src="/videos/aaa.mp4" type="video/mp4">
                            <source src="/videos/aaa.webm" type="video/webm">
                        </video>
                    <?php /*} */?>
                    <div class="play_button">
                        <a href="<?php /*the_permalink(); */?>">
                            <img src="<?/* echo get_template_directory_uri() */?>/images/icon-play-small.png" alt="open">
                            <h3><?php /*echo $post->post_title */?></h3>
                        </a>
                    </div>

                    <?php
/*                    $image_url = "https://thumbs.balticlivecam.com/" . get_post_meta($post->ID, 'blc_camera_id', true) . "_sm.png";
                    $type = get_post_meta($post->ID, "blc_camera_notour", true);
                    if ($type == 2) {
                        $image_url = get_post_meta($post->ID, 'blc_camera_id', true);
                    }
                    */?>
                    <img src="<?php /*echo $image_url; */?>" data-src="<?php /*echo $image_url; */?>" alt=""/>
                </div>
                <?php
/*            endwhile;
            wp_reset_postdata();

            */?>
        </div>
    </div>-->
</div>

<?php
$args = array(
    'post_type' => 'post',
    'post_status' => 'publish',
    'posts_per_page' => '3',
    'order' => 'DESC',
    'cat' => '56, 1, 55,'

);

$myposts = get_posts($args);
if (count($myposts) > 0) {
    ?>


    <div class="main_category">
        <?php
        $top_cameras = __("Blog", "blc");
        $top_cameras_pieces = explode(' ', $top_cameras);
        $top_cameras_first = preg_replace('/\W\w+\s*(\W*)$/', '$1', $top_cameras);
        $top_cameras_last_word = end($top_cameras_pieces);
        if (count($top_cameras_pieces) != 1) {
            $new_title = $top_cameras_first.'<span>'.$top_cameras_last_word.'</span>';
        }else{
            $new_title = '<span>'.$top_cameras_last_word.'</span>';
        }

    if ( wp_is_mobile() ) {
        $mobile = 'animation-element blog-animation';
    }else{
        $mobile = '';
    }
        ?>
        <div class="title">
            <h3 class="animation-element"><?=$new_title?></h3>
        </div>
        <div class="blog-blocks">
            <? foreach ($myposts as $post) : setup_postdata($post); ?>
                <!--<div class="col-md-3 col-sm-6 col-xs-12 blog-block">
                    <a href="<?php /*echo get_post_permalink(); */?>">
                        <div class="blog-img-block">
                            <img src="<?/* the_post_thumbnail_url("medium"); */?>" alt="image">
                            <h3><?php /*the_title(); */?></h3>
                        </div>
                    </a>
                </div>-->

            <div class="col-md-4 col-sm-6 col-xs-12 blog-block">
                <figure class="snip1477 <?=$mobile?>" style="background: url(<? the_post_thumbnail_url("medium"); ?>);">
                    <div class="title-blog">
                        <div>
                            <h2>Read</h2>
                            <h4>More</h4>
                        </div>
                    </div>
                    <figcaption>
                        <p><?php the_title(); ?></p>
                    </figcaption>
                    <a href="<?php the_permalink(); ?>"></a>
                </figure>
            </div>

            <?php endforeach;
            wp_reset_postdata(); ?>

        </div>
    </div>
<?php } ?>
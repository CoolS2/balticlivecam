<div class="main_solutions">
    <div style="overflow: hidden">
        <div class="col-md-3">
            <div class="main_solution_imgblock">

                <img src="<? echo get_template_directory_uri() ?>/images/Key-Lock-icon.png"/>
            </div>
            <h4><?php _e('Private Solution', 'blc')?></h4>
            <p><?php _e('Baltic Live Cam is offering you a solution for high quality, uninterrupted streaming 24
                hours a day, with a commercial possibility. Make your view work for you!')?>
            </p>
            <a href="<?php echo get_page_link(3438); ?>"><?php _e('See more', 'blc')?></a>
        </div>
        <div class="col-md-3">
            <div class="main_solution_imgblock">

                <img src="<? echo get_template_directory_uri() ?>/images/033-512.png"/>
            </div>
            <h4><?php _e('HOTELS AND RESTAURANTS', 'blc')?></h4>
            <p><?php _e('Do you want to make your hotel or restaurant more recognizable and increase its client
                conversion rate? Baltic Live Cam provides a solution that is already benefiting numerous
                hotels across the Baltic region.', 'blc')?>
            </p>
            <a href="<?php echo get_page_link(55); ?>"><?php _e('See more', 'blc')?></a>
        </div>
        <div class="col-md-3">
            <div class="main_solution_imgblock">

                <img src="<? echo get_template_directory_uri() ?>/images/Megaphone-512.png"/>
            </div>
            <h4><?php _e('MEDIA AND NEWS PORTALS', 'blc')?></h4>
            <p><?php _e('Are you looking for high-quality content for a TV channel or a news portal? Don’t
                hesitate, improve your media outlet now with the unique solution from Baltic Live Cam!', 'blc')?>
            </p>
            <a href="<?php echo get_page_link(52); ?>"><?php _e('See more', 'blc')?></a>
        </div>
        <div class="col-md-3">
            <div class="main_solution_imgblock">
                <img src="<? echo get_template_directory_uri() ?>/images/balloons.png"/>
            </div>
            <h4><?php _e('EVENT HOLDERS', 'blc')?></h4>
            <p>
                <?php _e('Are you an event organizer and you seek to attract new audiences and attention to your
                event? Get the perfect solution for providing more attraction to any holiday celebration
                or any other event.', 'blc')?>
            </p>
            <a href="<?php echo get_page_link(40); ?>"><?php _e('See more', 'blc')?></a>
        </div>

    </div>
</div>

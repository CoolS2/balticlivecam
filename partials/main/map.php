<?
global $euro_map;
$dom = new DOMDocument("1.0");
$node = $dom->createElement("markers"); //Create new element node
$parnode = $dom->appendChild($node); //make the node show up

$args = array(
    'orderby' => 'id',
    'post_status' => array('Publish'),
    'post_type' => 'page',
    'nopaging' => true,
    'meta_query' => array(
        array(
            'key' => '_wp_page_template',
            'value' => 'country.php',
            'compare' => '=',
        ),
    ),
);
// The Query
$query = new WP_Query($args);
if ($query->have_posts()) {

    while ($query->have_posts()) {
        $query->the_post();
        $link = get_permalink();
        $style = basename(parse_url($link, PHP_URL_PATH));
        $weather_id = get_post_meta($post->ID, 'blc_city_id', true);
        $markPosY = get_post_meta($post->ID, 'top', true);
        $markPosX = get_post_meta($post->ID, 'left', true);


        $node = $dom->createElement("marker");
        $newnode = $parnode->appendChild($node);
        $newnode->setAttribute("name", $post->post_title);
        $newnode->setAttribute("lat", $markPosY);
        $newnode->setAttribute("lng", $markPosX);
        $newnode->setAttribute('url', $link);

    }
}
$euro_map = $dom;

function add_this_map_script_footer()
{
    global $euro_map ;
    ?>
    <script>
        var data = '<?=$euro_map->saveXML($euro_map->documentElement)?>';
        var parentY = parseFloat('');
        var parentX = parseFloat('');
        var zoom = 5;
        if (window_width < 992){
            zoom =4;

        }
        var map;
        function initMap() {
            map = new google.maps.Map(document.getElementById('map'), {
                center: {lat: 58.222189, lng: 19.111080},
                zoom: zoom,
                streetViewControl: true,
                scrollwheel: false,
                zoomControlOptions: {
                    position: google.maps.ControlPosition.LEFT_BOTTOM
                },
                maxZoom: 5,
                minZoom: 3,
            });

            $(data).find("marker").each(function () {
                var name = $(this).attr('name');
                var point = new google.maps.LatLng(parseFloat($(this).attr('lat')), parseFloat($(this).attr('lng')));
                var link = $(this).attr('url');
                var info = '<div class="marker-info"><ul><li><h3>' + name + '</h3></li><li><a href="' + link + '">link</a></li></ul></div>';


                create_marker(point, name, link, false, false, false);
            });


            function create_marker(MapPos, MapTitle, Link, InfoOpenDefault, DragAble, Removable) {

//new marker
                var marker = new google.maps.Marker({
                    position: MapPos,
                    map: map,
                    draggable: DragAble,
                    title: MapTitle
                });

                marker.addListener('click', function () {
                    location.href = Link;
                });
            }
        }


    </script>
    <script async defer
            src="https://maps.googleapis.com/maps/api/js?key=AIzaSyDSoSHpqN3wmSoHKWRQfulDykzzNT65fBA&callback=initMap">
    </script>

<?php }

add_action('wp_footer', 'add_this_map_script_footer', 20);

?>


<div class="main_category">
    <?php
    $top_cameras = __("MAP OF CAMERAS", "blc");
    $top_cameras_pieces = explode(' ', $top_cameras);
    $top_cameras_first = array_shift(array_values($top_cameras_pieces));
    $top_cameras_last_word = end($top_cameras_pieces);
    if (count($top_cameras_pieces) != 1) {
        $new_title = $top_cameras_first.'<span>'.$top_cameras_last_word.'</span>';
    }else{
        $new_title = '<span>'.$top_cameras_last_word.'</span>';
    }
    ?>
    <div class="title">
        <h3 class="animation-element"><?=$new_title?></h3>
    </div>
    <div id="map" class="main_map"></div>
</div>
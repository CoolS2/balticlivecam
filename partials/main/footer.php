<footer>
    <div class="container">
        <a href="<?php echo icl_get_home_url() ?>" class="logo"><img src="<? echo get_template_directory_uri() ?>/images/logo.png" alt=""/></a>
        <a href="http://animalslife.net" class="logo" target="_blank"><img src="<? echo get_template_directory_uri() ?>/images/logo-anim.png" alt="animalslife.net"/></a>
        <? wp_nav_menu(array('theme_location' => 'footer-menu', 'container' => 'ul', 'container_class' => 'menu-footer', 'menu_class' => 'menu-footer')) ?>
        <img src="https://cdn.balticlivecam.com/images/acb_logo.png" alt="acb" style="padding: 25px;">
        <div class="social-footer">
            <ul class="white-icons">
                <?php get_template_part("partials/sociallinksfooter") ?>
            </ul>
        </div>
        <p class="copyright">© copyright 2015-<?php echo (new DateTime('NOW'))->format('Y');  ?>. Baltic Live cam</p>
    </div>
</footer>
<?php /*get_template_part("partials/main/part-mobapp") */?>
</div>
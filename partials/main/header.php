<?php
$languages = apply_filters('wpml_active_languages', null, array('skip_missing' => '0'));
$my_current_lang = apply_filters('wpml_current_language', NULL);
/*if($_SESSION['user-id']){
    $login_style = 'logged';
}else{
    $login_style='';
}*/
?>
<header id="header" class="site-header">
    <div id="fp-background"></div>
    <nav class="navbar navbar-static-top">


        <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar"
                aria-expanded="false" aria-controls="navbar">
            <span class="sr-only">Toggle navigation</span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
        </button>
        <a class="navbar-brand" href="/"><img src="<?php echo get_template_directory_uri();?>/images/logo.png"/>
        </a>
        <div id="navbar" class="navbar-collapse collapse navbar-right">
            <div class="only_mobile">
                <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar"
                        aria-expanded="false" aria-controls="navbar">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
            </div>
            <div class="social-header only_desktop">

                <ul class="white-icons">
                    <?php get_template_part("partials/sociallinks") ?>
                </ul>

            </div>
            <div class="app_nav only_desktop">

                <a href="<?php echo get_permalink(icl_object_id(4830, 'page', true)); ?>"><p style="background: #fff; color: #565656;">APP</p></a>
            </div>
            <!--<div class="login-button-header only_desktop <?/*=$login_style*/?>">
                <a href="javascript:"><?/*=_e('Login')*/?></a>
            </div>-->
            <div class="change-lang">
                <a href="javascript:">
                    <span><?php echo ICL_LANGUAGE_NAME; ?></span>
                </a>

                <ul class="dropdown-lang" style="display:none;">
                    <?php
                    foreach ($languages as $lang_key => $language) { ?>
                        <li><a <?php if ($my_current_lang == $language["language_code"]) {
                                echo 'class="active"';
                            } ?>
                                href="<?php echo $language["url"] ?>"><?php echo $language["native_name"] ?>  </a></li>
                        <?php
                    }
                    ?>
                </ul>
            </div>

            <div class="search-block-form">
                <form role="search" method="get" class="search-form" action="<?php echo home_url( '/' ); ?>">

                    <input type="search" class="search-field"
                           placeholder="<?=_e('Search...', 'blc')?>"
                           value="<?php echo get_search_query() ?>" name="s"/>
                    <button><i class="fa fa-search" aria-hidden="true"></i></button>

                </form>
            </div>

          <?  wp_nav_menu( array(
            'menu'              => 'primary',
            'theme_location'    => 'header-menu',
            'depth'             => 2,
            'container'         => 'ul',
            'container_class'   => 'nav navbar-nav',
            'menu_class'        => 'nav navbar-nav',
            'fallback_cb'       => 'wp_bootstrap_navwalker::fallback',
            'walker'            => new wp_bootstrap_navwalker())
            );

          ?>

            <div class="search-block-form only_mobile">
                <form role="search" method="get" class="search-form" action="<?php echo home_url( '/' ); ?>">

                    <input type="search" class="search-field"
                           placeholder="<?=_e('Search...', 'blc')?>"
                           value="<?php echo get_search_query() ?>" name="s"/>
                    <button><i class="fa fa-search" aria-hidden="true"></i></button>

                </form>
            </div>

            <div class="social-header only_mobile">

                <ul class="white-icons">
                    <?php get_template_part("partials/sociallinks") ?>
                </ul>

            </div>
            <div class="app_nav  only_mobile">
                <a href="<?php echo get_permalink(icl_object_id(4830, 'page', true)); ?>"><p style="background: #fff; color: #565656;">APP</p></a>
            </div>
            <!--<div class="login-button-header only_mobile <?/*=$login_style*/?>">
                <a href="javascript:"><?/*=_e('Login')*/?></a>
            </div>-->
                <?// wp_nav_menu(array('theme_location' => 'header-menu', 'container' => 'ul', 'container_class' => 'nav navbar-nav', 'menu_class' => 'nav navbar-nav')) ?>
        </div>

    </nav>

    <?php
    $country_args = array(
        'post_type' => 'page',
        'post_status' => 'publish',
        'nopaging' => true,
        'orderby' => 'id',
        'order' => 'ASC',
        'meta_query' => array(
            array(
                'key' => '_wp_page_template',
                'value' => 'country.php',
                'compare' => 'LIKE',
            ),
        ),
    );

    $country_query = new WP_Query($country_args);
    $city_args = array(
        'post_type' => 'page',
        'post_status' => 'publish',
        'nopaging' => true,
        'post_parent' => $country_post->ID,
        'orderby' => 'id',
        'order' => 'ASC',
        'meta_query' => array(
            array(
                'key' => '_wp_page_template',
                'value' => 'City.php',
                'compare' => '=',
            ),
        ),
    );
    $city_query = new WP_Query($city_args);

    $camera_args = array(
        'post_type' => 'page',
        'post_status' => 'publish',
        'nopaging' => true,
        'orderby' => 'id',
        'order' => 'ASC',
        'meta_query' => array(
           // 'relation' => 'AND',
            array(
                'key' => '_wp_page_template',
                'value' => 'camera.php',
                'compare' => '=',
            ),
//            array(
//                'key' => '_wp_page_template',
//                'value' => 'user-content/camera-second.php',
//                'compare' => '=',
//            ),
        ),
    );
    $camera_query = new WP_Query($camera_args);

    if ( $country_query->have_posts() ) {
            $country_query->the_post();
        wp_reset_query();
    }
    if ( $city_query->have_posts() ) {
        $city_query->the_post();
        wp_reset_query();
    }
    if ( $camera_query->have_posts() ) {
        $camera_query->the_post();
        wp_reset_query();
    }
    ?>


    <div class="container">
        <div class="main_text">
            
                <div style="margin:10px 0 20px; max-height: 90px;" class="ads-g-a header-ad-zone">
                    <?php echo do_shortcode("[pro_ad_display_adzone id=47104]"); ?>
                </div>

            <h1><?=_e('See live streams! More than 500 web cameras on-line from different cities all over the world', 'blc')?></h1>
            <div class="count-of-pages">
                <ul>
                    <li>
                        <p><?=_e('Countries', 'blc')?>:</p>
                        <span><?=$country_query->post_count;?></span>
                    </li>
                    <li>
                        <p><?=_e('Cities', 'blc')?>:</p>
                        <span><?=$city_query->post_count;?></span>
                    </li>
                    <li>
                        <p><?=_e('Cameras', 'blc')?>:</p>
                        <span><?=$camera_query->post_count;?></span>
                    </li>
                </ul>
            </div>
        </div>

        <div class="main_slide">
<!--            <a href="--><?php //echo esc_url(get_permalink(20));?><!--">--><?//=_e('see all cameras', 'blc')?><!--</a>-->
            <img src="<?php echo get_template_directory_uri();?>/images/scroll.png">
        </div>
    </div>
</header>
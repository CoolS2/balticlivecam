<div class="mob-app" id="mob-app">
    <div id="app-img"></div>
    <i id="el-i" class="fa fa-times" aria-hidden="true"></i>
</div>
<script>
    /*COOKIE*/
    function setCookie(name, value, days) {
        if (days) {
            var date = new Date();
            date.setTime(date.getTime()+(days*24*60*60*1000));
            var expires = "; expires="+date.toGMTString();
        }
        else var expires = "";
        document.cookie = name+"="+value+expires+"; path=/";
    }

    function getCookie(name) {
        var nameEQ = name + "=";
        var ca = document.cookie.split(';');
        for(var i=0;i < ca.length;i++) {
            var c = ca[i];
            while (c.charAt(0)==' ') c = c.substring(1,c.length);
            if (c.indexOf(nameEQ) == 0) return c.substring(nameEQ.length,c.length);
        }
        return null;
    }

    function eraseCookie(name) {
        createCookie(name,"",-1);
    }

        function getMobileOperatingSystem() {
            var userAgent = navigator.userAgent || navigator.vendor || window.opera;

            // Windows Phone must come first because its UA also contains "Android"
            if (/windows phone/i.test(userAgent)) {
                return "Windows Phone";
            }

            if (/android/i.test(userAgent)) {
                return "Android";
            }

            // iOS detection from: http://stackoverflow.com/a/9039885/177710
            if (/iPad|iPhone|iPod/.test(userAgent) && !window.MSStream) {
                return "iOS";
            }

            return "unknown";
        }


        var w=window,
            d=document,
            e=d.documentElement,
            g=d.getElementsByTagName('body')[0],
            x=w.innerWidth||e.clientWidth||g.clientWidth,
            y=w.innerHeight||e.clientHeight||g.clientHeight;

        var moblock = document.getElementById('mob-app');
        var appimgblock = document.getElementById('app-img');
        var appiclick = document.getElementById('el-i');
        if(x <= 768){

            /*MOBILE APP REDIRECT START*/

            var cookie = getCookie('mobileApp');
            if(cookie == null){
                setCookie('mobileApp', 'true', 300);
                if(getMobileOperatingSystem() == 'Android'){
                    window.location.href = "market://details?id=com.balticlivecam.android.app&referrer=utm_source%3Dbalticlivecam_site";
                    setTimeout(function () {
                        window.open('https://play.google.com/store/apps/details?id=com.balticlivecam.android.app&referrer=utm_source%3Dbalticlivecam_site', '_blank');
                    }, 1000)
                }/*else if(getMobileOperatingSystem() == 'iOS'){
                    window.open('https://itunes.apple.com/lv/app/balticlivecam/id1159702242?mt=8', '_blank');
                }*/

            }

            /*MOBILE APP REDIRECT END*/

            moblock.style.display = 'block';
            if(getMobileOperatingSystem() == 'Android'){
                var appimg = '<a href="https://itunes.apple.com/lv/app/balticlivecam/id1159702242?mt=8"><img src="<? echo get_template_directory_uri() ?>/images/android-app.png" alt="button image"></a>';
            }else if(getMobileOperatingSystem() == 'iOS'){
                var appimg = '<a href="https://play.google.com/store/apps/details?id=com.balticlivecam.android.app&hl=en"><img src="<? echo get_template_directory_uri() ?>/images/android-app.png" alt="button image"></a>';
            }
            appimgblock.innerHTML = appimg;
        }
        appiclick.addEventListener('click', function() {
            moblock.style.display = 'none';
        }, false
        );


</script>


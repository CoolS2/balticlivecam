<?php
function add_this_script_header()
{ ?>
    <!--- FlowPlayer --->
    <link rel="stylesheet" href="<?php echo get_template_directory_uri() ?>/css/slick.css">
    <!--- FlowPlayer End --->
<?php }

add_action('wp_head', 'add_this_script_header', 20);

function add_this_script_footer()
{ ?>


    <script src="<?php echo get_template_directory_uri() ?>/js/slick.min.js"></script>

    <script>

        $(document).ready(function () {
            $('.slider-clients').slick({
                slidesToShow: 4,
                slidesToScroll: 1,
                autoplay: true,
                autoplaySpeed: 2000,
                responsive: [
                    {
                        breakpoint: 768,
                        settings: {
                            slidesToShow: 2
                        }
                    }
                ]
            });
        });
    </script>


<?php }

add_action('wp_footer', 'add_this_script_footer', 20);

/* Template Name: Business - PAGE */
get_header();
?>

    <div class="site">
        <?php get_template_part("partials/nav"); ?>

        <div class="container">
            <div class="bis-page">
                <h1><?php the_title(); ?></h1>
                <?php the_content(); ?>
            </div>
            <?php get_template_part("partials/about_contact") ?>

            <?php
            $curid = get_the_ID();
            $args = array(
                'posts_per_page' => '3',
                'post__not_in' => array(-$curid),
                'order' => 'ASC',
                'post_type' => 'page',
                'post_status' => array('published'),
                'meta_query' => array(
                    array(
                        'key' => '_wp_page_template',
                        'value' => 'Business-page.php',
                        'compare' => '=',
                    ),
                ),
            );


            $query = new WP_Query($args);
            // The Loop
            if ($query->have_posts()) {
                ?>

                <div class="possibilities">
                    <h6><?php __("Business possibilities", "blc") ?></h6>
                    <div class="possibilities-blocks">
                        <?php

                        while ($query->have_posts()) {
                            $query->the_post();
                            ?>


                            <div class="possibilities-block col-sm-12 col-md-6 col-lg-4">
                                <div class="cont">
                                    <div>

                                        <div class="black-possibilities-block">
                                            <div class="inner-black-possibilities">
                                                <p><?php the_title(); ?></p>
                                                <a href="<? echo esc_url(get_permalink()); ?>"><?php _e('see more', 'blc') ?></a>
                                            </div>
                                        </div>
                                        <img src="<?php the_post_thumbnail_url("large"); ?>" alt=""/>
                                    </div>
                                </div>
                            </div>
                            <?php
                        }

                        ?>


                    </div>
                </div>
                <?php

            }
            wp_reset_postdata();

            ?>

            <?php echo blc_partners_func(); ?>


        </div>
        <?php get_template_part("partials/main/footer"); ?>
    </div>


<?
get_footer();
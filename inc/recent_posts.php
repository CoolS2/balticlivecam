<?php

Class My_Recent_Posts_Widget extends WP_Widget_Recent_Posts
{

    function widget($args, $instance)
    {

        if (!isset($args['widget_id'])) {
            $args['widget_id'] = $this->id;
        }

        $title = (!empty($instance['title'])) ? $instance['title'] : __('Recent Posts');

        /** This filter is documented in wp-includes/widgets/class-wp-widget-pages.php */
        $title = apply_filters('widget_title', $title, $instance, $this->id_base);

        $number = (!empty($instance['number'])) ? absint($instance['number']) : 5;
        if (!$number)
            $number = 5;
        $show_date = isset($instance['show_date']) ? $instance['show_date'] : false;

        /**
         * Filter the arguments for the Recent Posts widget.
         *
         * @since 3.4.0
         *
         * @see WP_Query::get_posts()
         *
         * @param array $args An array of arguments used to retrieve the recent posts.
         */
        $r = new WP_Query(apply_filters('widget_posts_args', array(
            'posts_per_page' => $number,
            'no_found_rows' => true,
            'post_status' => 'publish',
            'ignore_sticky_posts' => true
        )));

        if ($r->have_posts()) :
            ?>
            <?php echo $args['before_widget']; ?>
            <?php if ($title) {
            echo $args['before_title'] . $title . $args['after_title'];
        } ?>
            <ul>
                <?php while ($r->have_posts()) : $r->the_post(); ?>

                    <li>
                        <a href="<?php the_permalink(); ?>">
                            <?php the_post_thumbnail(); ?>

                            <div class="href_blog">
                                <?php get_the_title() ? the_title() : the_ID(); ?>

                            </div>
                        </a>

                    </li>

                <?php endwhile; ?>
            </ul>
            <?php echo $args['after_widget']; ?>
            <?php
// Reset the global $the_post as this query will have stomped on it
            wp_reset_postdata();

        endif;
    }
}

function my_recent_widget_registration()
{
    unregister_widget('WP_Widget_Recent_Posts');
    register_widget('My_Recent_Posts_Widget');
}

add_action('widgets_init', 'my_recent_widget_registration');

// AJAX POSTS

function more_post_ajax()
{

    $ppp = (isset($_REQUEST["ppp"])) ? $_REQUEST["ppp"] : 3;
    $page = (isset($_REQUEST['pageNumber'])) ? $_REQUEST['pageNumber'] : 1;
    $cat = $_REQUEST['cat'];

    $page++;
    header("Content-Type: text/html");

    $args = array(
        //'suppress_filters' => true,
        'category__in' => $cat,
        'post_type' => 'post',
        'posts_per_page' => $ppp,
        'post_status' => 'publish',
        'paged' => $page,
    );

    $loop = new WP_Query($args);

    if ($loop->have_posts()) : while ($loop->have_posts()) : $loop->the_post();
        ?>
        <div class="col-md-4 small-blog">
            <div class="blog-2">
                <a href="<?php the_permalink(); ?>">
                    <img src="<? the_post_thumbnail_url('medium'); ?>" alt="#">
                    <h3><?php the_title(); ?></h3>
                </a>
            </div>
        </div>


<?php
    endwhile;
    endif;
    wp_reset_postdata();
    die();
}

add_action('wp_ajax_nopriv_more_post_ajax', 'more_post_ajax');
add_action('wp_ajax_more_post_ajax', 'more_post_ajax');
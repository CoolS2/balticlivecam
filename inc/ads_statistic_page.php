<?php
/** Step 2 (from text above). */
add_action( 'admin_menu', 'ads_statistic' );

/** Step 1. */
function ads_statistic() {
    add_menu_page('Ads statistic', 'Статистика по видео рекламе', 10, 'ads_stats', 'ads_stats_func', '', 5);
}
/** Step 3. */
function ads_stats_func()
{
    wp_enqueue_media();
    if (!current_user_can('manage_options')) {
        wp_die(__('You do not have sufficient permissions to access this page.'));
    }
    global $wpdb;
    if($_POST['date']){
        $date = $_POST['date'];
        $select = $wpdb->get_results('SELECT * FROM ads_statistic WHERE date_t = "'.$date.'"');
    }else{
        $date = date('Y-m-d');
        $select = $wpdb->get_results('SELECT * FROM ads_statistic WHERE date_t = "'.$date.'"');
    }
    $date = date('Y-m-d');
    ?>

    <div id="table_wrapper" class="table_wrapper">

        <ul class="file-list">
            <?php
                foreach(glob($_SERVER['DOCUMENT_ROOT'].'ads_stat/*.*') as $file) {

                    $file_name = substr($file, - 19);
                    echo '<li><a href="'.get_home_url().'/ads_stat/'.$file_name.'" download>'.$file_name.'</a></li>';
                }
            ?>
        </ul>

        <form action="#" method="POST">
            <div>
                <input type="date" name="date" min="<?=date('Y-m')?>-01" max="<?=$date?>">
                <input type="submit" value="Получить">
            </div>
        </form>

    <table id="big_table">
        <thead>
        <tr class="table-header">
            <th>ID</th>
            <th>Name</th>
            <th>Load</th>
            <th>Error</th>
            <th>Skipped</th>
            <th>Click</th>
            <th>Complete</th>
        </tr>
        </thead>
        <tbody>

<?php
    foreach ($select as $key => $val){
        echo '<tr>
                <td>'.$val->id.'</td>
                <td>'.$val->name_ads.'</td>
                <td>'.$val->load_t.'</td>
                <td>'.$val->error_t.'</td>
                <td>'.$val->skipped_t.'</td>
                <td>'.$val->click_t.'</td>
                <td>'.$val->complete_t.'</td>
              </tr>';
    }
    ?>
        </tbody>
    </table>
    </div>
    <style>
        .file-list li{
            display: inline-block;
            padding-left: 10px;
        }
        #table_wrapper{
            max-width: 728px;
            margin: 0 auto;
        }
        #big_table{
            width: 100%;
            font-size: 20px;
            text-align: center;
        }
        .table_wrapper table td{
            padding: 10px;
        }
        .table_wrapper table tr:nth-child(even) {
            background-color: #f2f2f2
        }
        .table_wrapper table tr:hover {
            background-color: rgba(141, 215, 244, 0.2);
        }
        .table_wrapper table{
            width: 100%;
            border-collapse: collapse;
            text-align: center;
        }
    </style>
    <?php
}

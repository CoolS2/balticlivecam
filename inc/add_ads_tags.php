<?php
/** Step 2 (from text above). */
add_action( 'admin_menu', 'add_ads_tags' );

/** Step 1. */
function add_ads_tags() {
    add_menu_page('Add Ads tags', 'Добавить рекламные теги', 10, 'add_ads', 'add_ads_tags_func', '', 5);
}
/** Step 3. */
function add_ads_tags_func()
{
    wp_enqueue_media();
    if (!current_user_can('manage_options')) {
        wp_die(__('You do not have sufficient permissions to access this page.'));
    }
    global $wpdb;
    $wpdb->show_errors();

    if(isset($_POST)){

        if($_POST['formSave']){
            $country = htmlspecialchars(trim($_POST['country']));
            $tags = trim($_POST['tags']);
            $company = htmlspecialchars(trim($_POST['company']));
            $priority = htmlspecialchars(trim($_POST['priority']));


                $insert = $wpdb->insert(
                    'ads_tags',
                    array(
                        'country' => $country,
                        'company' => $company,
                        'priority' => $priority,
                        'tags' => $tags
                    )
                );
                if($insert){
                    $notification = '<h3 style="color:green;">Успешно добавлено</h3>';
                }else{
                    $notification = '<h3 style="color:red;">Ошибка</h3>';
                    $wpdb->print_error();
                }


        }

        if($_POST['action'] == 'delete'){
            $id = htmlspecialchars(trim(stripcslashes($_POST['ids'])));
            $id = explode(',', $id);
            $ids = implode( ',', array_map( 'absint', $id ) );
            $delete = $wpdb->query( "DELETE FROM ads_tags WHERE ID IN($ids)" );
            if($delete){
                $notification = '<h3 style="color:green;">Успешно удалено</h3>';
            }else{
                $notification = '<h3 style="color:red;">Ошибка</h3>';
                $wpdb->print_error();
            }
        }

        if($_POST['stattimeform']){
            $starttime = trim(htmlspecialchars($_POST['starttime']));
            $starttimeembed = trim(htmlspecialchars($_POST['starttimeembed']));
            $intervaltime = trim(htmlspecialchars($_POST['intervaltime']));
            $intervaltimeembed = trim(htmlspecialchars($_POST['intervaltimeembed']));
            $tabmute = trim(htmlspecialchars($_POST['mutetab']));

            add_option("startTime", $starttime, '', 'yes');
            update_option("startTime", $starttime);

            add_option("startTimeEmbed", $starttimeembed, '', 'yes');
            update_option("startTimeEmbed", $starttimeembed);

            add_option("intervalTime", $intervaltime, '', 'yes');
            update_option("intervalTime", $intervaltime);

            add_option("intervalTimeEmbed", $intervaltimeembed, '', 'yes');
            update_option("intervalTimeEmbed", $intervaltimeembed);

            add_option("muteTab", $tabmute, '', 'yes');
            update_option("muteTab", $tabmute);
        }

        $getAll = $wpdb->get_results('SELECT * FROM ads_tags ORDER BY country');
    }

    ?>
    <div class="content">
        <?=$notification?>
        <table>
            <tr>
                <th>ID</th>
                <th>Страна</th>
                <th>Компания</th>
                <th>Приоритет</th>
                <th>Теги</th>
                <th>Чек</th>
            </tr>
            <?php
            foreach ($getAll as $key => $val){
                ?>
                <tr>
                    <td class="id"><?=$val->id?></td>
                    <td><?=$val->country?></td>
                    <td><?=$val->company?></td>
                    <td class="change_priority"><input type="number" value="<?=$val->priority?>"></td>
                    <td><?=$val->tags?></td>
                    <td class="check_t"><input type="checkbox" name="checkboxtable" class="checkbox_check"></td>
                </tr>
                <?php
            }
            ?>

        </table>

        <div class="delete-block">
            <button id="delete">Удалить</button>
        </div>



        <h3>Время рекламы</h3>

        <form action="#" method="POST">
            <label for="starttime">Время старта на <b>сайте</b></label>
            <input type="number" name="starttime" id="starttime" placeholder="20" value="<?=get_option("startTime");?>"> в секундах<br>

            <label for="starttimeembed">Время старта на <b>эмбэдах</b></label>
            <input type="number" name="starttimeembed" id="starttimeembed" placeholder="20" value="<?=get_option("startTimeEmbed");?>"> в секундах<br>

            <label for="intervaltime">Время интервала на <b>сайте</b></label>
            <input type="number" name="intervaltime" id="intervaltime" placeholder="20" value="<?=get_option("intervalTime");?>"> в секундах<br>

            <label for="intervaltimeembed">Время интервала на <b>эмбэдах</b></label>
            <input type="number" name="intervaltimeembed" id="intervaltimeembed" placeholder="20" value="<?=get_option("intervalTimeEmbed");?>"> в секундах<br>

            <label for="mutetab">Отключать звук рекламы, если в другой вкладке</label>
            <input type="checkbox" name="mutetab" id="mutetab" <?php if(get_option("muteTab") == 'on'){echo 'checked';} ?>><br>

            <button name="stattimeform" value="stattimeform">Сохранить</button>
        </form>

        <h3>Добавить новый тег</h3>
        <form action="#" method="POST" id="formSave">
            <label for="country">Код страны, если надо все страны, пишем ALL</label>
            <input type="text" name="country" id="country" placeholder="LV">

            <label for="company">Название компании</label>
            <input type="text" name="company" id="company" placeholder="Adsense">

            <label for="priority">Приоритет</label>
            <input type="number" name="priority" id="priority" placeholder="1">

            <label for="tags">Тэг</label>
            <textarea name="tags" id="tags" rows="5"></textarea>

            <button name="formSave" value="formSave">Сохранить</button>
        </form>
    </div>

    <script>
        jQuery(function () {
            jQuery('#delete').on('click', function () {
                var array = [];
                jQuery('tr').each(function () {
                    if (jQuery(this).find('.check_t').children('.checkbox_check').is(':checked')) {
                        var id = jQuery(this).find('.id').html();
                        array.push(id);
                    }
                });
                jQuery('<form action="#" method="POST"><input type="hidden" name="ids" value='+array+'><input type="hidden" name="action" value="delete"></form>').appendTo('body').submit();
            });

            var ajaxurl = '<?php echo admin_url('admin-ajax.php'); ?>';
            jQuery('.change_priority input').on('keyup', function () {
                var parent = jQuery(this).closest('tr');
                var id = parent.find('.id').html();
                var priority = jQuery(this).val();
                jQuery.ajax({
                    url: ajaxurl,
                    type:"POST",
                    data: {action:'update_tag_priority', id: id, priority: priority},
                    cache: false,
                    beforeSend: function() {
                        parent.css({
                            outline: "0px solid transparent"
                        }).animate({
                            outlineWidth: '1px',
                            outlineColor: 'green'
                        }, 500);
                    },
                    success: function (data) {
                        if(data == 'success'){
                            parent.animate({
                                outlineWidth: '0px',
                                outlineColor: '#fff'
                            }, 500);
                        }else{
                            parent.animate({
                                outlineWidth: '1px',
                                outlineColor: 'red'
                            }, 500);
                        }

                    },
                    error: function() { // if error occured
                        parent.animate({
                            outlineWidth: '1px',
                            outlineColor: 'red'
                        }, 500);
                    }
                });
            });
        });
    </script>
    <style>
        .content{
            max-width: 1200px;
            margin: 20px auto;
            padding: 20px;
            background: #fff;
        }
        .content h3{
            text-align: center;
        }
        #formSave label, #formSave input, #formSave textarea, #formSave button{
            display: block;
        }
        #formSave textarea{
            width: 100%;
        }
        #formSave button{
            margin: 15px auto;
        }
        .content table th{
            text-transform: uppercase;
        }
        .content table tr:nth-child(even) {
            background-color: #f2f2f2
        }
        .content table tr:hover {
            background-color: rgba(145, 216, 248, 0.2);
        }
        .content table td{
            padding: 10px;
        }
        .content table{
            width: 100%;
            border-collapse: collapse;
        }
        .change_priority{
            max-width: 20px;
        }
        .change_priority input{
            width: 100%;
        }
        .delete-block{
            text-align: right;
        }
    </style>
    <?php
}
function update_tag_priority_func(){

    global $wpdb;
    $priority = htmlspecialchars(trim($_POST['priority']));
    $id = htmlspecialchars(trim($_POST['id']));

    $update = $wpdb->update(
        'ads_tags',
        array(
            'priority' => $priority
        ),
        array( 'id' => $id )
    );

    if($update){
        die('success');
    }else{
        die('false');
    }

}

add_action('wp_ajax_nopriv_update_tag_priority', 'update_tag_priority_func');
add_action('wp_ajax_update_tag_priority', 'update_tag_priority_func');

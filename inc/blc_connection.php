<?php


class blc_connection
{
    private $key = "";
    private $secret = "";
    private $server = 'https://auth1.balticlivecam.com';

    public $secure = false;

    /**
     * blc_connection constructor.
     * @param $key  Parameter Key access For BLC API
     * @param $secret Parameter Secret For BLC API Key
     */
    public function __construct($key,$secret)
    {
        $this->key = $key;
        $this->secret = $secret;
    }


    private function makeRequest($command, $args=null)
    {
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $this->server . '/service/' . $command);
        curl_setopt($ch, CURLOPT_POST, 1);
        $post = [
            'key' => $this->key,
            'secret' => $this->secret,
            'secure' => $this->secure
        ];
        if($args) {
            $post =  array_merge($post, $args);
        }
        curl_setopt($ch, CURLOPT_POSTFIELDS,
            $post);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        $server_output = curl_exec($ch);
        if (curl_errno($ch)) {
            $server_output = curl_error($ch);
        }
        curl_close($ch);
        return json_decode( $server_output);
    }


    /**
     * Get Camera Url and banner Configs
     * @param $name Parameter Camera Name
     * @param $ip Parameter  Client Ip
     * @return mixed
     */
    public function GetCameraUrl($name,$ip){
        return  $this->makeRequest('get_token_full',['ip'=>$ip,'name'=>$name]);
    }

    /**
     * Get allowed Camera List
     * @return Array Array of Camera Names
     */
    public function GetCameraList(){
        return $this->makeRequest('get_camlist');
    }

}

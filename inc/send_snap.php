<?php
function send_snap_func(){
  if(isset($_POST['to']))
    {
  $to = htmlspecialchars($_POST['to']);
  $subject = $_POST['subject'];
  $text_m = htmlspecialchars($_POST['text']);
  $url = $_POST['url'];
  //$img = trim($_POST['image']);
  $title = $_POST['title'];
  $title = substr($title, strrpos($title, ' ') + 1);

        $attachments = array(ABSPATH.'images/snaps/snap_'.$title.'.jpg');
        $subject = 'Snapshot from '.$subject;
        $message = 'Snapshot from '.$url. "<br />" . $text_m;
        $headers  = 'MIME-Version: 1.0' . "\r\n";
        $headers .= 'Content-type: text/html; charset=utf-8' . "\r\n";
        $headers .= 'From: Balticlivecam <info@balticlivecam.com>' . "\r\n";
        $send = wp_mail( $to, $subject, $message, $headers, $attachments);

  if($send){
    $return = 'Mail Sent Successfully';
  }else{
    $return = 'Mail Not Sent';
  }

die($return);
}

}
add_action('wp_ajax_send_snap', 'send_snap_func');
add_action('wp_ajax_nopriv_send_snap', 'send_snap_func');
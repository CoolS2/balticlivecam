<?php

function camera_meta_box($post)
{

    // Get the page template post meta
    $page_template = get_post_meta($post->ID, '_wp_page_template', true);
    // If the current page uses our specific
    // template, then output our custom metabox
    if (('camera.php' == $page_template) || ('user-content/camera-second.php' == $page_template) || ('ads-camera-test.php' == $page_template)) {
        add_meta_box(
            'camera-custom-box', // Metabox HTML ID attribute
            'Special Post Meta', // Metabox title
            'camera_meta_box_template', // callback name
            'page', // post type
            'normal', // context (advanced, normal, or side)
            'high' // priority (high, core, default or low)
        );
    }
}


function camera_meta_box_template($object, $box)
{
    wp_nonce_field(basename(__FILE__), 'blc_post_class_nonce');
    $args = array(
        'post_type' => array('camera_partner'),
        'nopaging' => true
    );
    $partners = get_posts($args);
    $current_partner = esc_attr(get_post_meta($object->ID, 'blc_camera_partner', true));
    $current_partner2 = esc_attr(get_post_meta($object->ID, 'blc_camera_partner2', true));
    $non_hls = get_post_meta($object->ID, 'blc_camera_nonhlsjs', true);
    $under_construction = get_post_meta($object->ID, 'under_construction', true);
    $camera_overlay = get_post_meta($object->ID, 'camera_overlay', true);
    $hide_logo = get_post_meta($object->ID, 'hide_logo', true);
    $our_camera = get_post_meta($object->ID, 'blc_camera_notour', true);
    $ads_embed = get_post_meta($object->ID, 'blc_camera_embed', true);
    $ads_toggle = get_post_meta($object->ID, 'blc_ads_toggle', true);
    ?>
    <p>
        <select id="camera_type" name="camera_type">
            <option value="" <?php echo(0 == $our_camera ? 'selected="selected"' : '') ?>>Normal Camera</option>
            <option value="1" <?php echo(1 == $our_camera ? 'selected="selected"' : '') ?>>Not Our Camera Our Player
            </option>
            <option value="2" <?php echo(2 == $our_camera ? 'selected="selected"' : '') ?>>Youtube</option>
            <option value="3" <?php echo(3 == $our_camera ? 'selected="selected"' : '') ?>>Iframe</option>
        </select>
    </p>
    <div class="camera_params" id="params">
        <h3>
            Our Camera Settings
        </h3>

        <label
            for="blc_camera_id">BLC Camera Name</label>
        <br/>
        <input type="text" name="blc_camera_id" id="blc_camera_id"
               value="<?php echo esc_attr(get_post_meta($object->ID, 'blc_camera_id', true)); ?>" size="50"/>

        <label for="blc_camera_partner"><?php _e("Camera Partner", 'blc'); ?></label>
        <select type="text" name="blc_camera_partner" id="blc_camera_partner">
            <option>No Partner</option>
            <?php
            foreach ($partners as $partner) {
                echo '<option ' . ($partner->ID == $current_partner ? 'selected="selected"' : '') . ' value="' . $partner->ID . '">' . $partner->post_title . '</option>';
            }
            ?>
        </select>

    <div>
        <label for="camera_overlay">Camera overlay</label>
        <input id="camera_overlay" name="camera_overlay" type="text" value="<?php echo $camera_overlay; ?>"/>
        <input type="button" class="buttoncam" value="Select image">
    </div>
        <script>
            jQuery(document).ready(function () {

                jQuery('.buttoncam').click(function (e) {
                    e.preventDefault();
                    var inp = jQuery(this).siblings('input');
                    var image = wp.media({
                        title: 'Upload Image',
                        multiple: false
                    }).open()
                        .on('select', function (e) {
                            var uploaded_image = image.state().get('selection').first();
                            var image_url = uploaded_image.toJSON().url;
                            inp.val(image_url);
                        });
                });

            });
        </script>

        <label for="blc_camera_partner2"><?php _e("Camera Partner", 'blc'); ?>2</label>
        <select type="text" name="blc_camera_partner2" id="blc_camera_partner2">
            <option>No Partner</option>
            <?php
            foreach ($partners as $partner) {
                echo '<option ' . ($partner->ID == $current_partner2 ? 'selected="selected"' : '') . ' value="' . $partner->ID . '">' . $partner->post_title . '</option>';
            }
            ?>
        </select>

        <label
            for="blc_camera_id">Turn Off HLS.JS = <?php echo $non_hls;?></label>

        <input id="blc_camera_nonhlsjs" name="blc_camera_nonhlsjs" type="checkbox" value="1"
                 <?php echo ($non_hls=="1") ? "checked='checked'" : ""; ?>/>

        <label
            for="under_construction">Under construction = <?php echo $under_construction;?></label>

        <input id="under_construction" name="under_construction" type="checkbox" value="1"
                  <?php echo ($under_construction=="1") ? "checked='checked'" : ""; ?>/>

        <label for="hide_logo">Hide blc logo = <?php echo $hide_logo;?></label>
        <input id="hide_logo" name="hide_logo" type="checkbox" value="1" <?php echo ($hide_logo=="1") ? "checked='checked'" : ""; ?>/>

        <label
            for="blc_camera_embed">Turn On EMBED = <?php echo $ads_embed;?></label>

        <input id="blc_camera_embed" name="blc_camera_embed" type="checkbox" value="1"
            <?php echo ($ads_embed=="1") ? "checked='checked'" : ""; ?>/>



    </div>

    <div class="camera_params" id="params1">
        <h3>
            Not Our Camera Settings
        </h3>
        <p>

            <label
                for="blc_camera_id">Thumb Name</label>
            <br/>
            <input type="text" name="blc_thumb_name" id="blc_thumb_name"
                   value="<?php echo esc_attr(get_post_meta($object->ID, 'blc_camera_id', true)); ?>" size="50"/>
        </p>

        <p>
            <label
                for="blc_camera_notour_address"><?php _e("Not Our Camera Address", 'blc'); ?></label>
            <input type="text" name="blc_camera_notour_address" id="blc_camera_notour_address"
                   value="<?php echo esc_attr(get_post_meta($object->ID, 'blc_camera_notour_address', true)); ?>"
                   size="50"/>
        </p>
    </div>

    <div class="camera_params" id="params2">
        <h3>
            Youtube
        </h3>
        <label
            for="blc_embed_code">Youtube Camera</label>
        <br/>
        <textarea name="blc_embed_code" id="blc_embed_code" style="width:400px;" rows="10">
            <?php echo get_post_meta($object->ID, 'blc_embed_code', true); ?>
        </textarea>
        <p>

            <label
                for="blc_camera_id">Thumb Name</label>
            <br/>
            <input type="text" name="blc_thumb_url" id="blc_thumb_url"
                   value="<?php echo esc_attr(get_post_meta($object->ID, 'blc_camera_id', true)); ?>" size="50"/>
            <input id="select_thumb_url" name="select_thumb_url" type="button" value="Images"/>
        </p>

        <script>
            jQuery(document).ready(function () {

                jQuery('#select_thumb_url').click(function (e) {
                    e.preventDefault();
                    var image = wp.media({
                        title: 'Upload Image',
                        // mutiple: true if you want to upload multiple files at once
                        multiple: false
                    }).open()
                        .on('select', function (e) {
                            // This will return the selected image from the Media Uploader, the result is an object
                            var uploaded_image = image.state().get('selection').first();
                            // We convert uploaded_image to a JSON object to make accessing it easier
                            // Output to the console uploaded_image
                            var image_url = uploaded_image.toJSON().url;
                            // Let's assign the url value to the input field
                            jQuery('#blc_thumb_url').val(image_url);
                        });
                });

            });
        </script>

    </div>

    <div class="camera_params" id="params3">
        <h3>Iframe</h3>
        <label for="blc_iframe_code">Iframe Camera</label>
        <br/>
        <textarea name="blc_iframe_code" id="blc_iframe_code" style="width:400px;" rows="10">
            <?php echo get_post_meta($object->ID, 'blc_iframe_code', true); ?>
        </textarea>
    </div>

    <div>
        <label
                for="blc_ads_toggle">Turn Off ADS = </label>

        <input id="blc_ads_toggle" name="blc_ads_toggle" type="checkbox" value="1"
            <?php echo ($ads_toggle=="1") ? "checked='checked'" : ""; ?>/>
    </div>

    <script>
        jQuery(document).ready(function () {
                jQuery(".camera_params").hide();
                jQuery("#params" + jQuery("#camera_type").val()).show();
                jQuery("#camera_type").change(function () {
                    jQuery(".camera_params").hide();
                    jQuery("#params" + jQuery(this).val()).show();
                });
            }
        )
    </script>


    <?php
}

function camera_meta_box_save($post_id)
{
    if (is_admin()) {
        if (!isset($_POST['blc_post_class_nonce']) || !wp_verify_nonce($_POST['blc_post_class_nonce'], basename(__FILE__)))
            return $post_id;

        $camera_type = (isset($_POST['camera_type']) ? $_POST['camera_type'] : '');
        $meta_key = 'blc_camera_notour';
        $meta_value = get_post_meta($post_id, $meta_key, true);
        change_meta_key($post_id, $meta_key, $meta_value, $camera_type);


        if ($camera_type == 0) {

            $meta_key = 'blc_camera_id';
            $new_meta_value = (isset($_POST[$meta_key]) ? $_POST[$meta_key] : '');
            $meta_value = get_post_meta($post_id, $meta_key, true);
            change_meta_key($post_id, $meta_key, $meta_value, $new_meta_value);

            $meta_key = 'blc_camera_partner';
            $new_meta_value = (isset($_POST[$meta_key]) ? $_POST[$meta_key] : '');
            $meta_value = get_post_meta($post_id, $meta_key, true);
            change_meta_key($post_id, $meta_key, $meta_value, $new_meta_value);

            $meta_key = 'blc_camera_partner2';
            $new_meta_value = (isset($_POST[$meta_key]) ? $_POST[$meta_key] : '');
            $meta_value = get_post_meta($post_id, $meta_key, true);
            change_meta_key($post_id, $meta_key, $meta_value, $new_meta_value);
        }

        if ($camera_type == 1) {
            $meta_key = 'blc_camera_id';
            $new_meta_value = (isset($_POST["blc_thumb_name"]) ? $_POST["blc_thumb_name"] : '');
            $meta_value = get_post_meta($post_id, $meta_key, true);
            change_meta_key($post_id, $meta_key, $meta_value, $new_meta_value);

            $meta_key = 'blc_camera_notour_address';
            $new_meta_value = (isset($_POST[$meta_key]) ? $_POST[$meta_key] : '');
            $meta_value = get_post_meta($post_id, $meta_key, true);
            change_meta_key($post_id, $meta_key, $meta_value, $new_meta_value);
        }
        if ($camera_type == 2) {

            $meta_key = 'blc_camera_id';
            $new_meta_value = (isset($_POST["blc_thumb_url"]) ? $_POST["blc_thumb_url"] : '');
            $meta_value = get_post_meta($post_id, $meta_key, true);
            change_meta_key($post_id, $meta_key, $meta_value, $new_meta_value);

            $meta_key = 'blc_embed_code';
            $new_meta_value = (isset($_POST[$meta_key]) ? $_POST[$meta_key] : '');
            $meta_value = get_post_meta($post_id, $meta_key, true);
            change_meta_key($post_id, $meta_key, $meta_value, $new_meta_value);
        }

        if ($camera_type == 3) {
            $meta_key = 'blc_camera_id';
            $new_meta_value = (isset($_POST["blc_thumb_url"]) ? $_POST["blc_thumb_url"] : '');
            $meta_value = get_post_meta($post_id, $meta_key, true);
            change_meta_key($post_id, $meta_key, $meta_value, $new_meta_value);

            $meta_key = 'blc_iframe_code';
            $new_meta_value = (isset($_POST[$meta_key]) ? $_POST[$meta_key] : '');
            $meta_value = get_post_meta($post_id, $meta_key, true);
            change_meta_key($post_id, $meta_key, $meta_value, $new_meta_value);
        }

        $new_meta_value = (isset($_POST['blc_camera_nonhlsjs']) ? $_POST['blc_camera_nonhlsjs'] : '');
        $meta_key = 'blc_camera_nonhlsjs';
        $meta_value = get_post_meta($post_id, $meta_key, true);
        change_meta_key($post_id, $meta_key, $meta_value, $new_meta_value);

        $new_meta_value = (isset($_POST['under_construction']) ? $_POST['under_construction'] : '');
        $meta_key = 'under_construction';
        $meta_value = get_post_meta($post_id, $meta_key, true);
        change_meta_key($post_id, $meta_key, $meta_value, $new_meta_value);

        $new_meta_value = (isset($_POST['hide_logo']) ? $_POST['hide_logo'] : '');
        $meta_key = 'hide_logo';
        $meta_value = get_post_meta($post_id, $meta_key, true);
        change_meta_key($post_id, $meta_key, $meta_value, $new_meta_value);

        $new_meta_value = (isset($_POST['blc_camera_embed']) ? $_POST['blc_camera_embed'] : '');
        $meta_key = 'blc_camera_embed';
        $meta_value = get_post_meta($post_id, $meta_key, true);
        change_meta_key($post_id, $meta_key, $meta_value, $new_meta_value);

        $new_meta_value = (isset($_POST['blc_ads_toggle']) ? $_POST['blc_ads_toggle'] : '');
        $meta_key = 'blc_ads_toggle';
        $meta_value = get_post_meta($post_id, $meta_key, true);
        change_meta_key($post_id, $meta_key, $meta_value, $new_meta_value);

        $new_meta_value = (isset($_POST['camera_overlay']) ? $_POST['camera_overlay'] : '');
        $meta_key = 'camera_overlay';
        $meta_value = get_post_meta($post_id, $meta_key, true);
        change_meta_key($post_id, $meta_key, $meta_value, $new_meta_value);

    }
}

add_action('add_meta_boxes_page', 'camera_meta_box');


add_action('publish_page', 'camera_meta_box_save');
add_action('draft_page', 'camera_meta_box_save');
add_action('future_page', 'camera_meta_box_save');

<?php


function auth_token_ajax_request()
{
    // The $_REQUEST contains all the data sent via ajax
    if (isset($_REQUEST)) {
        $id = $_REQUEST['id'];
        $embed = $_REQUEST['embed'];
        $type = get_post_meta($id, 'blc_camera_notour', true);
        $main_referer = $_REQUEST['main_referer'];
        $main_referer = parse_url($main_referer, PHP_URL_HOST);
        $main_referer = str_replace("www.","",$main_referer);
        $name_hd = get_post_meta($id, 'blc_camera_id', true);
        //$non_hls = get_post_meta($id, 'blc_camera_nonhlsjs', true);
        //$camera_embed_ads = get_post_meta($id, 'blc_camera_embed', true);
        //$refer = $_SERVER['HTTP_REFERER'];



        /*CHECK FOR EMBED ADS*/
        /*$domain_exclude = true;
         * global $wpdb;
        $blc_banner_exc = $wpdb->get_row('SELECT banner_exc_id, show_ads FROM blc_banner_exc WHERE host = "'.$main_referer.'"');
        $blc_banner_exc_cameras = $wpdb->get_results('SELECT cam_name FROM blc_banner_exc_cameras WHERE id_exc = "'.$blc_banner_exc->banner_exc_id.'"');
            if($blc_banner_exc_cameras){
                foreach ($blc_banner_exc_cameras as $key => $val){
                    if($val->cam_name == $name_hd){
                        if($blc_banner_exc->show_ads == false){
                            $domain_exclude = false;
                            break;
                        }
                    }
                }
            }*/
        $domain_exclude = true;
        $partner_show = true;
        $logo_show = true;
        $except_cams = false;
        global $wpdb;
        $blc_banner_exc = $wpdb->get_row('SELECT banner_exc_id, show_ads, show_logo, show_partner FROM blc_banner_exc WHERE host = "'.$main_referer.'"');
        $blc_banner_exc_cameras = $wpdb->get_results('SELECT cam_name, except_cams FROM blc_banner_exc_cameras WHERE id_exc = "'.$blc_banner_exc->banner_exc_id.'"');
        if($blc_banner_exc_cameras){
            foreach ($blc_banner_exc_cameras as $key => $val){
                if($val->cam_name == $name_hd){
                    $partner_show = $blc_banner_exc->show_partner;
                    $logo_show = $blc_banner_exc->show_logo;
                    $domain_exclude = $blc_banner_exc->show_ads;
                    //if($val->except_cams == true){
                        $except_cams = true;
                    //}
                    break;
                }
            }
        }
        /*CHECK FOR EMBED ADS END*/


        $country = '';
        $source = '';
        if ($type == 2) {
            ?><script src="<?php echo get_template_directory_uri() ?>/videojs/plugins/videojs-youtube/youtube.min.js"></script><?php
            $embed_from_youtube = get_post_meta($id, 'blc_embed_code', true);

            preg_match('/src="([^"]+)"/', $embed_from_youtube, $match);
            $url = $match[1];
            $type_player = 'video/youtube';
            $techOrder = '["youtube"]';
            $y_controls = ',youtube: { "ytControls": 2 }';
            $n_controls = 'false';
            $source = "sources: [{
                        src: '$url',
                        type: '$type_player'
                    }],";
        } else {

            if ($type == 1) {
                $url = get_post_meta($id, 'blc_camera_notour_address', true);
            } else {
                $remote = $_SERVER["REMOTE_ADDR"];
                $connector = new blc_connection('g549Ga49asg5', 'VoYZeqGNTshEClcZxaH6');
                $connector->secure = true;

                $type_player = 'application/x-mpegurl';
                $techOrder = '["html5"]';
                $y_controls = '';
                $n_controls = 'true';
                $res = $connector->GetCameraUrl($name_hd, $remote);
                $url = "";
                if ($res->result) {
                    $url = esc_url_raw($res->result->url);
                    $country = $res->result->country;
                }

                    $source = "sources: [{
                        src: '$url',
                        type: '$type_player'
                    }],";

                //$url = 'https://edge01.balticlivecam.com/blc/PanoTallinn/index.m3u8?token=1b93489f3d6154a8e916e7a60179ea79:1509459902319';
            }
        }

        global $wpdb;

        if ($type == 2) {
            $db_country = $wpdb->get_results('SELECT tags, company FROM ads_tags WHERE company != "Setupad" ORDER BY priority');
        }else{
            //$db_country = $wpdb->get_results('SELECT tags, company FROM ads_tags WHERE country IN ("'.$country.'", "ALL") ORDER BY priority');
            $db_country = $wpdb->get_results('SELECT tags, company FROM ads_tags WHERE country = "'.$country.'" ORDER BY priority');
            if(!$db_country){
                $db_country = $wpdb->get_results('SELECT tags, company FROM ads_tags WHERE country = "ALL" ORDER BY priority');
            }
        }
            ?>

            <script>
                var ads = [];

                var country = '<?=$country?>';
                var startTime, intervalTime;
                var muteTab = '<?=get_option("muteTab")?>';
                console.log(country);
                <?php
                if($embed == 1){
                ?>
                intervalTime = '<?=get_option("intervalTimeEmbed");?>';
                startTime = '<?=get_option("startTimeEmbed");?>';
                <?php
                }else{
                ?>
                intervalTime = '<?=get_option("intervalTime");?>';
                startTime = '<?=get_option("startTime");?>';

                <?php
                }
                if(!($embed == 1 && $domain_exclude == false && $except_cams == true)) {  // if ads is turn on in admin panel
                    if($_REQUEST['main_referer'] != 'https://realmeteo.ru/' && $_REQUEST['main_referer'] != 'https://www.tallinn.ee/') {
                        foreach ($db_country as $key => $val){
                            ?>
                                ads.push({
                                    name : '<?=$val->tags?>',
                                    company : '<?=$val->company?>',
                                    played : 0
                                });
                            <?php
                        }
                    }
                }
                  ?>
                player = videojs('my-video', {
                    controls: <?=$n_controls?>,
                    autoplay: false,
                    /*controlBar: {
                        volumePanel: {inline: false}
                    },*/
                    preload: 'auto',
                    sourceOrder: true,
                    customControlsOnMobile: false,
                    nativeControlsForTouch: false,
                    hls: {
                        withCredentials: true
                    },
                    <?=$source?>
                    techOrder: <?=$techOrder?>
                    <?=$y_controls?>
                });

                player.on('error', function(e) {
                    console.log('error player');
                    $('.vjs-modal-dialog-content').html("<? _e('Due to technical difficulties, the camera is temporarily unavailable. Sorry for the inconvenience.', 'blc') ?>");
                    $('.vjs-has-started .vjs-poster').show();
                    player.pause();
                    setTimeout(function () {
                        player.ima.requestAds();
                    }, 10000);
                });



                <?php //if ($domain_exclude !== true) { ?>
                $(".fp-logo").show();

                <? //} ?>
                <?php //if ($domain_exclude->show_partner) {  ?>
                $(".fp-sponsor").css("display", "");
                <?php //} ?>
                <?php  if ($domain_exclude == true || $except_cams == true) {   ?>
                //$(".fp_advert").css("display", "none");
                <?php } ?>

                <?php
                if ( wp_is_mobile() ) {
                    ?>var device = 'mobile'; <?php
                }else{
                    ?>var device = 'desktop'; <?php
                }
                ?>

                function when_external_loaded (callback) {
                    if (typeof videoOptions === 'undefined') {
                        setTimeout (function () {
                            when_external_loaded (callback);
                        }, 500); // wait 100 ms
                    } else { callback (); }
                }

                when_external_loaded (function () {
                    setTimeout(videoOptions(ads, device, startTime, intervalTime, muteTab), 500);
                });

            </script>
            <?php

    }
    die();
}

add_action('wp_ajax_auth_token', 'auth_token_ajax_request');
add_action('wp_ajax_nopriv_auth_token', 'auth_token_ajax_request');






<?php
/**
 * Created by PhpStorm.
 * User: Admin
 * Date: 17.06.2016
 * Time: 16:14
 */


$images = ["ACB", "VISITRHODES", "Eglukalns", "wetter_com", "jurmaladum", "Radisson_Blu_logo", "LTV", "Albatross", "Amberton", "AnimalsLife_net", "apollo-logo", "klausk", "Crowne_Plaza_svg", "delfi"
    , "Hales_Turgus", "ilm_ee", "islande_hotel_logo", "latgale-viesnica", "Latgola", "lpb-logo", "oru", "TVNET_logo", "tallinn", "bite", "semarah", "pirita", "imperial"];

function blc_partners_func()
{
    global $images;


        $top_cameras = __("Our partners", "blc");
        $top_cameras_pieces = explode(' ', $top_cameras);
        $top_cameras_first = array_shift(array_values($top_cameras_pieces));
        $top_cameras_last_word = end($top_cameras_pieces);
        if (count($top_cameras_pieces) != 1) {
            $new_title = $top_cameras_first.'<span>'.$top_cameras_last_word.'</span>';
        }else{
            $new_title = '<span>'.$top_cameras_last_word.'</span>';
        }



    $content = '  <div class="main_category">
                <div class="title">
                    <h3 class="animation-element">'.$new_title.'</h3>
                </div>
                <div class="main_partners">
                <ul>';

    foreach ($images as $partner) {
        $content .= '<li><img src="/images/Partners/' . $partner . '_result.png" alt=""/></li>';
    };


    $content .= '</ul>';
    $slider = '<div class="slider-clients">';
    $i = 0;
    foreach ($images as $partner) {
        if (i % 2 == 0) {
            $slider .= '<div>';
        }
        $i++;
        $slider .= '<img src="/images/Partners/' . $partner . '_result.png" alt="" />';
        if (i % 2 == 0) {
            $slider .= '</div>';
        }

    }


    $slider .= '</div>';
    $content .= $slider;

    $content .= '</div> ';
    $content .= '</div> ';

    return $content;
}

add_shortcode('clientscode', 'blc_partners_func');

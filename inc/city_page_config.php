<?php

function city_meta_box($post)
{

    // Get the page template post meta
    $page_template = get_post_meta($post->ID, '_wp_page_template', true);
    // If the current page uses our specific
    // template, then output our custom metabox
    if (('City.php' == $page_template) ||  ('user-content/camera-sub-category.php' == $page_template)) {
        add_meta_box(
            'city-custom-box', // Metabox HTML ID attribute
            'Special Post Meta', // Metabox title
            'city_meta_box_template', // callback name
            'page', // post type
            'normal', // context (advanced, normal, or side)
            'high' // priority (high, core, default or low)
        );
    }
}


function city_meta_box_template($object, $box)
{
    wp_nonce_field(basename(__FILE__), 'blc_post_class_nonce'); ?>
    <p>
        <label
            for="blc-city-id"><?php _e("City Weather Id", 'blc'); ?></label>
        <br/>
        <input type="text" name="blc-city-id" id="blc-city-id"
               value="<?php echo esc_attr(get_post_meta($object->ID, 'blc_city_id', true)); ?>" size="50"/>
    </p>
    <p>
        <label
            for="blc-image-url"><?php _e("City Image Url", 'blc'); ?></label>
        <br/>
        <input type="text" name="blc-image-url" id="blc-image-url"
               value="<?php echo esc_attr(get_post_meta($object->ID, 'blc_image_url', true)); ?>"  style="width:100%"/>
    </p>
    <?php
}

function city_meta_box_save($post_id)
{
    if (is_admin()) {
        if (!isset($_POST['blc_post_class_nonce']) || !wp_verify_nonce($_POST['blc_post_class_nonce'], basename(__FILE__)))
            return $post_id;
        $new_meta_value = (isset($_POST['blc-city-id']) ? $_POST['blc-city-id']:'');
        $meta_key = 'blc_city_id';
        $meta_value = get_post_meta($post_id, $meta_key, true);
        change_meta_key($post_id,$meta_key,$meta_value,$new_meta_value);
        

        $new_meta_value = (isset($_POST['blc-image-url']) ? esc_attr($_POST['blc-image-url']) : '');
        $meta_key = 'blc_image_url';
        $meta_value = get_post_meta($post_id, $meta_key, true);
        change_meta_key($post_id,$meta_key,$meta_value,$new_meta_value);

    }
}

add_action('add_meta_boxes_page', 'city_meta_box');


add_action('publish_page', 'city_meta_box_save');
add_action('draft_page', 'city_meta_box_save');
add_action('future_page', 'city_meta_box_save');
<?php


define("APP_ROOT", $_SERVER['DOCUMENT_ROOT'] . 'weather/cache/');

define("CACHE_DIR", $_SERVER['DOCUMENT_ROOT'] . 'weather/cache/');
define("CACHE_AGE", 3600);

function get_cache_value($path, $force = false)
{
    if (file_exists($path)) {
        $now = time();
        $file_age = filemtime($path);
        if (($now - $file_age) < CACHE_AGE || $force) {
            return file_get_contents($path);
        } else {
            return false;
        }
    } else {
        return false;
    }
}

function set_cache_value($path, $value)
{
    return file_put_contents($path, $value);
}


function kv_euro($city_id)
{

    global $w_city;
    global $w_pic;
    global $w_temperature;
    global $w_wind_speed;
    global $w_humidity;

    $path = CACHE_DIR . 'weather_' . $city_id . '.json';
    $json = get_cache_value($path);


    if (false !== $json) {
        echo "";
        $phpObj = json_decode($json);
//echo $json;
    } else {
        $BASE_URL = "http://api.openweathermap.org/data/2.5/forecast/city";
        $yql_query_url = $BASE_URL . "?id=" . $city_id . "&APPID=8d418bcb497bff22ecbc087ce02c95d7";
        $session = curl_init($yql_query_url);
        curl_setopt($session, CURLOPT_RETURNTRANSFER, true);
        $json = curl_exec($session);

        $phpObj = json_decode($json);
        if ($phpObj) {
            set_cache_value($path, $json);
        } else {
            $json = get_cache_value($path, true);
            $phpObj = json_decode($json);
        }
    }

    $w_city = $phpObj->city->name;
    $w_pic = $phpObj->list[0]->weather[0]->icon;
    $w_temperature = round($phpObj->list[0]->main->temp - 273.15);
    $w_wind_speed = round($phpObj->list[0]->wind->speed);
    $w_humidity = $phpObj->list[0]->main->humidity;


}

function generate_weather($city_id)
{
    kv_euro($city_id);
    ?>
    <div class="weather-camera">
        <img src="/weather/icons/<?php echo $GLOBALS['w_pic']; ?>.png" class="weather-icon"
                alt="Weather Icon"/>
        <div class="weather-data">
        <b><?php echo $GLOBALS['w_temperature']; ?><sup>o</sup>C</b>
        <?php echo $GLOBALS['w_wind_speed']; ?><?php _e('m/s', 'blc') ?>
        </div>

    </div>
    <?php

}


?>
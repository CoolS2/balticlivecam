<?php


function timelapseFunc()
{
    // The $_REQUEST contains all the data sent via ajax
    if (isset($_REQUEST)) {
        $id_en = $_REQUEST['id_en'];
       
        $args = array(
            'post_type' => array('camera_timelapse'),
            'meta_query' => array(
                array(
                    'key' => 'camera_id',
                    'value' => $id_en,
                    'compare' => '=',
                    'type' => 'NUMERIC',
        ),
    ),
);

global $timelapse;
global $post;
$timelapse = new WP_Query($args);
        while ($timelapse->have_posts()) {
                    $timelapse->the_post();
 
            $data[] = [
            name => $post->post_title,
            img => get_post_meta($post->ID, 'blc_timelapse_url', true)
                ];
  
    }
    
    die(json_encode($data));
}
}

add_action('wp_ajax_timelapse_token', 'timelapseFunc');
add_action('wp_ajax_nopriv_timelapse_token', 'timelapseFunc');

function timelapseSingle(){
    if (isset($_REQUEST)) {
    $name = $_REQUEST['name'];
    $id_en = $_REQUEST['id_en'];

    $remote = $_SERVER["REMOTE_ADDR"];
    $connector = new blc_connection('g549Ga49asg5', 'VoYZeqGNTshEClcZxaH6');
    $connector->secure = true;

    $args = array(
            'post_type' => array('camera_timelapse'),
            'meta_query' => array(
                array(
                    'key' => 'camera_id',
                    'value' => $id_en,
                    'compare' => '=',
                    'type' => 'NUMERIC',
        ),
    ),
);


global $timelapse;
global $post;
$timelapse = new WP_Query($args);
        while ($timelapse->have_posts()) {
                    $timelapse->the_post();

                    if($name == $post->post_title){
                        $name = get_post_meta($post->ID, 'blc-timelapse-name', true);
                    $res = $connector->GetCameraUrl($name, $remote);
                    $url = '';
                    $url = esc_url_raw($res->result->url);
                    die($url);
                    }
                    
}
}
}

add_action('wp_ajax_single_timelapse', 'timelapseSingle');
add_action('wp_ajax_nopriv_single_timelapse', 'timelapseSingle');
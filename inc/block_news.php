<?php


add_action('admin_init', 'news_block', 1);
function news_block() {
	 $post_id = $_GET['post'] ? $_GET['post'] : $_POST['post_ID'] ;
	$template_file = get_post_meta($post_id,'_wp_page_template',TRUE);
  // check for a template type
  if ($template_file == 'City.php') {
    add_meta_box( 'extra_fields', 'News block', 'news_block_func', 'page', 'normal', 'high'  );
	}
}
function news_block_func( $post ){
?>
	<div>
				<textarea name="extra[newsblock]" rows="10" style="width: 100%;"><?php echo get_post_meta($post->ID, "newsblock", true); ?></textarea>
			</div>
				<input type="hidden" name="extra_fields_nonce" value="<?php echo wp_create_nonce(__FILE__); ?>" />
        
<?php
}
add_action('save_post', 'news_block_update', 0);
function news_block_update( $post_id ){
    if ( !wp_verify_nonce($_POST['extra_fields_nonce'], __FILE__) ) return false;
    if ( defined('DOING_AUTOSAVE') && DOING_AUTOSAVE  ) return false;
    if ( !current_user_can('edit_post', $post_id) ) return false;
    if( !isset($_POST['extra']) ) return false;
    $_POST['extra'] = array_map('trim', $_POST['extra']);
    foreach( $_POST['extra'] as $key=>$value ){
        if( empty($value) ) {
            delete_post_meta($post_id, $key);
			continue;
			}
        update_post_meta($post_id, $key, $value);
    }
 
    return $post_id;
} 

<?php 

function getAppIos(){
?>
<img src="<? echo get_template_directory_uri() ?>/images/logo.png" alt="logo">

     <div class="app_shop"> 
	    <a href="https://itunes.apple.com/us/app/balticlivecam/id1159702242?mt=8&ign-mpt=uo%3D2">
	        <img src="<? echo get_template_directory_uri() ?>/images/apple-app.jpg" alt="apple">
	    </a>
	    <div class="close_app">X</div>
	    <?php
	    die();
}

add_action('wp_ajax_app_ios', 'getAppIos');
add_action('wp_ajax_nopriv_app_ios', 'getAppIos');

function getAppAndroid(){
	?>
	<img src="<? echo get_template_directory_uri() ?>/images/logo.png" alt="logo">

	<div class="app_shop">
	<a href="https://play.google.com/store/apps/details?id=com.balticlivecam.android.app&hl=en">
		<img src="<? echo get_template_directory_uri() ?>/images/android-app.png" alt="android">
	</a>
	<div class="close_app">X</div>
	<?php
	die();
}

add_action('wp_ajax_app_android', 'getAppAndroid');
add_action('wp_ajax_nopriv_app_android', 'getAppAndroid');

function send_mistake_func(){
	$text_area = htmlspecialchars($_POST['text_area']);
	$user_info = stripcslashes($_POST['user_info']);
	$email = htmlspecialchars($_POST['email']);
	$url = htmlspecialchars($_POST['full_url']);

    $message = $email.'<br>'.$text_area.'<br>'.$url;

	$user_info = json_decode($user_info);

    foreach ($user_info as $key => $val){
        $message .= '<br>'.$key.': '.$val;
    }


	$subject = 'Report a mistake';
	$to = 'team@balticlivecam.com';
	$headers = array('Content-Type: text/html; charset=UTF-8');
	$send_mail = wp_mail( $to, $subject, $message, $headers );
	if($send_mail){
		die('ok');
	}else{
		die('error');
	}
}

add_action('wp_ajax_send_mistake', 'send_mistake_func');
add_action('wp_ajax_nopriv_send_mistake', 'send_mistake_func');

function send_statistics_func(){

	$param = htmlspecialchars(trim($_POST['param']));
	$name = htmlspecialchars(trim($_POST['name']));
	$date = date('Y-m-d');
	$cur_month = date('m');
	global $wpdb;

	function updateStatisticDatabase($param, $name, $date, $wpdb){
		if($param != 'error'){
			$param = $param.'_t';
			if($name == ''){
				$name = 'without-ads-name';
			}
			$ads_name = $wpdb->get_var('SELECT name_ads FROM ads_statistic WHERE name_ads = "'.$name.'" AND date_t = "'.$date.'"');
			// CHECK ADS NAME
			if(!$ads_name){
				// insert
				$wpdb->insert(
					'ads_statistic',
					array(
						'name_ads' => $name,
						'date_t'   => $date,
						$param => 1
					)
				);
			}else{
				// update
				$count_db = $wpdb->get_var('SELECT '.$param.' FROM ads_statistic WHERE name_ads = "'.$name.'" AND date_t = "'.$date.'"');
				$wpdb->update(
					'ads_statistic',
					array(
						$param => $count_db + 1
					),
					array(
						'name_ads' => $name,
						 'date_t'  => $date
 					)
				);
			}
		}else{
			// IF ERROR
			$ads_error = $wpdb->get_var('SELECT error_code FROM error_ads_statistic WHERE error_code = "'.$name.'"');
			// CHECK ADS NAME
			if(!$ads_error){
				// insert
				$wpdb->insert(
					'error_ads_statistic',
					array(
						'error_code' => $name,
						'count_t' => 1
					)
				);
			}else{
				// update
				$count_db = $wpdb->get_var('SELECT count_t FROM error_ads_statistic WHERE error_code = "'.$name.'"');
				$wpdb->update(
					'error_ads_statistic',
					array(
						'count_t' => $count_db + 1
					),
					array( 'error_code' => $name )
				);
			}
		}

	}

	/*CHECK THE DATE*/
	$date_db = $wpdb->get_var('SELECT cur_date FROM cur_date');
	if(!$date_db){
		$wpdb->insert(
			'cur_date',
			array(
				'cur_date' => date("Y-m-d"),
			)
		);
		updateStatisticDatabase($param, $name, $date, $wpdb);
	}else{
		$month_db = date("m",strtotime($date_db));
		if($month_db != $cur_month){
			$wpdb->update(
				'cur_date',
				array(
					'cur_date' => $date
				),
				array( 'id' => 1 )
			);
			// DELETE ALL TABLE AND SAVE STATISTICS
			$all = $wpdb->get_results('SELECT * FROM ads_statistic');
			$myfile = fopen($_SERVER['DOCUMENT_ROOT'].'ads_stat/stat-'.date("Y-m",strtotime($date_db)).'.txt', "w") or die();
			$txt = '';
			foreach($all as $key => $val){
				$txt .= $val->id.' '.$val->name_ads.' '.$val->load_t.' '.$val->error_t.' '.$val->skipped_t.' '.$val->click_t.' '.$val->complete_t."\n";
			}
			fwrite($myfile, $txt);
			fclose($myfile);
			$wpdb->query('TRUNCATE TABLE ads_statistic');
		}else{
			updateStatisticDatabase($param, $name, $date, $wpdb);
		}
	}




/*	$select = $wpdb->get_row('SELECT load_t, error_t FROM ads_statistic WHERE country = "'.$country.'"');
	$page_select = $wpdb->get_row('SELECT load_t, error_t FROM country_statistic WHERE country = "'.$page.'"');
	$date = $wpdb->get_var('SELECT cur_date FROM cur_date');
	if($date){
		$month = date("m",strtotime($date));
		$cur_month = date('m');
		if($month != $cur_month){
			$all = $wpdb->get_results('SELECT * FROM ads_statistic');
			$all_page = $wpdb->get_results('SELECT * FROM country_statistic');
			$myfile = fopen($_SERVER['DOCUMENT_ROOT'].'ads_stat/stat'.$month.'.txt', "w") or die();
			$myfile2 = fopen($_SERVER['DOCUMENT_ROOT'].'ads_country_stat/stat'.$month.'.txt', "w") or die();
			$txt = '';
			$txt2 = '';
			foreach($all as $key => $val){
				$txt .= $val->id.' '.$val->country.' '.$val->load_t.' '.$val->error_t."\n";
			}
			foreach($all_page as $key => $val){
				$txt2 .= $val->id.' '.$val->country.' '.$val->load_t.' '.$val->error_t."\n";
			}
			fwrite($myfile, $txt);
			fwrite($myfile, $txt);
			fclose($myfile);
			fwrite($myfile2, $txt2);
			fwrite($myfile2, $txt2);
			fclose($myfile2);

			$wpdb->update(
				'cur_date',
				array(
					'cur_date' => date("Y-m-d"),
				),
				array('id' => 1)
			);
			$wpdb->query('TRUNCATE TABLE ads_statistic');
			$wpdb->query('TRUNCATE TABLE country_statistic');

		}
	}else{
		$wpdb->insert(
			'cur_date',
			array(
				'cur_date' => date("Y-m-d"),
			)
		);
	}
	if($select){

		if($param == 'load'){
			$load = $select->load_t+1;
			$wpdb->update(
				'ads_statistic',
				array(
					'load_t' => $load
				),
				array( 'country' => $country )
			);
		}else{
			$error = $select->error_t+1;
			$wpdb->update(
				'ads_statistic',
				array(
					'error_t' => $error
				),
				array( 'country' => $country )
			);
		}

	}else{
		if($param == 'error'){
			$wpdb->insert(
				'ads_statistic',
				array(
					'country' => $country,
					'error_t' => 1,
					'load_t' => 0
				)
			);
		}else{
			$wpdb->insert(
				'ads_statistic',
				array(
					'country' => $country,
					'load_t' => 1,
					'error_t' => 0
				)
			);
		}

	}
	if($page_select){

		if($param == 'load'){
			$load = $page_select->load_t+1;
			$wpdb->update(
				'country_statistic',
				array(
					'load_t' => $load
				),
				array( 'country' => $page )
			);
		}else{
			$error = $page_select->error_t+1;
			$wpdb->update(
				'country_statistic',
				array(
					'error_t' => $error
				),
				array( 'country' => $page )
			);
		}

	}else{
		if($page != ''){
			if($param == 'error'){
				$wpdb->insert(
					'country_statistic',
					array(
						'country' => $page,
						'error_t' => 1,
						'load_t' => 0
					)
				);
			}else{
				$wpdb->insert(
					'country_statistic',
					array(
						'country' => $page,
						'load_t' => 1,
						'error_t' => 0
					)
				);
			}

		}
	}*/
	die();
}

//add_action('wp_ajax_send_statistics', 'send_statistics_func');
//add_action('wp_ajax_nopriv_send_statistics', 'send_statistics_func');

function search_cams_func(){

	$search = htmlspecialchars(trim($_POST['search_input']));
	global $post;
	$args = array (
		'post_type'              => 'page',
		'post_status'            => 'publish',
		'order'                  => 'ASC',
		'orderby'                => 'rand',
		's'                      => $search,
		'nopaging' 				 => true,
		'suppress_filters'       => '0',
		'meta_query'             => array(
			array(
				'key' => '_wp_page_template',
				'value' => 'camera.php',
				'compare' => '=',
			),
		),
	);

	$cams_list = new WP_Query($args);

	if ($cams_list->have_posts()) {
		while ($cams_list->have_posts()) {
			$cams_list->the_post();
			$children = get_pages('child_of=' . $post->ID);
			if (count($children) == 1) {
				$url = get_page_link($children[0]->ID);
			} else {
				$url = get_permalink();
			}

			include(locate_template('partials/list/camera-item.php', false, false));
		}
	}
	die();
}

add_action('wp_ajax_search_cams', 'search_cams_func');
add_action('wp_ajax_nopriv_search_cams', 'search_cams_func');

function search_cams_null_func(){

	global $post;
	$args = array(
		'category_name' => 'firstpagecameras',
		'post_type' => 'page',
		'posts_per_page' => '6',
		'order' => 'DESC',
		'orderby'  => 'menu_order'
	);
	$query = new WP_Query($args);

?>
	<div class="camera-block loaded-cam-block cams-blocks">
	<div class="camera-list row">
	<?php
	 while ($query->have_posts()) : $query->the_post();

		 $url = get_the_permalink();
		 include(locate_template('partials/list/camera-item.php', false, false));

	 endwhile;
	 ?>
	 </div>
	 </div>
<?php
	die();
}

add_action('wp_ajax_search_cams_null', 'search_cams_null_func');
add_action('wp_ajax_nopriv_search_cams_null', 'search_cams_null_func');

function load_more_cams_func(){

	global $post;
	$page = htmlspecialchars(trim($_POST['page']));

	$args = array(
		'orderby'             => 'id',
		'order'               => 'ASC',
		'post_type'           => 'page',
		'posts_per_page'      => '3',
		'paged'               => $page,
		'post_status'         => 'publish',
		'meta_query'          => array(
			array(
				'key' => '_wp_page_template',
				'value' => 'country.php',
				'compare' => '=',
			),
		),
	);
	$country_list = new WP_Query($args);

	if ($country_list->have_posts()) {
                while ($country_list->have_posts()) {
                    $country_list->the_post();
                    $country_post = $post;
                    ?>

                    <div class="camera-block loaded-cam-block cams-blocks" style="display: none">
                        <h2>
                            <?php the_title() ?>
                        </h2>
                        <div class="camera-list row">
                            <?php
                            $args_inner = array(
                                'orderby' => 'id',
                                'post_parent' => $country_post->ID,
                                'order' => 'ASC',
                                'post_type' => 'page',
                                'post_status' => 'publish',
                                'nopaging' => true,
                                'meta_query' => array(
                                    array(
                                        'key' => '_wp_page_template',
                                        'value' => 'City.php',
                                        'compare' => '=',
                                    ),
                                ),
                            );
                            $inner_query = new WP_Query($args_inner);


                            if ($inner_query->have_posts()) {
                                while ($inner_query->have_posts()) {
                                    $inner_query->the_post();
                                    $children = get_pages('child_of=' . $post->ID);
                                    if (count($children) == 1) {
                                        $url = get_page_link($children[0]->ID);
                                    } else {
                                        $url = get_permalink();
                                    }

                                    include(locate_template('partials/list/camera-item.php', false, false));
                                }
                            }
                            ?>



                        </div>

                    </div>
                    <div style="margin: 10px auto;">
                    <?php echo do_shortcode("[pro_ad_display_adzone id=2161]"); ?>
                    </div>

                    <?php
                }
            }
	die();
}

add_action('wp_ajax_load_more_cams', 'load_more_cams_func');
add_action('wp_ajax_nopriv_load_more_cams', 'load_more_cams_func');

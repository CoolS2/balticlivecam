<?php
/*GET AJAX DATA*/

add_action('wp_ajax_nopriv_ajax_request', 'ajax_handle_request');
add_action('wp_ajax_ajax_request', 'ajax_handle_request');

function ajax_handle_request(){

global $post;
$img = get_template_directory_uri().'/images/point-map.png';

$args = array(
    'post_type' => 'page',
    'post_parent' => $_POST['id'],
    'orderby' => 'id',
    'order' => 'ASC',
    'post_status'    => 'publish',
);
 $query = new WP_Query($args);
            // The Loop

     if ( $query->have_posts() ) : 
     while ( $query->have_posts() ) : $query->the_post(); 

                $link = get_permalink(); 
                $post_name = $post->post_title;
    
    $style = basename(parse_url($link, PHP_URL_PATH));


$weather_id = get_post_meta($post->ID, 'blc_city_id', true);

    include_once(TEMPLATEPATH.'/inc/blc_weather.php');
    $weather = new blc_weather();
    $w = $weather->GetCurrent($weather_id);
    $markPosY = get_post_meta($post->ID, 'top', true);
    $markPosX = get_post_meta($post->ID, 'left', true);
    $tempc = $w->temp;
    $ico = $w->w_ico;
    $temp = get_template_directory_uri().'/images/icons/'.$ico.'.png';
                        
                            //echo '<li><a href="'.$link.'">'.$post_name.'</a></li>';
                            echo '<a href="'.$link.'" class="point-camera point-camera-'.$style.'"><span class="img-point-camera"><img src="'.$img.'" alt="#"></span><span class="temp_city"><img src="'.$temp.'"><p>'.$tempc.'°C</p></span><br>'.$post_name.'</a>';


                                

         endwhile; 
     endif; wp_reset_query(); 

exit();
            }
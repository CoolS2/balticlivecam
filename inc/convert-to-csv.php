<?php
/** Step 2 (from text above). */
add_action( 'admin_menu', 'convert_to_csv' );

/** Step 1. */
function convert_to_csv() {
    add_menu_page('Convert to csv', 'Конвертировать в csv', 10, 'convert_to_csv', 'convert_to_csv_func', '', 5);
}
/** Step 3. */
function convert_to_csv_func()
{
    wp_enqueue_media();
    if (!current_user_can('manage_options')) {
        wp_die(__('You do not have sufficient permissions to access this page.'));
    }
    global $post;

    if (!file_exists($_SERVER['DOCUMENT_ROOT'] . 'csv_files')) {
        mkdir($_SERVER['DOCUMENT_ROOT'] . 'csv_files', 0777, true);
    }
    $myfile = fopen($_SERVER['DOCUMENT_ROOT'].'csv_files/all_cams.txt', "w") or die();
    $txt = '';

    $args = array(
        'order' => 'ASC',
        'post_type' => 'page',
        'nopaging' => true,
        'post_status' => array( 'publish', 'private' ),
        'meta_query' => array(
            array(
                'key' => '_wp_page_template',
                'value' => 'country.php',
                'compare' => '=',
            ),
        ),
    );
    $country_list = new WP_Query($args);

    if ($country_list->have_posts()) {
        while ($country_list->have_posts()) {
            $country_list->the_post();
            $country_post = $post;

            $args_inner = array(
                'order' => 'ASC',
                'post_parent' => $country_post->ID,
                'post_type' => 'page',
                'post_status' => array( 'publish', 'private' ),
                'nopaging' => true,
                'meta_query' => array(
                    array(
                        'key' => '_wp_page_template',
                        'value' => 'City.php',
                        'compare' => '=',
                    ),
                ),
            );
            $city_list = new WP_Query($args_inner);


            if ($city_list->have_posts()) {
                while ($city_list->have_posts()) {
                    $city_list->the_post();
                    $city_post = $post;
                    $city = get_the_title($post->ID);


                    $args_inner = array(
                        'post_parent' => $city_post->ID,
                        'order' => 'ASC',
                        'post_type' => 'page',
                        'post_status' => array( 'publish', 'private' ),
                        'nopaging' => true,
                        'meta_query' => array(
                            array(
                                'key' => '_wp_page_template',
                                'value' => 'camera.php',
                                'compare' => '=',
                            ),
                        ),
                    );
                    $inner_query = new WP_Query($args_inner);


                    if ($inner_query->have_posts()) {
                        while ($inner_query->have_posts()) {
                            $inner_query->the_post();

                            $title = get_the_title($post->ID);
                            $title = str_replace('&#8221', "", $title);
                            $title = str_replace("&#8220", "", $title);
                            $title = str_replace("&#8211", "", $title);
                            $title = str_replace(";", "", $title);
                            $url = get_the_permalink($post->ID);
                            $image_url = "https://thumbs.balticlivecam.com/" . get_post_meta($post->ID, 'blc_camera_id', true) . "_sm.jpg";
                            $lat = get_post_meta($post->ID, 'top', true);
                            $lng = get_post_meta($post->ID, 'left', true);

                            if(get_post_meta($post->ID, 'blc_camera_id', true) == ''){
                                $image_url = get_post_meta($post->ID, 'blc_image_url', true);
                            }
                            $type = get_post_meta($post->ID, "blc_camera_notour", true);
                            if ($type == 2) {
                                $image_url = wp_get_attachment_image_src(get_post_thumbnail_id($post->ID), 'medium')[0];
                            }
                            if(!$image_url){
                                $image_url = get_post_meta($post->ID, 'blc_camera_id', true);
                            }


                            echo $title.'|'.$url.'|'.$image_url.'|'.$lat.'|'.$lng.'|'.$city.' COMPLETE <br>';
                            $txt .= $title.'|'.$url.'|'.$image_url.'|'.$lat.'|'.$lng.'|'.$city."\n";
                        }
                    }


                }
            }
        }
    } // COUNTRY LIST END

    $args2 = array(
        'order' => 'ASC',
        'post_type' => 'page',
        'nopaging' => true,
        'post_parent' => 2884,
    );
    $query2 = new WP_Query($args2);

    if ($query2->have_posts()) {
        while ($query2->have_posts()) { $query2->the_post();
            $args_inner2 = array(
                'post_type' => 'page',
                'post_parent' => $post->ID,
                'order' => 'ASC',
            );
            $inner_query2 = new WP_Query($args_inner2);
            // The Loop
            if ($inner_query2->have_posts()) {
                while ($inner_query2->have_posts()) { $inner_query2->the_post();

                    $title = get_the_title($post->ID);
                    $title = str_replace('&#8221', "", $title);
                    $title = str_replace("&#8220", "", $title);
                    $title = str_replace("&#8211", "", $title);
                    $title = str_replace(";", "", $title);
                    $url = get_the_permalink($post->ID);
                    $image_url = "https://thumbs.balticlivecam.com/" . get_post_meta($post->ID, 'blc_camera_id', true) . "_sm.jpg";
                    $lat = get_post_meta($post->ID, 'top', true);
                    $lng = get_post_meta($post->ID, 'left', true);

                    if(get_post_meta($post->ID, 'blc_camera_id', true) == ''){
                        $image_url = get_post_meta($post->ID, 'blc_image_url', true);
                    }
                    $type = get_post_meta($post->ID, "blc_camera_notour", true);
                    if ($type == 2) {
                        $image_url = wp_get_attachment_image_src(get_post_thumbnail_id($post->ID), 'medium')[0];
                    }
                    if(!$image_url){
                        $image_url = get_post_meta($post->ID, 'blc_camera_id', true);
                    }
                    echo $title.'|'.$url.'|'.$image_url.'|'.$lat.'|'.$lng.'| World COMPLETE <br>';
                    $txt .= $title.'|'.$url.'|'.$image_url.'|'.$lat.'|'.$lng.'| World'."\n";
                }
            }
        }
    }

    fwrite($myfile, $txt);
    fclose($myfile);
    ?>



    <?php
}

<?php
function change_meta_key($post_id, $meta_key, $meta_value, $new_meta_value)
{
    if ($new_meta_value && '' == $meta_value)
        add_post_meta($post_id, $meta_key, $new_meta_value, true);

    /* If the new meta value does not match the old value, update it. */
    elseif ($new_meta_value && $new_meta_value != $meta_value)
        update_post_meta($post_id, $meta_key, $new_meta_value);

    /* If there is no new meta value but an old value exists, delete it. */
    elseif ('' == $new_meta_value|| !isset($new_meta_value) || $new_meta_value ==null )
        delete_post_meta($post_id, $meta_key, $meta_value);


}


function GetBannerDomainException($cam_name,$referer)
{
    global $wpdb;
    $host = parse_url($referer, PHP_URL_HOST);
    $array = explode(".", $host);
    $host = (array_key_exists(count($array) - 2, $array) ? $array[count($array) - 2] : "") . "." . $array[count($array) - 1];
    $exccore = $wpdb->get_results("SELECT * FROM blc_banner_exc WHERE host = '" . esc_sql($host) .
        "'AND enabled = 1");


    $what_to_show = new stdClass();
    $what_to_show->show_ads = true;
    $what_to_show->show_partner = true;
    $what_to_show->show_logo = true;

    if (isset($exccore)) {
        foreach ($exccore as $value) {
            $has_exc = false;
            if ($value->full_ignore == 1) {
                $has_exc = true;
            } else {
                $exc_video = $wpdb->get_results("SELECT * FROM blc_banner_exc_cameras WHERE id_exc = " . $value->banner_exc_id);
                if (isset($exc_video)) {
                    foreach ($exc_video as $video) {
                        if ($video->cam_name == $cam_name)
                            $has_exc = true;
                    }
                }
            }
            if ($has_exc) {
                if ($value->show_ads == 0) {
                    $what_to_show->show_ads = false;
                }
                if ($value->show_logo == 0) {
                    $what_to_show->show_logo = false;
                }
                if ($value->show_partner == 0) {
                    $what_to_show->show_partner = false;
                }
            }
        }
    }
    return $what_to_show;


}
<?php


function auth_main_ajax_request()
{
    // The $_REQUEST contains all the data sent via ajax
    if (isset($_REQUEST)) {
        $main_referer = $_REQUEST['main_referer'];
        $name = "blc/RadissonDaugava";

        $remote = $_SERVER["REMOTE_ADDR"];
        $connector = new blc_connection('g549Ga49asg5', 'VoYZeqGNTshEClcZxaH6');
        $connector->secure = true;


        $res = $connector->GetCameraUrl($name, $remote);
        $url = "";
        if ($res->result) {
            $url = esc_url_raw($res->result->url);
        }
        ?>
        <script>
            flowplayer("#fp-background", {
                live: true,
                ratio: 9 / 16,
                key: "$153760428632348",
                autoplay: true,
                hlsjs: {
                    // listen to hls.js ERROR
                    listeners: ["hlsError"],
                    // limit amount of hls level loading retries
                    levelLoadingMaxRetry: 2
                },
                flashls: {
                    // limit amount of retries to load hls manifests in Flash
                    manifestloadmaxretry: 2
                },
                // this is a background video
                background: {
                    // make mask lighter to reduce distraction
                    mask: "rgba(0, 0, 0, 0.2)"
                },

                clip: {
                    sources: [
                        {
                            type: "application/x-mpegurl",
                            src: "<? echo $url; ?>"
                        }
                    ]
                }
            });

        </script>
        <?php
    }

    die();
}

add_action('wp_ajax_main_token', 'auth_main_ajax_request');
add_action('wp_ajax_nopriv_main_token', 'auth_main_ajax_request');


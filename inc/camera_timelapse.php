<?php
/**
 * Created by PhpStorm.
 * User: Admin
 * Date: 21.08.2016
 * Time: 14:41
 */

function my_Camera_Timelapses() {
    $labels = array(
        'name'               =>  'Camera Timelapses',
        'singular_name'      =>  'Camera Timelapses',
        'add_new'            => 'Add New',
        'add_new_item'       => 'Add New Timelapse',
        'edit_item'          => 'Edit Camera Timelapses',
        'new_item'           =>  'New Camera Timelapses',
        'all_items'          => 'All Camera Timelapses',
        'view_item'          => 'View Camera Timelapses',
        'search_items'       => 'Search Camera Timelapses',
        'not_found'          =>  'No Camera Timelapse found',
        'not_found_in_trash' =>  'No Camera Timelapse in the Trash',
        'parent_item_colon'  => '',
        'menu_name'          => 'Camera Timelapses'
    );

    $args = array(
        'labels'        => $labels,
        'description'   => 'Holds our products and product specific data',
        'public' => false,  // it's not public, it shouldn't have it's own permalink, and so on
        'publicly_queriable' => true,  // you should be able to query it
        'show_ui' => true,  // you should be able to edit it in wp-admin
        'exclude_from_search' => true,  // you should exclude it from search results
        'show_in_nav_menus' => false,  // you shouldn't be able to add it to menus
        'show_in_menu' => false,
        'has_archive' => false,  // it shouldn't have archive page
        'rewrite' => false,
        'capability_type' => 'page',
        'supports' => array('title', 'page-attributes'),
        'hierarchical' => false,
        'parent_item_colon' => 'Parent Page:'
    );
    register_post_type( 'camera_timelapse', $args );
}
add_action( 'init', 'my_Camera_Timelapses' );



function delete_camera_timelapse()
{
    $timelapse = isset($_GET["timelapse_id"]) ? (int)$_GET["timelapse_id"] : null;
    wp_delete_post($timelapse);
    wp_redirect($_SERVER['HTTP_REFERER']);
}


add_action('admin_action_delete_timelapse', 'delete_camera_timelapse');



function camera_timelapse_meta_box($post)
{

    $page_template = get_post_meta($post->ID, '_wp_page_template', true);
    // If the current page uses our specific
    // template, then output our custom metabox
    if (('camera.php' == $page_template) || ('user-content/camera-second.php' == $page_template)) {
        add_meta_box(
            'camera-timelapse-box', // Metabox HTML ID attribute
            'Camera Timelapses', // Metabox title
            'camera_timelapses_template', // callback name
            'page', // post type
            'normal', // context (advanced, normal, or side)
            'high' // priority (high, core, default or low)
        );
    }
}



function timelapse_edit_meta_box($post)
{
    add_meta_box(
        'timelapse-custom-box', // Metabox HTML ID attribute
        'Special Timelapses Meta', // Metabox title
        'camera_timelapse_meta_box_template', // callback name
        'camera_timelapse', // post type
        'normal', // context (advanced, normal, or side)
        'high' // priority (high, core, default or low)
    );
}

add_action('add_meta_boxes_page', 'camera_timelapse_meta_box');
add_action('add_meta_boxes', 'timelapse_edit_meta_box');


function camera_timelapses_template($object, $box)
{
    $args = array(
        'post_type' => array('camera_timelapse'),
        'meta_query' => array(
            array(
                'key' => 'camera_id',
                'value' => $object->ID,
                'compare' => '=',
                'type' => 'NUMERIC',
            ),
        ),
    );
    $query = new WP_Query($args);
    global $post;
    if ($query->have_posts()) {
        ?>

        <div>
            <table class="wp-list-table">
                <thead>
                <tr>
                    <td>File</td>
                    <td>Name</td>
                    <td></td>
                </tr>
                </thead>
                <?php

                while ($query->have_posts()) {
                    $query->the_post();

                    ?>
                    <tr>
                        <td><img style="height:90px" src="<?php echo get_post_meta($post->ID, 'blc_timelapse_url', true);
                            ?>"/></td>
                        <td><?php echo $post->post_title;
                            ?></td>
                        <td>
                            <a  href="<?php echo get_admin_url() . "admin.php?action=delete_image&image_id=" . $post->ID;
                            ?>">Удалить</a>
                        </td>
                    </tr>
                    <?php
                }

                ?>
            </table>
        </div>
        <?php
    }


    ?>


    <p>
        <a class="btn"
           href="<?php echo add_query_arg(array('post_type' => 'camera_timelapse', 'camera_id' => $object->ID), admin_url('post-new.php')); ?>"
           id="add-timelapse" name="add-timelapse">
            Add Timelapse
        </a>
    </p>
    <?php
}


function camera_timelapse_meta_box_template($object, $box)
{
    wp_enqueue_media();
    wp_nonce_field(basename(__FILE__), 'blc_post_class_nonce');
    $id = get_post_meta($object->ID, 'camera_id', true);
    if (!$id) {
        $id = $_REQUEST["camera_id"];
    }
    if (!$id) {
        ?>

        <?php

    } else {
        ?>
        <input type="hidden" id="camera_id" name="camera_id" value="<?php echo $id; ?>">
    <?php } ?>
    <p>
    	<label
            for="blc-timelapse-name"><?php _e("Timelapse Name", 'blc'); ?></label>
        <br/>
        <input type="text" name="blc-timelapse-name" id="blc-timelapse-name"
               value="<?php echo esc_url(get_post_meta($object->ID, 'blc-timelapse-name', true)); ?>" size="50"/><br>


        <label
            for="blc_timelapse_url"><?php _e("Image Url", 'blc'); ?></label>
        <br/>
        <input type="text" name="blc_timelapse_url" id="blc_timelapse_url"
               value="<?php echo esc_url(get_post_meta($object->ID, 'blc_timelapse_url', true)); ?>" size="50"/>
        <input id="select_img" name="select_img" type="button" value="Images"/>

    </p>
    <script>
        jQuery(document).ready(function () {
            jQuery('#select_img').click(function (e) {
                e.preventDefault();
                var image = wp.media({
                    title: 'Upload Image',
                    multiple: false
                }).open()
                    .on('select', function (e) {
                        var uploaded_image = image.state().get('selection').first();
                        var image_url = uploaded_image.toJSON().url;
                        jQuery('#blc_timelapse_url').val(image_url);
                    });
            });

        });
    </script>


    <?php
}

function camera_timelapse_meta_box_save($post_id)
{
    if (is_admin()) {


        if (!isset($_POST['blc_post_class_nonce']) || !wp_verify_nonce($_POST['blc_post_class_nonce'], basename(__FILE__)))
            return $post_id;
        $meta_key = 'camera_id';
        $new_meta_value = (isset($_POST[$meta_key]) ? $_POST[$meta_key] : '');
        $meta_value = get_post_meta($post_id, $meta_key, true);
        change_meta_key($post_id, $meta_key, $meta_value, $new_meta_value);

        $meta_key = 'blc_timelapse_url';
        $new_meta_value = (isset($_POST[$meta_key]) ? $_POST[$meta_key] : '');
        $meta_value = get_post_meta($post_id, $meta_key, true);
        change_meta_key($post_id, $meta_key, $meta_value, $new_meta_value);

        $meta_key = 'blc-timelapse-name';
        $new_meta_value = (isset($_POST[$meta_key]) ? $_POST[$meta_key] : '');
        $meta_value = get_post_meta($post_id, $meta_key, true);
        change_meta_key($post_id, $meta_key, $meta_value, $new_meta_value);
    }
}


add_action('save_post', 'camera_timelapse_meta_box_save');


add_action('save_post', 'redirect_camera_timelapse_page');
function redirect_camera_timelapse_page()
{
    $type = get_post_type();

    switch ($type) {
        case "camera_timelapse":
            $post = get_post();
            $id_camera = get_post_meta($post->ID, 'camera_id', true);
            $url = add_query_arg(array('action' => 'edit', 'post' => $id_camera), admin_url('post.php'));
            wp_redirect($url);
            exit;
            break;
    }
}

<?php

function weather_func()
{

    $weatherId = $_POST['weather_id'];
    $weather = new blc_weather();
    $w = $weather->GetForecast($weatherId);
    $a = 0;
        $current = $weather->GetCurrent($weatherId);
        ?>
        <div class="row">
            <div class="col-md-6">

                <div class="weather_day_new weather-blue">
                    <div class="gradient-weather"></div>
                    <div class="row">
                        <div class="col-md-6">

                            <h4><?php echo $current->name ?></h4>
                            <div class="row">
                                <div class="col-md-7">

                                    <div class="text-row">
                                        <p><?php _e(date('l'), "blc"); ?></p>
                                    </div>
                                    <div class="text-row">
                                        <p><?php _e(date('d/m/Y'), "blc"); ?></p>
                                    </div>
                                    <div class="text-row">
                                    <p>Wind <?php echo $current->wind_speed ?> m/s</p>
                                        <img class="therm"
                                             src="<?php echo get_template_directory_uri(); ?>/images/weather/drop.svg" alt="#">
                                    </div>
                                    <div class="text-row">
                                        <p><?php echo $current->humidity ?>%</p>
                                    </div>

                                </div>
                                <div class="col-md-5">
                                    <div class="current-day-right-column">
                                        <img class="weather-column"
                                             src="<?php echo get_template_directory_uri() . '/images/iconsWhite/' . $current->w_ico . '.png' ?>"
                                             alt="#">
                                        <h5><?php echo $current->temp; ?>°</h5>
                                    </div>
                                </div>
                            </div>

                        </div>

                        <div class="col-md-6">
                            <div class="row">

                                <?php
                                foreach ($w->weather as $value) {

                                    if (intval($value->dt) < intval(microtime(true))) continue;
                                    $a++;
                                    ?>
                                    <div class="col-md-4">
                                        <div class="col-day-other">
                                            <h5><?php echo date('G:i', $value->dt); ?></li></h5>
                                            <img class="weather-column"
                                                 src="<?php echo get_template_directory_uri() . '/images/iconsWhite/' . $value->w_ico . '.png' ?>"
                                                 alt="#">
                                            <p><?php echo $value->temp; ?>°</p>
                                        </div>
                                    </div>
                                    <?php
                                    if ($a == '3') {
                                        break;
                                    }
                                }

                                $weath = $weather->GetDaily($weatherId);
                                $a = 0;
                                ?>

                            </div>
                        </div>
                    </div>


                </div>
            </div>

            <div class="col-md-6">

                <div class="weather_other_new weather-blue">
                <div class="row">


                <?php
                foreach ($weath->weather as $daily) {
                    $a++;
                    if ($a == 1) continue;
                    ?>
                    <div class="col-md-2">
                        <div class="col-day-other">
                            <h5><?php _e(date('D', $daily->dt), "blc"); ?></h5>
                            <img class="weather-column"
                                 src="<?php echo get_template_directory_uri() . '/images/iconsWhite/' . $daily->w_ico . '.png'; ?>"
                                 alt="#">
                            <p><?php echo $daily->temp_day; ?>°</p>
                        </div>
                    </div>
                    <?php
                    if ($a == 7) {
                        break;
                    }
                }
                ?>

                </div>
            </div>
            </div>
        </div>


    <?php

    die();
}


add_action('wp_ajax_weatherblock', 'weather_func');
add_action('wp_ajax_nopriv_weatherblock', 'weather_func');

// WEATHER FOR MAP

function weather_map()
{

    $postid = $_POST['post_id'];


    $dom = new DOMDocument("1.0");
    $node = $dom->createElement("markers"); //Create new element node
    $parnode = $dom->appendChild($node); //make the node show up
    $parentY = get_post_meta($post->ID, 'top', true);
    $parentX = get_post_meta($post->ID, 'left', true);

    $args = array(
        'orderby' => 'id',
        'post_parent' => $postid,
        'post_status' => array('Publish'),
        'post_type' => 'page',
    );
    global $post;
    // The Query
    $query = new WP_Query($args);
    if ($query->have_posts()) {
        while ($query->have_posts()) {
            $query->the_post();

            $link = get_permalink();
            $style = basename(parse_url($link, PHP_URL_PATH));
            $weather_id = get_post_meta($post->ID, 'blc_city_id', true);
            $markPosY = get_post_meta($post->ID, 'top', true);
            $markPosX = get_post_meta($post->ID, 'left', true);

            $weather = new blc_weather();
            $w = $weather->GetCurrent($weather_id);
            $tempc = $w->temp;
            $ico = $w->w_ico;
            $temp = get_template_directory_uri() . '/images/icons/' . $ico . '.png';
            $thumb = get_post_meta($post->ID, 'blc_image_url', true);

            $node = $dom->createElement("marker");
            $newnode = $parnode->appendChild($node);
            $newnode->setAttribute("name", $post->post_title);
            $newnode->setAttribute("lat", $markPosY);
            $newnode->setAttribute("lng", $markPosX);
            $newnode->setAttribute('temp', $temp);
            $newnode->setAttribute('tempc', $tempc);
            $newnode->setAttribute('url', $link);
            $newnode->setAttribute('thumb', $thumb);
        }
    }
    die($dom->saveXML($dom->documentElement));

}

add_action('wp_ajax_weathermap', 'weather_map');
add_action('wp_ajax_nopriv_weathermap', 'weather_map');
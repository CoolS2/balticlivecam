<?php
function weather_ajax_request()
{
    // The $_REQUEST contains all the data sent via ajax
    if (isset($_REQUEST)) {
        $id = $_REQUEST['id'];
//        generate_weather($id);
//    }
//    die();
        $weather_id = $_POST['id'];
        $weather = new blc_weather();
        $w = $weather->GetCurrent($weather_id);
        $tempc = $w->temp;
        $speed = $w->wind_speed;
        $ico = $w->w_ico;

        echo '<div class="weather-camera">
        <img src="/weather/icons/' . $ico . '.png" class="weather-icon" alt="Weather Icon"/>
        <div class="weather-data">
        <b>' . $tempc . '<sup>o</sup>C</b>
        ' . $speed . __('m/s', 'blc') . '
        </div>

    </div>';
    }
    die();
}

add_action('wp_ajax_weather', 'weather_ajax_request');
add_action('wp_ajax_nopriv_weather', 'weather_ajax_request');
<?php
add_action('admin_init', 'my_slide_box', 1);
function my_slide_box() {

	 $post_id = $_GET['post'] ? $_GET['post'] : $_POST['post_ID'] ;
	$template_file = get_post_meta($post_id,'_wp_page_template',TRUE);
  // check for a template type
  if ($template_file == 'about.php') {
   			 add_meta_box( 'slide_box', 'Дополнительное поле', 'slide_box_func', 'page', 'normal', 'high' );
      }
	 
}

function slide_box_func( $post ){
?>

	<div>
            <label style="font-weight: bold; padding-top:20px; font-size: 20px;">Первый блок</label><br>
            <span>Картинка</span><br>
            <input name="extra[img]" type="text" value="<?php echo get_post_meta($post->ID, "img", true); ?>" style="width:50%"><br>
            <span>Текст</span><br>
            <textarea rows="10" name="extra[text]" style="width:100%"><?php echo get_post_meta($post->ID, "text", true); ?></textarea>
			<br>
            <label style="font-weight: bold; padding-top:20px; font-size: 20px;">Слайд блок</label><br>
            <span>Слайды</span><br>
            <textarea rows="10" name="extra[slide]" style="width:100%"><?php echo get_post_meta($post->ID, "slide", true); ?></textarea><br>
            <span>Текст</span><br>
            <textarea rows="10" name="extra[text2]" style="width:100%"><?php echo get_post_meta($post->ID, "text2", true); ?></textarea>
            <span>Технические характеристики</span>
            <textarea rows="10" name="extra[teh]" style="width:100%"><?php echo get_post_meta($post->ID, "teh", true); ?></textarea>
			<br>
            <label style="font-weight: bold; padding-top:20px; font-size: 20px;">Третий блок</label><br>
            <span>Картинка</span><br>
            <input name="extra[img2]" type="text" value="<?php echo get_post_meta($post->ID, "img2", true); ?>" style="width:50%">
            <span>Текст</span><br>
            <textarea rows="10" name="extra[text3]" style="width:100%"><?php echo get_post_meta($post->ID, "text3", true); ?></textarea>

            <input type="hidden" name="extra_fields_nonce2" value="<?php echo wp_create_nonce(__FILE__); ?>" />
			
            <br>

    
        </div>
        
<?php
}

add_action('save_post', 'slide_box_save', 0);
function slide_box_save( $post_id ){
    if ( !wp_verify_nonce($_POST['extra_fields_nonce2'], __FILE__) ) return false;
    if ( defined('DOING_AUTOSAVE') && DOING_AUTOSAVE  ) return false;
    if ( !current_user_can('edit_post', $post_id) ) return false;
    if( !isset($_POST['extra']) ) return false;
    $_POST['extra'] = array_map('trim', $_POST['extra']);
    foreach( $_POST['extra'] as $key=>$value ){
        if( empty($value) ) {
            delete_post_meta($post_id, $key);
			continue;
			}
        update_post_meta($post_id, $key, $value);
    }
 
    return $post_id;
} 


add_action('admin_init', 'mark_position', 1);
function mark_position() {

     $post_id = $_GET['post'] ? $_GET['post'] : $_POST['post_ID'] ;
    $template_file = get_post_meta($post_id,'_wp_page_template',TRUE);
  // check for a template type
  if ( ($template_file == 'camera.php') || ($template_file == 'City.php') || ($template_file == 'country.php') || ($template_file == 'user-content/camera-second.php') || ($template_file == 'user-content/world-city.php') || ($template_file == 'user-content/world-country.php') || ($template_file == 'user-content/snow-city.php') || ($template_file == 'user-content/snow-country.php') ) {
             //add_meta_box( 'slide_box', 'Позиция марки', 'mark_pos_func', 'page', 'normal', 'high' );
      }

    if ( ($template_file == 'camera.php') ) {
                 //add_meta_box( 'slide_box2', 'Дополнительные марки', 'custom_mark_pos_func', 'page', 'normal', 'high' );
        add_meta_box( 'custom_meta_box', 'Additional options', 'custom_meta_box_func', 'page', 'normal', 'high' );
          }
     
}

function mark_pos_func( $post ){
?>

         <div>
            <label style="font-weight: bold; padding-top:20px; font-size: 20px;">Установка марки на карте</label><br>          
            <input name="extra[top]" step="0.0000001" type="number" value="<?php echo get_post_meta($post->ID, "top", true); ?>" style="width:25%"><span>TOP (Y-lat)</span><br>
            <input name="extra[left]" step="0.0000001" type="number" value="<?php echo get_post_meta($post->ID, "left", true); ?>" style="width:25%"><span>LEFT (X-lng)</span><br>
            <h3>Обзор камеры</h3>
            <span>Angle1 (Y-lat)</span><input name="extra[start-l2]" step="0.0000001" type="number" value="<?php echo get_post_meta($post->ID, "start-l2", true); ?>" style="width:25% float:left;">
            <span>Angle1 (X-lng)</span><input name="extra[start-t2]" step="0.0000001" type="number" value="<?php echo get_post_meta($post->ID, "start-t2", true); ?>" style="width:25% clear:right;"><br>
            <span>Angle2 (Y-lat)</span><input name="extra[start-l3]" step="0.0000001" type="number" value="<?php echo get_post_meta($post->ID, "start-l3", true); ?>" style="width:25% float:left;">
            <span>Angle2 (X-lng)</span><input name="extra[start-t3]" step="0.0000001" type="number" value="<?php echo get_post_meta($post->ID, "start-t3", true); ?>" style="width:25% clear:right;"><br>

            <input type="hidden" name="extra_fields_nonce" value="<?php echo wp_create_nonce(__FILE__); ?>" />    
            <br>

        </div>
        
<?php
}

function custom_mark_pos_func( $post ){
?>

         <div>
            <label style="font-weight: bold; padding-top:20px; font-size: 20px;">Установка доп. марки на карте</label><br>
            <ul>Иконки
            <li>Coffee - https://balticlivecam.com/wp-content/themes/Blc/images/coffee.png</li>
            <li>MacDak - https://balticlivecam.com/wp-content/themes/Blc/images/macdak.png</li>
            <li>Salon - https://balticlivecam.com/wp-content/themes/Blc/images/salon.png</li>
            <li>Hotel - https://balticlivecam.com/wp-content/themes/Blc/images/hotel.png</li>
            <li>Restoran - https://balticlivecam.com/wp-content/themes/Blc/images/restoran.png</li>
            <li>Shop - https://balticlivecam.com/wp-content/themes/Blc/images/shop.png</li>
            </ul>        
            <textarea rows="10" name="extra[markpos]" style="width:100%"><?php echo get_post_meta($post->ID, "markpos", true); ?></textarea><br>

            <input type="hidden" name="extra_fields_nonce" value="<?php echo wp_create_nonce(__FILE__); ?>" />    
            <br>

        </div>
        
<?php
}

function custom_meta_box_func( $post ){
    ?>
    <div>
        <table class="form-table">
            <tbody>
                <tr>
                    <th scope="row">
                        <label for="text_under_camera">Text under camera</label>
                    </th>
                    <td>
                        <input name="extra[text_under_camera]" id="text_under_camera" class="regular-text" type="text" value="<?php echo get_post_meta($post->ID, "text_under_camera", true); ?>">
                    </td>
                </tr>
            </tbody>
        </table>
        <input type="hidden" name="extra_fields_nonce" value="<?php echo wp_create_nonce(__FILE__); ?>" />
    </div>
<?php
}

add_action('save_post', 'mark_pos_save', 0);

function mark_pos_save( $post_id ){
    if ( !wp_verify_nonce($_POST['extra_fields_nonce'], __FILE__) ) return false;
    if ( defined('DOING_AUTOSAVE') && DOING_AUTOSAVE  ) return false;
    if ( !current_user_can('edit_post', $post_id) ) return false;
    if( !isset($_POST['extra']) ) return false;
    $_POST['extra'] = array_map('trim', $_POST['extra']);
    foreach( $_POST['extra'] as $key=>$value ){
        if( empty($value) ) {
            delete_post_meta($post_id, $key);
            continue;
            }
        update_post_meta($post_id, $key, $value);
    }
 
    return $post_id;
} 


<?php

/**
 * Created by PhpStorm.
 * User: Admin
 * Date: 08.08.2016
 * Time: 9:59
 */
class blc_weather
{

    private $user = "blc";
    private $secret = "34bI0J5KOcdPvCoGoyaj";
    private $server = 'https://auth1.balticlivecam.com';


    public function __construct()
    {

    }

    private function makeRequest($command, $args = null)
    {
        if ($args) {
            foreach ($args as $key => $val) {
                $get_param = $key . "=" . urlencode($val);
            }
        }
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $this->server . '/api/Weather/' . $command . (isset($get_param) ? "?" . $get_param : ""));
        $header = [
            'BlcKey:'.$this->user,
            'BlcSecret:'.$this->secret
        ];
        curl_setopt($ch, CURLOPT_HTTPHEADER, $header);
        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 2);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);


        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        $server_output = curl_exec($ch);
        if (curl_errno($ch)) {
            $server_output = curl_error($ch);
        }
        curl_close($ch);
        return json_decode ($server_output);
    }

    public function GetCurrent($cityid)
    {
        return $this->makeRequest('Current', ['city_id' => $cityid]);
    }

    public function GetDaily($cityid)
    {
        return $this->makeRequest('Daily', ['city_id' => $cityid]);
    }


    public function GetForecast($cityid)
    {
        return $this->makeRequest('Hourly', ['city_id' => $cityid]);
    }


}

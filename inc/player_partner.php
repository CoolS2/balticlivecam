<?php

function my_Camera_Partner() {
    $labels = array(
        'name'               =>  'Camera Partner',
        'singular_name'      =>  'Camera Partner',
        'add_new'            => 'Add New',
        'add_new_item'       => 'Add New Camera Partner',
        'edit_item'          => 'Edit Camera Partner',
        'new_item'           =>  'New Camera Partner',
        'all_items'          => 'All Camera Partner',
        'view_item'          => 'View Camera Partner',
        'search_items'       => 'Search Camera Partner',
        'not_found'          =>  'No Camera Partner found',
        'not_found_in_trash' =>  'No Camera Partner in the Trash',
        'parent_item_colon'  => '',
        'menu_name'          => 'Camera Partner'
    );
    
    $args = array(
        'labels'        => $labels,
        'description'   => 'Holds our products and product specific data',
        'public'        => false,
        'menu_position' => 5,
        'publicly_queriable' => true,  // you should be able to query it
        'show_in_menu' => true,
        'show_ui' => true,  // you should be able to edit it in wp-admin
        'supports'      => array( 'title','thumbnail', 'excerpt' ),
        'has_archive'   => false,
    );
    register_post_type( 'camera_partner', $args );
}
add_action( 'init', 'my_Camera_Partner' );




function camera_partner_meta_box($post)
{

       add_meta_box(
            'camera-custom-box', // Metabox HTML ID attribute
            'Special Post Meta', // Metabox title
            'camera_partner_meta_box_template', // callback name
            'camera_partner', // post type
            'normal', // context (advanced, normal, or side)
            'high' // priority (high, core, default or low)
        );
}


function camera_partner_meta_box_template($object, $box)
{
    wp_nonce_field(basename(__FILE__), 'blc_post_class_nonce'); ?>
    <p>
        <label
            for="blc-camera-id"><?php _e("Partner Url", 'blc'); ?></label>
        <br/>
        <input type="text" name="blc-camera-id" id="blc-camera-id"
               value="<?php echo esc_url(get_post_meta($object->ID, 'partner_url', true)); ?>" size="50"/>
    </p>
    <?php
}

function camera_partner_meta_box_save($post_id)
{
    if (is_admin()) {
        if (!isset($_POST['blc_post_class_nonce']) || !wp_verify_nonce($_POST['blc_post_class_nonce'], basename(__FILE__)))
            return $post_id;

        $new_meta_value = (isset($_POST['blc-camera-id']) ? $_POST['blc-camera-id'] : '');
        $meta_key = 'partner_url';
        $meta_value = get_post_meta($post_id, $meta_key, true);
        change_meta_key($post_id,$meta_key,$meta_value,$new_meta_value);
    }

}

add_action('add_meta_boxes', 'camera_partner_meta_box');


add_action('save_post', 'camera_partner_meta_box_save');

<?php

function wpdocs_echo_first_image( $post_id ) {
    $args = array(
        'posts_per_page' => 1,
        'order'          => 'ASC',
        'post_parent'    => $post_id
    );

    $obj = new WP_Query($args);

    if ($obj->have_posts()) {
        while ($obj->have_posts()) {
            $obj->the_post();
            echo $post->ID;
            echo "https://thumbs.balticlivecam.com/" . get_post_meta($post->ID, 'blc_camera_id', true) . "_sm.png";
        }

    }
    echo 'check';
}

function add_custom_template_filter() {
    global $typenow;

    if( $typenow == 'page' ){

        $templates = wp_get_theme()->get_page_templates();
        echo '<select name="custom_template_filter">';
        echo "<option value=''>Show All Templates</option>";
        foreach ( $templates as $template_name => $template_filename ) {

            if(isset($_GET['custom_template_filter']) && ($_GET['custom_template_filter'] == $template_name)){
                $selected = "selected";
            }else{
                $selected = '';
            }

            echo '<option value="'.$template_name.'" '.$selected.'>' . $template_filename .'</option>';

            echo $template_filename.'<br>';

        }
        echo '</select>';

    }
}
add_action( 'restrict_manage_posts', 'add_custom_template_filter' );

add_filter( 'parse_query', 'sort_page_by_template' );
function sort_page_by_template($query) {
    global $pagenow;

    if(is_admin() && $pagenow=='edit.php' && isset($_GET['post_type']) && isset($_GET['post_type'])=='page' && $_GET['custom_template_filter'] != ''){
        $query->query_vars['meta_key'] = '_wp_page_template';
        $query->query_vars['meta_value'] = $_GET['custom_template_filter'];
    }
}

add_action('admin_head', 'my_column_width');

function my_column_width() {
    echo '<style type="text/css">';
    echo '.column-author, .column-tags{display:none} .column-title{width:10%} .td[class*=column-language_], th[class*=column-language_] {width: 0.1em;}';
    echo '.snippet-editor__heading-icon{padding-left: 45px!important;} .yoast-section__heading-icon{padding-left: 45px!important;} #wp-admin-bar-languages .ab-empty-item{padding: 14px 10px 15px 10px!important;}';
    echo '</style>';
}
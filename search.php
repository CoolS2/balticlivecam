<?php /* Template Name: Camera List */ ?>
<?php
$args = array(
    'orderby'             => 'id',
    'order'               => 'ASC',
    'post_type'           => 'page',
    'posts_per_page'      => '3',
    'paged'               => '1',
    'post_status'         => 'publish',
    'meta_query'          => array(
        array(
            'key' => '_wp_page_template',
            'value' => 'country.php',
            'compare' => '=',
        ),
    ),
);
$country_list = new WP_Query($args);
function search_script_function()
{
    ?>
    <script>
        $(function () {
            var useSearch = false;
            var page = 1;
            var ajaxLoading = false;

            function scrollFunc(numb) {
                var scrollNumb = parseInt($(window).scrollTop()) + numb;

                if( scrollNumb >= $(document).height() - $(window).height()) {
                    if(!useSearch){
                        if(!ajaxLoading){
                            page++;
                            $.ajax({
                                url: ajaxurl,
                                method: "POST",
                                data: {'action': 'load_more_cams', page: page},
                                dataType: "html",
                                beforeSend: function() {
                                    ajaxLoading = true;
                                    $('.content-city-blocks').append('<div class="spin-load" style="text-align: center;"><i class="fa fa-cog fa-spin fa-3x fa-fw"></i></div>');
                                },
                                success: function (data) {
                                    if(data){
                                        $('.content-city-blocks').append(data);
                                        $('.loaded-cam-block').slideDown(1000);
                                        ajaxLoading = false;
                                    }
                                    $('.spin-load').html('').hide();
                                }
                            });
                        }

                    }
                }
            }

            $(window).on('touchmove', function() {
                scrollFunc(300);
            });
            $(window).scroll(function() {
                scrollFunc(100);
            });

            function searchFunc(){
                useSearch = true;
                var ajaxurl = '<?php echo admin_url('admin-ajax.php'); ?>';
                var search_input = $('#search_input').val();

                ga('send', {
                    hitType: 'event',
                    eventCategory: 'Search-cameras',
                    eventAction: 'Action',
                    eventLabel: search_input
                });

                $.ajax({
                    url: ajaxurl,
                    method: "POST",
                    data: {'action': 'search_cams', 'search_input': search_input},
                    dataType: "html",
                    beforeSend: function() { $('.camera-list').slideUp(500); },
                    success: function (data) {
                        $('.content-city-blocks').html('').append('<div class="camera-block"><div class="camera-list" style="display:none;margin: 30px 0;"></div></div>');
                        if(data){
                            $('.camera-list').html(data).slideDown(1000);
                        }else{
                            $.ajax({
                                url: ajaxurl,
                                method: "POST",
                                data: {'action': 'search_cams_null'},
                                dataType: "html",
                                success: function (data) {
                                    $('.content-city-blocks').prepend('<h3 class="nothing-found"><?=__("Oops, nothing has been found. Try again later or look at the recommended cameras" , "blc") ?></div>');
                                    $('.camera-list').html(data).slideDown(1000);
                                }
                            });
                        }

                    }
                });
            }
            $(document).keypress(function(e) {
                if(e.which == 13) {
                    searchFunc();
                }
            });


            $('#find_cameras_button').on('click', function () {
                searchFunc();
            });

            <?php
            if(isset($_GET['s'])){
            ?>
            searchFunc();
            <?php
            }
            ?>
        });
    </script>
<?php }

add_action('wp_footer', 'search_script_function', 20);

?>

<?php get_header(); ?>

    <div class="site">
        <?php get_template_part("partials/nav"); ?>
        <aside class="left follow">
            <div class="left-ads">

                <?php echo do_shortcode("[pro_ad_display_adzone id=7659]"); //LEFT PANEL ?>
            </div>
        </aside>


        <aside class="right follow">
            <div class="right-ads">
                <?php echo do_shortcode("[pro_ad_display_adzone id=7661]"); //RIGHT PANEL ?>
            </div>
        </aside>
        <div class="container">
            <div style="margin-top:10px;">
                <?php echo do_shortcode("[pro_ad_display_adzone id=7738]"); ?>
            </div>
            <div class="search-block">
                <div class="centered-search-block">
                    <input type="search" id="search_input" value="<?=$_GET['s']?>" placeholder="<?php _e("Search cameras", "blc") ?>..">
                    <button id="find_cameras_button"><i class="fa fa-search" aria-hidden="true"></i></button>
                </div>
            </div>

            <div class="content-city-blocks">


                <?php
                if ($country_list->have_posts()) {
                    while ($country_list->have_posts()) {
                        $country_list->the_post();
                        $country_post = $post;
                        $country_count++;
                        ?>

                        <div class="camera-block cams-blocks">
                            <h2>
                                <?php the_title() ?>
                            </h2>
                            <div class="camera-list row">
                                <?php
                                $args_inner = array(
                                    'orderby' => 'id',
                                    'post_parent' => $country_post->ID,
                                    'order' => 'ASC',
                                    'post_type' => 'page',
                                    'post_status' => 'publish',
                                    'nopaging' => true,
                                    'meta_query' => array(
                                        array(
                                            'key' => '_wp_page_template',
                                            'value' => 'City.php',
                                            'compare' => '=',
                                        ),
                                    ),
                                );
                                $inner_query = new WP_Query($args_inner);


                                if ($inner_query->have_posts()) {
                                    while ($inner_query->have_posts()) {
                                        $inner_query->the_post();
                                        $children = get_pages('child_of=' . $post->ID);
                                        if (count($children) == 1) {
                                            $url = get_page_link($children[0]->ID);
                                        } else {
                                            $url = get_permalink();
                                        }

                                        include(locate_template('partials/list/camera-item.php', false, false));
                                    }
                                }
                                ?>



                            </div>

                        </div>
                        <div style="margin: 10px auto;">
                            <?php echo do_shortcode("[pro_ad_display_adzone id=2161]"); ?>
                        </div>

                        <?php
                    }
                }


                ?>
            </div>
        </div>


        <?php get_template_part("partials/main/footer"); ?>
    </div>

<?
get_footer();


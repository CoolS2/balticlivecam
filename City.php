<?php /* Template Name: City */
$city_post = $post;
$country_post = get_post($city_post->post_parent);
$weather_id = get_post_meta($city_post->ID, 'blc_city_id', true);

?>


<?php get_header(); ?>

    <div class="site">
        <?php get_template_part("partials/nav"); ?>
        <aside class="left follow">
            <div class="left-ads">
                <?php echo do_shortcode("[pro_ad_display_adzone id=7659]"); //LEFT PANEL ?>
            </div>
        </aside>

        <aside class="right follow">
            <div class="right-ads">
                <?php echo do_shortcode("[pro_ad_display_adzone id=7661]"); //RIGHT PANEL ?>
            </div>
        </aside>
        <div class="container">
            <div style="margin-top:10px;">
                <?php echo do_shortcode("[pro_ad_display_adzone id=7738]"); ?>
            </div>


            <div class="camera-block cams-blocks">
                <h2>
                    <?php the_title() ?>
                </h2>
                <div class="camera-list row">
                    <?php
                    $args_inner = array(
                        'orderby' => 'id',
                        'post_parent' => $city_post->ID,
                        'order' => 'ASC',
                        'post_type' => 'page',
                        'post_status' => 'publish',
                        'nopaging' => true,
                        'meta_query' => array(
                            array(
                                'key' => '_wp_page_template',
                                'value' => 'camera.php',
                                'compare' => '=',
                            ),
                        ),
                    );
                    $inner_query = new WP_Query($args_inner);


                    if ($inner_query->have_posts()) {
                        while ($inner_query->have_posts()) {
                            $inner_query->the_post();
                            $children = get_pages('child_of=' . $post->ID);
                            if (count($children) == 1) {
                                $url = get_page_link($children[0]->ID);
                            } else {
                                $url = get_permalink();
                            }

                            include(locate_template('partials/list/camera-item.php', false, false));
                        }
                    }
                    ?>


                </div>

            </div>
            <div style="margin: 10px auto;">
                <?php echo do_shortcode("[pro_ad_display_adzone id=2161]"); ?>
            </div>
        </div>
        <?php get_template_part("partials/main/footer"); ?>
    </div>


<?
get_footer();


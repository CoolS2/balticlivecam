<?php /* Template Name: Blog */ ?>

<?php

function add_this_script_footer()
{ ?>
    <script>
        var ppp = 3; // Post per page
        function load_posts(object, cat, page) {
           // var str = '&cat=' + cat + '&pageNumber=' + page + '&ppp=' + ppp + '&action=more_post_ajax';
            $.ajax({
                type: "POST",
                dataType: "html",
                url: ajaxurl,
                data: {"action":"more_post_ajax","cat": cat, "pageNumber": page, "ppp": ppp},
                success: function (data) {
                    var $data = $(data);
                    if ($data.length) {
                        object.append($data);
                        $(".more_posts").attr("disabled", false);
                    } else {
                        $(".more_posts").attr("disabled", true);
                    }
                },
                error: function (jqXHR, textStatus, errorThrown) {
                    //                 $loader.html(jqXHR + " :: " + textStatus + " :: " + errorThrown);
                }
            });
            return false;
        }

        $(".more_posts").on("click", function () { // When btn is pressed.
            $this = $(this);
            cat = $this.data('category');
            page = parseInt($this.data('page'), 10) > 0 ? parseInt($this.data('page')) : 1;
            max_pages = $this.data("max-pages");
            container = $this.prev();
            $(".more_posts").attr("disabled", true); // Disable the button, temp.
            load_posts(container, cat, page);
            page++;
            $this.data("page", page);
            if(page>=max_pages) $this.hide(500);
        });

    </script>
<?php }

add_action('wp_footer', 'add_this_script_footer', 20);


get_header();


?>


    <div class="site">
        <?php get_template_part("partials/nav"); ?>
        <div class="container">
            <div class="row">
                <div class="col-md-12 big-first">
                    <h3><?php _e("Newest posts", "blc") ?></h3>
                    <div class="row">
                        <?php $args = array(
                            'posts_per_page' => 2,
                            'offset' => 0,
                            'orderby' => 'date',
                            'order' => 'DESC',
                            'cat' => '56, 1, 55,',
                            'post_type' => 'post',
                            'post_status' => 'publish',
                            'suppress_filters' => true
                        );
                        $myposts = get_posts($args);
                        foreach ($myposts as $post) : setup_postdata($post); ?>

                            <div class="col-md-6 big-blog">
                                <div class="blog-2">
                                    <a href="<?php the_permalink(); ?>">
                                        <img src="<? the_post_thumbnail_url('large'); ?>" alt="#">
                                        <h3><?php the_title(); ?></h3>
                                    </a>
                                </div>
                            </div>

                        <?php endforeach;
                        wp_reset_postdata(); ?>
                    </div>

                    <?php
                    //get all children of category "Site Articles", then display posts in each cat
                    $taxonomy = 'category';
                    $param_type = 'category__in';
                    $cat_id = get_cat_ID('Site Articles');
                    $term_args = array(
                        'orderby' => 'name',
                        'order' => 'ASC',
                        'child_of' => $cat_id
                    );
                    $terms = get_terms($taxonomy, $term_args);
                    if ($terms) {
                        foreach ($terms as $term) {
                            $args = array(
                                "$param_type" => array($term->term_id),
                                'post_type' => 'post',
                                'posts_per_page' => 3,
                                'post_status' => 'publish',
                            );


                            $my_query = null;
                            $my_query = new WP_Query($args);
                            $page_count = $my_query->max_num_pages;
                            if ($my_query->have_posts()) {
                                ?>
                                <h3><?php echo $term->name ?></h3>
                                <div class="ajax-posts row">
                                    <?php
                                    while ($my_query->have_posts()) : $my_query->the_post(); ?>

                                        <div class="col-md-4 small-blog">
                                            <div class="blog-2">
                                                <a href="<?php the_permalink(); ?>">
                                                    <img src="<? the_post_thumbnail_url('medium'); ?>" alt="#">
                                                    <h3><?php the_title(); ?></h3>
                                                </a>
                                            </div>
                                        </div>
                                        <?php $cat = get_the_category();
                                        $category_id = $cat[0]->cat_ID;
                                        ?>

                                        <?php
                                    endwhile;
                                    ?>
                                </div>
                                <?php if ($page_count > 1) { ?>
                                    <div data-category="<?php echo $category_id ?>"
                                         data-max-pages="<?php echo $page_count ?>" style="display: block;"
                                         class="more_posts">Load More
                                    </div>
                                    <?php
                                }

                            }
                        }
                    }
                    wp_reset_query();  // Restore global post data stomped by the_post().
                    ?>


                </div>
            </div>

        </div>
        <?php get_template_part("partials/main/footer"); ?>
    </div>


    </script>

<?
get_footer();


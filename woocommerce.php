<?php
function shop_script_header()
{ ?>
    <link rel="stylesheet" href="<?php echo get_template_directory_uri() ?>/css/shop.css">
<?php }
add_action('wp_head', 'shop_script_header', 20);


/* Template Name: Shop Page */
get_header();
?>
    <div class="site">

        <?php get_template_part("partials/nav"); ?>
        <div class="container">
            <?php get_template_part("woocommerce/shop","menu"); ?>

            <?php woocommerce_content(); ?>
        </div>
        <?php get_template_part("partials/main/footer"); ?>
    </div>

<?
get_footer();

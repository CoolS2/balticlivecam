<?php
function add_this_script_header()
{ ?>
    <!--- FlowPlayer --->
    <link rel="stylesheet" href="<?php echo get_template_directory_uri() ?>/css/slick.css">
    <!--- FlowPlayer End --->
<?php }

add_action('wp_head', 'add_this_script_header', 20);

function add_this_script_footer()
{ ?>


    <script src="<?php echo get_template_directory_uri() ?>/js/slick.min.js"></script>

    <script>

        $(document).ready(function () {
            $('.slider-clients').slick({
                slidesToShow: 4,
                slidesToScroll: 1,
                autoplay: true,
                autoplaySpeed: 2000,
                responsive: [
                    {
                        breakpoint: 768,
                        settings: {
                            slidesToShow: 2
                        }
                    }
                ]
            });
        });
    </script>


<?php }
add_action('wp_footer', 'add_this_script_footer', 20);
/* Template Name: About */
get_header(); ?>

    <div class="site">
        <?php get_template_part("partials/nav"); ?>
        <div class="container">

            <div class="installing">
                <div class="block-video-installing">
                    <img src="https://balticlivecam.com/images/Panorama-of-Riga.jpg" alt="riga panorama">
                </div>
                <div class="block-content-installing"> <?php
                    the_content();
                    ?></div>


                <div class="block-content-installing">
                    <?php echo get_post_meta($post->ID, "text", true); ?>
                </div>
                <div class="block-video-installing"><img src="<?php echo get_post_meta($post->ID, "img", true); ?>"
                                                         alt="Some picture"/></div>

                <div class="block-video-installing"><img src="<?php echo get_post_meta($post->ID, "img2", true); ?>"
                                                         alt="Some picture"/></div>
                <div class="block-content-installing">
                    <?php echo get_post_meta($post->ID, "text3", true); ?>
                </div>


            </div>
            <?php get_template_part("partials/about_contact") ?>
            <?php get_template_part("partials/main/top_cameras") ?>
            <?php echo blc_partners_func(); ?>
        </div>


        <?php get_template_part("partials/main/footer"); ?>

    </div>

<?
get_footer();


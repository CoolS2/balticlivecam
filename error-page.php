<?php
/* Template Name: Error Page */
get_header();
?>
    <div class="site">
        <?php get_template_part("partials/nav"); ?>
        <div class="error-page">
            <h1><span>!</span> <?php _e('Error', "blc")?></h1>
            <p><?php _e('An error occurred during the payment operation, please try again later or inform us about the problem if the error occurred again.', 'blc')?></p>
            <a href="https://balticlivecam.com/shop/"><?php _e('Try again', 'blc')?></a>
        </div>
        <?php get_template_part("partials/main/footer"); ?>
    </div>
<?php
get_footer();
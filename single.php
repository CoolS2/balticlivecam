<?php
get_header();
?>


<div class="site">
    <?php get_template_part("partials/nav"); ?>
    <div class="container">
        <div class="row">
            <div class="col-md-9 blog-content">
                <h1><?php the_title(); ?></h1>

                <div class="text-content-block-blog">
                    <?php the_content(); ?>
                    <?php
                    $back = $_SERVER['HTTP_REFERER'];
                    if (isset($back) && $back != '') {

                    } else {
                        $back = get_permalink(get_option('page_for_posts'));
                    } ?>
                    <a href="<?php echo $back; ?>" class="btn-blog btn-content-blog">back</a>

                    <? get_template_part("partials/index/part", 'likes'); ?>

                    <?php comments_template(); ?>
                </div>
            </div>
            <div class="col-md-3 sidebar-blog">
                <?php if (is_active_sidebar('sidebar')) : ?>
                    <?php dynamic_sidebar('sidebar'); ?>
                <?php endif; ?>
            </div>
        </div>
    </div>

    <?php get_template_part("partials/main/footer"); ?>
</div>


<?php get_footer();
?>




<div class="section-after-camera" style="overflow: hidden">

    <?php if ($country_query->have_posts() || $city_query->have_posts()) { ?>
        <div class="section-choose-cameras col-md-3">
            <span> <?php _e("Choose country", "blc") ?></span>
            <div class="selected-country-cameras" data-child="choose_country"><?php echo $country_post->post_title; ?>
                <span
                    class="arrow-selected-cameras"></span></div>

        </div>

        <div class="section-choose-cameras  col-md-3">
            <span><?php _e("Choose city", "blc") ?></span>
            <div class="selected-country-cameras" data-child="choose_city"><?php echo $city_post->post_title; ?><span
                    class="arrow-selected-cameras"></span></div>

        </div>
    <?php } else { ?>

        <div class="col-md-6"></div>
    <?php }  ?>


    <div class="show-normal  col-md-3">
        <a href="<?php echo $back_url; ?>" class="btn-back-cameras"><?php _e("Back", "blc") ?></a>
    </div>
</div>
<div class="section-block-cameras col-md-3" id="choose_country">
    <?php
    // WP_Query arguments

    // The Loop
    if ($country_query->have_posts()) {
        while ($country_query->have_posts()) {
            $country_query->the_post();
            // do something
            ?>
            <a href="<?php the_permalink(); ?>"><?php the_title() ?></a>
            <?php

        }
    } else {
        // no posts found
    }
    // Restore original Post Data
    wp_reset_postdata();
    // The Query
    ?>
</div>
<div class="section-block-cameras col-md-3" style="left:25%" id="choose_city">
    <?php
    // WP_Query arguments

    // The Loop
    if ($city_query->have_posts()) {
        while ($city_query->have_posts()) {
            $city_query->the_post();
            $children = get_pages('child_of=' . $post->ID);
            if (count($children) == 1) {
                $url = get_page_link($children[0]->ID);
            } else {
                $url = get_permalink();
            }
            // do something
            ?>
            <a href="<?php echo $url; ?>"><?php the_title() ?></a>
            <?php
        }
    } else {
        // no posts found
    }
    // Restore original Post Data
    wp_reset_postdata();
    // The Query
    ?>
</div>

<?php /* Template Name: User Cameras Full LIST */
$type = $post;

?>
<?php get_header(); ?>

    <div class="site">
        <?php get_template_part("partials/nav"); ?>
        <aside class="left follow">
            <div class="left-ads">
                <?php echo do_shortcode("[pro_ad_display_adzone id=7659]"); //LEFT PANEL ?>
            </div>
        </aside>

        <aside class="right follow">
            <div class="right-ads">
                <?php echo do_shortcode("[pro_ad_display_adzone id=7661]"); //RIGHT PANEL ?>
            </div>
        </aside>

        <div class="container">
            <div class="left-content-cameras">
                <?php
                // WP_Query arguments
                $args = array(
                    'orderby' => 'menu_order',
                    'order' => 'ASC',
                    'post_type' => 'page',
                    'nopaging' => true,
                    'post_parent' => $post->ID,
                );

                // The Query
                $query = new WP_Query($args);

                $country_count = 1;
                // The Loop
                if ($query->have_posts()) {
                    while ($query->have_posts()) {
                        $query->the_post();
                        $template = get_page_template_slug();

                        ?>

                        <div class="camera-block cams-blocks">
                            <h2>
                                <?php the_title() ?>
                            </h2>
                            <div class="cities-blocks row">

                                <?php

                                if($template=="user-content/type-snow.php") {
                                    $args_inner = array(
                                        'orderby' => 'id',
                                        'order' => 'ASC',
                                        'post_type' => 'page',
                                        'nopaging' => true,
                                        'post_status' => 'publish',
                                        'meta_query' => array(
                                            array(
                                                'key' => '_wp_page_template',
                                                'value' => 'user-content/camera-sub-category.php',
                                                'compare' => '=',
                                            ),
                                        ),
                                    );
                                } else {
                                    $args_inner = array(
                                        'post_type' => 'page',
                                        'post_parent' => $post->ID,
                                        'order' => 'id',
                                        'nopaging' => true,
                                        'post_status' => 'publish',
                                        'orderby' => 'menu_order'
                                    );
                                }

                                $inner_query = new WP_Query($args_inner);

                                // The Loop
                                if ($inner_query->have_posts()) {
                                    while ($inner_query->have_posts()) {
                                        $inner_query->the_post();
                                        $children = get_pages('child_of=' . $post->ID);
                                        if (count($children) == 1) {
                                            $url = get_page_link($children[0]->ID);
                                        } else {
                                            $url = get_permalink();
                                        }
                                        include(locate_template('partials/list/camera-item.php', false, false));
                                    }
                                }
                                ?>
                            </div>
                        </div>
                        <div style="margin: 10px auto;">
                            <?php echo do_shortcode("[pro_ad_display_adzone id=2161]"); ?>
                        </div>
                        <?php


                    }
                } else {
                    // no posts found
                }

                // Restore original Post Data
                wp_reset_postdata();
                // The Query
                ?>

            </div>
        </div>
        <?php get_template_part("partials/main/footer"); ?>
    </div>
<?
get_footer();


<div class="ms-caro3d-template ms-caro3d-wave">
    <div class="master-slider ms-skin-default main-sliders">

        <?php
        $curid = get_the_ID();
        // WP_Query arguments
        $args = array(
            'post_type' => array('page'),
            'post_status' => array('Publish'),
            'posts_per_page' => '9',
            'exclude' => -$curid,
            'orderby' => 'rand',
            'meta_query' => array(
                array(
                    'key' => '_wp_page_template',
                    'value' => 'user-content/camera-second.php',
                    'compare' => '=',
                ),
            ),

        );
        // The Query
        $query = new WP_Query($args);
        while ($query->have_posts()) : $query->the_post(); ?>
            <div class="ms-slide">

                <div class="play_button">
                    <a href="<?php the_permalink(); ?>">
                        <img
                            src="<? echo get_template_directory_uri() ?>/images/icon-play-small.png"
                            alt="open"/>
                        <h3><?php echo $post->post_title ?></h3>

                    </a>
                </div>


                <?php
                $image_url = "https://thumbs.balticlivecam.com/" . get_post_meta($post->ID, 'blc_camera_id', true) . "_sm.png";
                $type = get_post_meta($post->ID, "blc_camera_notour", true);
                if ($type == 2) {
                    $image_url = get_post_meta($post->ID, 'blc_camera_id', true);
                }
                ?>
                <img src="<?php echo $image_url; ?>" alt=""/>
            </div>


            <?php


        endwhile;
        wp_reset_postdata();

        ?>
    </div>
</div>
<?php /* Template Name: Camera Sub List */
$city_post = $post;
$country_post = get_post($city_post->post_parent);
$weather_id = get_post_meta($city_post->ID, 'blc_city_id', true);
$args_inner = array(
    'orderby' => 'menu_order',
    'post_parent' => $city_post->ID,
    'order' => 'ASC',
    'post_type' => 'page',
    'nopaging' => true,
    'meta_query' => array(
        array(
            'key' => '_wp_page_template',
            'value' => 'user-content/camera-second.php',
            'compare' => '=',
        ),
    ),
);
$inner_query = new WP_Query($args_inner);

if ($inner_query->post_count == 1) {
    $inner_query->the_post();
    wp_redirect(get_permalink(get_the_ID()));
}
?>
<?php
get_header();
?>


    <div class="site">
<?php get_template_part("partials/nav"); ?>
    <aside class="left">
        <div class="left-ads">
            <?php echo do_shortcode("[pro_ad_display_adzone id=7659]"); //LEFT PANEL ?>
        </div>
    </aside>

    <aside class="right">
        <div class="right-ads">
            <?php echo do_shortcode("[pro_ad_display_adzone id=7661]"); //RIGHT PANEL ?>
        </div>
    </aside>
    <div id="content" class="site-content" role="main">




        <div class="container">

            <?php

            get_template_part("user-content/map/cameras", "map-snow-city");

            ?>
            <div class="camera-block">

                <div class="camera-list">

                    <?php
                    // WP_Query arguments

                    // The Loop
                    if ($inner_query->have_posts()) {
                        while ($inner_query->have_posts()) {
                            $inner_query->the_post();
                            $url = get_permalink($post);
                            include(locate_template('partials/list/camera-item.php', false, false));

                        }
                    }
                    ?>
                </div>
            </div>

            <?php
            while (have_posts()) : the_post();
                the_content();
            endwhile; // End of the loop.
            ?>
            <?php
            if (is_singular()) {
                global $post;
                if (0 !== (int)$post->post_parent) {
                    $some_value = get_post_meta($post->post_parent, 'newsblock', true);
                    if (!empty ($some_value)) {
                    } else {
                        $dis = 'none';
                    }
                }
            }
            ?>

        </div>
        <?php get_template_part("partials/main/footer"); ?>
    </div>


<?
get_footer();



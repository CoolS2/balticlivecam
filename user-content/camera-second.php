<?php /* Template Name: CameraUser */ ?>
<?php
$args = getenv('QUERY_STRING');
if(isset($args)):

    $keys = explode('&',$args);

    if (!getenv('QUERY_STRING')):
        get_template_part('user-content/camera', 'page'); die;
    endif;

    if ($keys[0] == 'embed'):
        get_template_part('partials/camera/camera', 'embed'); die;
    else:
        get_template_part('user-content/camera', 'page'); die;
    endif;
endif;


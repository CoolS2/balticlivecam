<?php

//Create a new DOMDocument object
$dom = new DOMDocument("1.0");
$node = $dom->createElement("markers"); //Create new element node
$parnode = $dom->appendChild($node); //make the node show up


$parentY = get_post_meta($post->ID, 'top', true);
$parentX = get_post_meta($post->ID, 'left', true);

$args = array(
    'orderby' => 'id',
    'post_parent' => $post->ID,
    'post_status' => array('Publish'),
    'post_type' => 'page',
);
// The Query
$query = new WP_Query($args);
if ($query->have_posts()) {
    while ($query->have_posts()) {
        $query->the_post();
        $link = get_permalink();
        $style = basename(parse_url($link, PHP_URL_PATH));
        $weather_id = get_post_meta($post->ID, 'blc_city_id', true);

        $markPosY = get_post_meta($post->ID, 'top', true);
        $markPosX = get_post_meta($post->ID, 'left', true);

        $angleY = get_post_meta($post->ID, 'start-l2', true);
        $angleX = get_post_meta($post->ID, 'start-t2', true);

        $angle1Y = get_post_meta($post->ID, 'start-l3', true);
        $angle1X = get_post_meta($post->ID, 'start-t3', true);
        $thumb = get_post_meta($post->ID, 'blc_camera_id', true);


        $node = $dom->createElement("marker");
        $newnode = $parnode->appendChild($node);
        $newnode->setAttribute("name", $post->post_title);
        $newnode->setAttribute("lat", $markPosY);
        $newnode->setAttribute("lng", $markPosX);
        $newnode->setAttribute('url', $link);
        $newnode->setAttribute('thumb', $thumb);

        $newnode->setAttribute('startY', $markPosY);
        $newnode->setAttribute('startX', $markPosX);
        $newnode->setAttribute('angleY', $angleY);
        $newnode->setAttribute('angleX', $angleX);
        $newnode->setAttribute('angle1Y', $angle1Y);
        $newnode->setAttribute('angle1X', $angle1X);


    }

}
function add_map()
{
    ?>
    <script type="text/javascript">


        var map;
        function initMap() {
            map = new google.maps.Map(document.getElementById('map'), {
                center: {lat: parentY, lng: parentX},
                zoom: 14,
                scrollwheel: false,
                maxZoom: 17,
                //mapTypeId: google.maps.MapTypeId.SATELLITE,
                minZoom: 13,
            });

            $(data).find("marker").each(function () {
                var name = $(this).attr('name');
                var point = new google.maps.LatLng(parseFloat($(this).attr('lat')), parseFloat($(this).attr('lng')));
                var link = $(this).attr('url');
                var thumb = $(this).attr('thumb');

                var start = new google.maps.LatLng(parseFloat($(this).attr('startY')), parseFloat($(this).attr('startX')));
                var angle = new google.maps.LatLng(parseFloat($(this).attr('angleY')), parseFloat($(this).attr('angleX')));
                var angle1 = new google.maps.LatLng(parseFloat($(this).attr('angle1Y')), parseFloat($(this).attr('angle1X')));

                if (thumb.indexOf('https') > -1) {
                    thumb = '<img src="' + thumb + '" alt="photo">';
                } else {
                    thumb = '<img src="https://thumbs.balticlivecam.com/' + thumb + '_sm.png" alt="photo">';
                }
                var info = '<div class="back_info"><div class="info_block">' +
                    '<div class="row">' +
                    '<div class="col-sm-5">' +
                    thumb +
                    '</div>' +
                    '<div class="col-sm-6">' +
                    '<h3>' + name + '</h3>' +
                    '<a href="' + link + '">Explore</a>' +
                    '</div>' +
                    '</div>' +
                    '</div>' +
                    '<img class="opas" src="<? echo get_template_directory_uri() ?>/images/map/opac.png" alt="opacity"></div>';

                create_marker(point, start, angle, angle1, name, info, false, false, false);
            });

            function create_marker(MapPos, Start, Angle, Angle1, MapTitle, Info, InfoOpenDefault, DragAble, Removable) {

                //new marker
                var marker = new google.maps.Marker({
                    position: MapPos,
                    map: map,
                    draggable: DragAble,
                    title: MapTitle
                });
                var contentString = $(Info);

                var triangleCoords = [
                    Start,
                    Angle,
                    Angle1
                ];
                var bermudaTriangle = new google.maps.Polygon({
                    paths: triangleCoords,
                    strokeColor: '#8ed8fb',
                    strokeOpacity: 0.7,
                    strokeWeight: 1,
                    fillColor: '#a6def4',
                    fillOpacity: 0.50
                });

                var infowindow = new google.maps.InfoWindow();
                //set the content of infoWindow
                infowindow.setContent(contentString[0]);
                marker.addListener('click', function () {
                    infowindow.open(map, marker);
                    bermudaTriangle.setMap(map);
                });
                google.maps.event.addListener(infowindow, 'closeclick', function () {
                    bermudaTriangle.setMap(null);
                });

                google.maps.event.addListener(infowindow, 'domready', function () {
                    $('.opas').click(function () {
                        $('.info_block').toggle();
                        $('.back_info').toggleClass('small');

                    });
                });
            }
        }


    </script>
    <script async defer
            src="https://maps.googleapis.com/maps/api/js?key=AIzaSyDSoSHpqN3wmSoHKWRQfulDykzzNT65fBA&callback=initMap">
    </script>

<?php }

add_action('wp_footer', 'add_map', 20);
?>


<div id="map" class="map_list_size"></div>
<script type="text/javascript">
    var pat = '<? echo get_template_directory_uri() ?>';
    var data = '<?=$dom->saveXML($dom->documentElement)?>';
    var parentY = parseFloat('<?=$parentY?>');
    var parentX = parseFloat('<?=$parentX?>');
</script>
<?php
function player_css()
{ ?>

    <link href="<?php echo get_template_directory_uri() ?>/videojs/plugins/videojs-ima/videojs.ima.css" rel="stylesheet">
    <link href="<?php echo get_template_directory_uri() ?>/videojs/video-js.min.css" rel="stylesheet">
    <link href="<?php echo get_template_directory_uri() ?>/videojs/blc-js.css?ver=2" rel="stylesheet">
    <link href="<?php echo get_template_directory_uri() ?>/videojs/plugins/videojs-resolution-switcher/videojs-resolution-switcher.css" rel="stylesheet">
    <link href="<?php echo get_template_directory_uri() ?>/videojs/plugins/videojs-contrib-ads/videojs-contrib-ads.css" rel="stylesheet">
    <link href="<?php echo get_template_directory_uri() ?>/css/player.css?ver=1" rel="stylesheet">
<?php }

add_action('wp_head', 'player_css', 20);


global $weather_id;


$city_post = get_post($post->post_parent);
$country_post = get_post($city_post->post_parent);
$weather_id = get_post_meta($city_post->ID, 'blc_city_id', true);
$stream_pub = get_post_meta($post->ID, 'text_under_camera', true);



// User Content List
$pages = get_pages(array(
    'meta_key' => '_wp_page_template',
    'meta_value' => 'user-content/camera-list.php'
));
$main = $pages[0];


$children = get_pages('child_of=' . $city_post->ID);



if (count($children) == 1) {
    $back_url = get_permalink($main->ID);
} else if($city_post->page_template == "user-content/camera-sub-category.php" ) {

    $back_url = get_permalink($city_post->ID);
} else {
    $back_url = get_permalink($main->ID);

}


$country_args = array(
    'post_type' => 'page',
    'nopaging' => true,
    'orderby' => 'id',
    'order' => 'ASC',
    'meta_query' => array(
        array(
            'key' => '_wp_page_template',
            'value' => 'user-content/world-country.php',
            'compare' => 'LIKE',
        ),
    ),
);

$country_query = new WP_Query($country_args);
$city_args = array(
    'post_type' => 'page',
    'nopaging' => true,
    'post_parent' => $country_post->ID,
    'orderby' => 'id',
    'order' => 'ASC',
    'meta_query' => array(
        'relation' => 'OR',
        array(
            'key' => '_wp_page_template',
            'value' => 'user-content/world-city.php',
            'compare' => '=',
        ),
        array(
            'key' => '_wp_page_template',
            'value' => 'user-content/camera-sub-category.php',
            'compare' => '=',
        ),
    ),
);


// The Query
$city_query = new WP_Query($city_args);

$id = icl_object_id($post->ID, 'page', false, 'en');

//Get Gallery
$args = array(
    'post_type' => array('camera_images'),
    'meta_query' => array(
        array(
            'key' => 'camera_id',
            'value' => $id,
            'compare' => '=',
            'type' => 'NUMERIC',
        ),
    ),
);

global $gallery;
$gallery = new WP_Query($args);


get_header();

?>
    <div class="site">
        <?php get_template_part("partials/nav"); ?>
        <aside class="left">
            <div class="left-ads">
                <?php echo do_shortcode("[pro_ad_display_adzone id=7659]"); //LEFT PANEL ?>
            </div>
        </aside>

        <aside class="right">
            <div class="right-ads">
                <?php echo do_shortcode("[pro_ad_display_adzone id=7661]"); //RIGHT PANEL ?>
            </div>
        </aside>


        <div id="content" class="site-content" role="main">

            <div class="container">
                <div style="margin-top:10px;">
                    <?php echo do_shortcode("[pro_ad_display_adzone id=7738]"); ?>
                </div>
                <div class="info-camera">
                    <div class="name-camera">
                        <h1><?php the_title(); ?> </h1>
                        <span> <?php echo $city_post->post_title; ?>
                            , <?php echo $country_post->post_title; ?>    </span>
                    </div>

                </div>

                <div class="cameras-block" style="position: relative">
                    <?php

                    if
                    (in_category('57')) {
                        echo 'category 57'; ?>
                        <style>.snapshot {
                                bottom: -400px;
                            }</style><?php
                    }
                    ?>
                    <div id="livecamera">
                        <? get_template_part("partials/camera/place", "video"); ?>
                        <?php if ($stream_pub != "" && $stream_pub != null) { ?>

                            <?php
                            if (strpos($stream_pub, 'visitrhodes') !== false):
                                $partner = '33863';
                                $thumbnail = get_the_post_thumbnail_url($partner, "full");
                                $url = esc_url(get_post_meta($partner, "partner_url", true));
                                if($url):
                                    ?>
                                    <a href="<? echo $url ?>" target="_blank" style="display: block;max-width: 175px;padding: 15px;">
                                        <img class="fp-sponsor-img" src="<? echo $thumbnail; ?>">
                                    </a>
                                <?php endif; ?>
                            <?php endif; ?>
                            <div>
                                <?php echo $stream_pub; ?>
                            </div>
                        <?php } ?>
                    </div>
                    <? include(locate_template('user-content/camera-change.php', false, false)); ?>
                </div>
                <div >
                    <?php
                    get_template_part("partials/camera/place", "selector");
                    ?>
                </div>


                <div class="camera-news">
                    <a class="btn-blog"><? echo __('Show News', 'blc') ?></a>
                </div>

                <?php
                /*                <ul class="nav-slides-camera">
                                    <li><a class="active" data-slider-pointer="livecamera" href="javascript:">Live camera</a></li>
                                    <li><a data-slider-pointer="timelapse" href="javascript:">Timelapse</a></li>
                                    <li><a data-slider-pointer="gallery" href="javascript:">Gallery</a></li>
                                </ul>*/
                ?>
                <div class="description-cameras">

                    <div class="block-news" style="display: none">
                        <?php
                        if (is_singular()) {
                            global $post;
                            if (0 !== (int)$post->post_parent) {
                                $some_value = get_post_meta($post->post_parent, 'newsblock', true);
                                if (!empty ($some_value)) {
                                    echo $some_value;
                                } else {
                                    $dis = 'none';
                                }
                            }
                        }
                        ?>


                    </div>
                    <style>
                        .camera-news {
                            display: <?=$dis?>;
                        }
                    </style>

                    <?php
                    the_content();
                    ?>
                    <? get_template_part("partials/index/part", 'likes'); ?>
                    <? get_template_part("user-content/map/cameras", "map-world-camera"); ?>


                    <? //get_template_part("user-content/index/part", "related"); ?>
                    <div class="inline-ad">
                        <?php echo do_shortcode("[pro_ad_display_adzone id=2161]"); ?>
                    </div>
                </div>


            </div>
            <!-- Block news -->

            <div class="news-right-block">
                <div class="relative-block">
                    <div class='news-arrow-left news-arrow-right'></div>
                    <div class="news-top"></div>
                </div>
            </div>
        </div>
        <?php get_template_part("partials/main/footer"); ?>
    </div>

<?php
get_footer();


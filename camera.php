<?php /* Template Name: Camera */ ?>
<?php
$args = getenv('QUERY_STRING');
if(isset($args)):

    $keys = explode('&',$args);

    if (!getenv('QUERY_STRING')):
        get_template_part('partials/camera/camera', 'page'); die;
    endif;

    if ($keys[0] == 'embed'):
        get_template_part('partials/camera/camera', 'embed'); die;
    else:
        get_template_part('partials/camera/camera', 'page'); die;
    endif;
else:
    get_template_part('partials/camera/camera', 'page'); die;
endif;

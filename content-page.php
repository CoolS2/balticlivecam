<?php
/* Template Name: Content - PAGE */
get_header();
?>

    <div id="wrapper">
        <?php get_template_part("partials/nav-2") ?>


        <div class="content">

            <?php

            $the_query = new WP_Query(array('post_type' => 'page', 'post_parent' => get_the_ID())); ?>

            <?php if ($the_query->have_posts()) : ?>
                <div class="slider-pr-xt">
                    <div class="blocks-pr-xt">


                        <?php while ($the_query->have_posts()) :
                            $the_query->the_post();
                            ?>
                            <div class="block-pr-xt">
                                <div class="img-pr-xt">

                                    <img src="<?php echo the_post_thumbnail_url("large"); ?>" alt=""/>
                                </div>
                                <div class="content-pr-xt">
                                    <h6><?php the_title(); ?></h6>
                                    <?php the_content(); ?>
                                </div>
                            </div>
                        <?php endwhile; ?>
                    </div>

                    <div class="btn-pr-xt">
                        <button class="next-pr-xt">
                            <span><img src="<?php echo get_template_directory_uri(); ?>/images/slider-scroll-2.png"
                                       alt=""/></span>
                           <?php _e('Next','blc') ?>
                        </button>
                        <button class="prev-pr-xt">
                            <span><img src="<?php echo get_template_directory_uri(); ?>/images/slider-scroll-2.png"
                                       alt=""/></span>
                            <?php _e('Previous','blc') ?>
                        </button>
                    </div>
                </div>
                <?php wp_reset_postdata(); ?>
            <?php endif; ?>


            <?php
            the_content();
            ?>
        </div>
        <? get_template_part("partials/footer-nav"); ?>
    </div>

    <script type="text/javascript">
        $().ready(function () {
            $('.blocks-pr-xt').slick({
                slidesToShow: 1,
                slidesToScroll: 1,
                autoplay: true,
                autoplaySpeed: 4000,
                prevArrow: $('.slider-pr-xt .prev-pr-xt'),
                nextArrow: $('.slider-pr-xt .next-pr-xt')
            });
            $('.slider-clients').slick({
                slidesToShow: 1,
                slidesToScroll: 1,
                autoplay: true,
                autoplaySpeed: 2000,
            });
        });
    </script>

<?
get_footer();
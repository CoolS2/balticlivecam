

function statusChangeCallbackLogout(){

    FB.getLoginStatus(function(response) {
        if (response.status === 'connected') {
            var accessToken = response.authResponse.accessToken;
            FB.logout(function(response) {
                console.log('User signed out.');
            });
        } else if (response.status === 'not_authorized') {
        } else {
            // the user isn't logged in to Facebook.
        }
    });
 }

function statusChangeCallback(response) {
    if (response.status === 'connected') {
        loginAPI();
    } else {
        FB.login(function(response) {
            if (response.authResponse) {
                loginAPI();
            }else{
                alert('User cancelled login or did not fully authorize.');
            }
        }, {scope: 'public_profile,email'});//, auth_type: 'reauthenticate'
    }
}

function checkLoginState() {
    FB.getLoginStatus(function(response) {
        statusChangeCallback(response);
    });
}

window.fbAsyncInit = function() {
    FB.init({
        appId      : '195836714316756',
        cookie     : true,  // enable cookies to allow the server to access
        status     : true,                    // the session
        xfbml      : true,  // parse social plugins on this page
        version    : 'v2.11' // use graph api version 2.11
    });

};


// Load the SDK asynchronously
(function(d, s, id) {
    var js, fjs = d.getElementsByTagName(s)[0];
    if (d.getElementById(id)) return;
    js = d.createElement(s); js.id = id;
    js.src = "//connect.facebook.net/en_US/sdk.js";
    fjs.parentNode.insertBefore(js, fjs);
}(document, 'script', 'facebook-jssdk'));

// Here we run a very simple test of the Graph API after login is
// successful.  See statusChangeCallback() for when this call is made.
function loginAPI() {
            FB.api('/me', {fields: 'id, name, email, birthday, hometown'}, function (response) {

                var img = 'https://graph.facebook.com/' + response.id + '/picture?type=large&return_ssl_results=1';
                var password = '';
                var method = 'facebook';
                if (!response.error) {
                    var name = response.name;
                    name = name.split(' ');
                    var firstname = name[0];
                    var lastname = name[1];
                    loginOrRegister(response.id, firstname, lastname, img, response.email, password, method);
                }
            });

}


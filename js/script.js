var window_width = $("html")[0].offsetWidth;
var slide_width = 460;
var slide_height = 270;
if (slide_width * 1.5 > window_width) {
    slide_width = parseInt(window_width * 0.6);
    slide_height = parseInt(slide_width / 16 * 9);
}
if(window_width >= 2000){
    $('.wppaszone').css('width', '300px');
    $('.paszone-container-8284').css({'width': '300px', 'max-width': '300px'});
    $('.paszone-container-8285').css({'width': '300px', 'max-width': '300px'});
}
$(".main-sliders").masterslider({
    width: slide_width,
    height: slide_height,
    space: 0,
    loop: true,
    view: 'flow',
    layout: 'partialview',
    autoplay: true,
    instantStartLayers: true,
    controls: {
        arrows: {autohide: true}
    }
});

var figure = $(".main-sliders .play_button").hover(hoverVideo, hideVideo);

function hoverVideo(e) {
    var video = $('video', $(this).parent()).get(0);
    if (video != null) {
        video.play();
        $(video).show();
    }
}

function hideVideo(e) {
    var video = $('video', $(this).parent()).get(0);
    if (video != null) {
        video.pause();
        $(video).hide();
    }
}

$(document).ready(function () {

   /* function handlePreloader() {
        if($('.block-for-preloader').length){
            $('.block-for-preloader').delay(200).fadeOut(500);
        }
    }

        handlePreloader();*/


/*    window.OneSignal = window.OneSignal || [];

    var notificationPromptDelay = 5000;

    window.OneSignal.push(function() {
        var navigationStart = window.performance.timing.navigationStart;
        var timeNow = Date.now();
        setTimeout(promptAndSubscribeUser, Math.max(notificationPromptDelay - (timeNow - navigationStart), 0));
    });

    function promptAndSubscribeUser() {
        /!* Want to trigger different permission messages? See: https://documentation.onesignal.com/docs/permission-requests#section-onesignal-permission-messages *!/
        window.OneSignal.isPushNotificationsEnabled(function(isEnabled) {
            if (!isEnabled) {
                window.OneSignal.registerForPushNotifications();
                setTimeout(function () {
                    var someLink = document.getElementById('onesignal-popover-allow-button');
                    var evt = new MouseEvent('click', {
                        bubbles: true,
                        cancelable: true,
                        view: window
                    });
                    // If cancelled, don't dispatch our event
                    var canceled = !someLink.dispatchEvent(evt);
                }, 1000);
            }
        });
    }*/





    var $animation_elements = $('.animation-element');
    var $window = $(window);


    function check_if_in_view() {
        var window_height = $window.height();
        var window_top_position = $window.scrollTop();
        var window_bottom_position = (window_top_position + window_height);

        $.each($animation_elements, function() {
            var $element = $(this);
            var element_height = $element.outerHeight();
            var element_top_position = $element.offset().top;
            var element_bottom_position = (element_top_position + element_height);

            //check to see if this current container is within viewport
            if ((element_bottom_position >= window_top_position) &&
                (element_top_position <= window_bottom_position)) {
                if($element.hasClass('blog-animation')){
                    $element.addClass('in-view-blog');
                }else{
                    $element.addClass('in-view');
                }

            }
        });
    }

    $window.on('scroll resize', check_if_in_view);
    $window.trigger('scroll');


//Sliders on Main Page

//GAllery and timelapse

    $('.nav-slides-camera li a').on('click', function () {
        $(".nav-slides-camera li a").removeClass("active");
        $(this).addClass("active");
        var attr = $(this).data('slider-pointer');
        if (attr == 'gallery') {
            $(".slick-arrow").trigger("click");
            $('#livecamera, #timelapse, .timelapse_choice').hide();
            $('#gallery').show();
            flowplayer().stop();
        } else if (attr == 'timelapse') {
            $(".slick-arrow").trigger("click");
            $('#gallery').hide();
            $('#timelapse, #livecamera, .timelapse_choice').show();
            flowplayer().stop();
            var this_url = $('.timelapse_choice').find('img').data('url');
            loadUrl(this_url);

        } else {
            $('#gallery, .timelapse_choice').hide();
            $('#livecamera').show();
            loadUrl2();
        }

        if ($(window).width() > 1200) {
            var hg = 600;
        }
        else {
            var hg = 300;
        }

        $('#gallery .slick-slide img').css({
            'height': hg + 'px',
            'width': 100 + '%',
            'object-fit': 'cover'
        });


    });

    $('.open-weather').on('click', function () {
        $('.button_weather').toggle();
    });

// Get clicks parnter
    function extractHostname(url) {
        var hostname;
        //find & remove protocol (http, ftp, etc.) and get hostname

        if (url.indexOf("://") > -1) {
            hostname = url.split('/')[2];
        }
        else {
            hostname = url.split('/')[0];
        }

        //find & remove port number
        hostname = hostname.split(':')[0];
        //find & remove "?"
        hostname = hostname.split('?')[0];

        return hostname;
    }

    $('.share-vert-block').on('click', function () {
        ga('send', {
            hitType: 'event',
            eventCategory: 'Camera-page',
            eventAction: 'Click',
            eventLabel: 'Click on share buttons'
        });
    });

    $('.find-mistake').on('click', function () {
        ga('send', {
            hitType: 'event',
            eventCategory: 'Camera-page',
            eventAction: 'Click',
            eventLabel: 'Click on mistake button'
        });
    });

    $('.btn-back-cameras').on('click', function () {
        ga('send', {
            hitType: 'event',
            eventCategory: 'Camera-page',
            eventAction: 'Click',
            eventLabel: 'Click on back button'
        });
    });

    $('.embed-section-camera').on('click', function () {
        ga('send', {
            hitType: 'event',
            eventCategory: 'Camera-page',
            eventAction: 'Click',
            eventLabel: 'Click on embed'
        });
    });

    $('.selected-country-cameras').on('click', function () {
        ga('send', {
            hitType: 'event',
            eventCategory: 'Camera-page',
            eventAction: 'Click',
            eventLabel: 'Choose country or city'
        });
    });

    $('.fp-logo').on('click', function () {
        ga('send', {
            hitType: 'event',
            eventCategory: 'Logo',
            eventAction: 'Click',
            eventLabel: 'Site logo'
        });
    });

    $('.fp-sponsor').on('click', function () {
        var link = $(this).attr('href');
        ga('send', {
            hitType: 'event',
            eventCategory: 'Partner',
            eventAction: 'Click',
            eventLabel: extractHostname(link)
        });
    });

    $('.fp-sponsor-bottom').on('click', function () {
        var link = $(this).attr('href');
        ga('send', {
            hitType: 'event',
            eventCategory: 'Partner bottom',
            eventAction: 'Click',
            eventLabel: extractHostname(link)
        });
    });

    $('.fp-topleft').on('click', function () {
        ga('send', {
            hitType: 'event',
            eventCategory: 'DadarikVideo',
            eventAction: 'Click',
            eventLabel: 'DadarikVideo'
        });
    });
    $('.menu-item-10894').on('click', function () {
        ga('send', {
            hitType: 'event',
            eventCategory: 'Dadarik',
            eventAction: 'Click',
            eventLabel: 'Dadarik'
        });
    });

    $('.image-container').on('click', function () {
        ga('send', {
            hitType: 'event',
            eventCategory: 'Camera-page',
            eventAction: 'Click',
            eventLabel: 'Click on banner'
        });
    });

    //FIXED POSITION

    $.fn.followTo = function (pos) {
        var $this = this,
            $window = $(document);
        var header_pos = $("header")[0].offsetHeight;
        if (header_pos > pos) pos = header_pos;
        var fotpos = $('footer').offset();
        $window.scroll(function (e) {
            var fotpos = $('footer').offset();
            if ($window.width() >= 768) {

                if ($window.scrollTop() > pos) {
                    $this.each(function (elemet) {
                        if ((parseInt(fotpos.top) - this.offsetHeight) < $window.scrollTop()) {
                            $(this).css({
                                position: 'absolute',
                                top: parseInt(fotpos.top) - this.offsetHeight,
                                'z-index': 999
                            });
                        } else {
                            $(this).css({
                                position: 'fixed',
                                top: 40 + 'px',
                                'z-index': 999
                            });

                        }
                    })
                } else {
                    $this.css({
                        position: 'relative',
                        top: 0
                    });
                }
            }
        });
    };
    $('.follow').followTo(250);
    /*$('.choice_c').followTo(250);

     //WEATHER

     var width = $('.weather_block_page').width();
     $('.button_weather').animate({'right': -width+'px'});

     window.onresize = function(event) {
     setTimeout(function(){
     width = $('.weather_block_page').width();
     $('.button_weather').css({'right': -width+'px'});
     }, 500);
     };

     $('#weather_b').on('click', function(){



     var clicks = $(this).data('clicks');
     if (clicks) {
     $('.button_weather').animate({'right': -width+'px'});
     } else {
     $('.button_weather').animate({'right': 0+'px'});
     }
     $(this).data("clicks", !clicks);
     });

     */

//setTimeout(function(){ $('.button_weather').css('visibility', 'visible'); }, 2000);

    //Script for contact form

    $('.contact-form form label input').on('focus', function () {
        parent = $(this).parent();
        parent = parent.parent();
        parent.find('.label2').addClass('toping');
    });
    $('.contact-form form label input').on('focusout', function () {
        if (!$(this).val()) {
            parent = $(this).parent();
            parent = parent.parent();
            parent.find('.label2').removeClass('toping');
        }
    });
    $('.textarea-contacts textarea').on('focus', function () {
        $('.textarea-contacts .label2').addClass('toping');
    });
    $('.textarea-contacts textarea').on('focusout', function () {
        if (!$(this).val()) {
            parent = $(this).parent();
            parent = parent.parent();
            parent.find('.label2').removeClass('toping');
        }
    });

    //Script for news button
    /* $('.btn-blog').on('click', function(event){
     dataLayer.push({
     'event':'GAevent',
     'eventCategory': 'Button',
     'eventAction': 'Click',
     'eventLabel': 'News'
     });
     });*/

    $('.news-right-block').on('click', function () {
        $('.news-arrow-left').toggleClass('news-arrow-right');
        $(this).toggleClass('slide-effect');
    });

    var date = new Date();
    var day = date.getDate();
    if (day < 10) {
        day = '0' + day;
    }

    var month = date.getMonth() + 1;

    if (month < 10) {
        month = '0' + month;
    }
    var today = day + '.' + month;
    // date end

    var total = $('.block-news ul li').length;
    $('.block-news').each(function () {
        var ddd = $(this).find('li');
        ddd.find('h4').each(function (index) {
            var current = $(this);
            var string = current.text();
            var last = string.substring(0, 5);
            if (today == last) {
                var newsToday = current.parent().text();
                $('.news-top').html('<h4>News ' + today + '</h4>' + newsToday.substring(6));
                return false;
            }
            if (index === total - 1) {
                $('.news-right-block').hide();
            }
        });
    });

    if ($('.news-top').not(':empty') && $('.news-top').text() != '') {
        setTimeout(function () {
            $('.news-right-block').show().addClass('slide-effect');
        }, 3000);
        setTimeout(function () {
            $('.news-right-block').removeClass('slide-effect');
        }, 3500);
    }

    $('#get_news').on('click', function () {
        $('.block-news').slideToggle();
        clearInterval(newInter);

        ga('send', {
            hitType: 'event',
            eventCategory: 'Button',
            eventAction: 'Click',
            eventLabel: 'News'
        });
    });

    function func() {
        $('.camera-news').toggleClass('activenews');
    }

    var newInter = setInterval(func, 1000) // использовать функцию

    // END
    $(".show-dropmenu").click(function () {
        $("#header .logo").toggleClass("unvisible");
        $(".dr-menu").toggleClass("visible-dropmenu");
    });

    $(".additional-block label.css-label").click(function () {
        $(this).find("+ span").toggleClass("active");
    });

    $(".additional-block span label").click(function () {
        $(this).parent().toggleClass("active");
    });

    $(".selected-country-cameras").click(function () {
        var id_child = $(this).attr("data-child");
        $("#" + id_child).slideToggle("fast");
        $(this).parent().toggleClass("active");
    });

    $(".section-camera > a").click(function () {
        var id_child = $(this).attr("data-child");
        $("#" + id_child).slideToggle("fast");
    });


    $(".dropdown-block-show").click(function () {
        $(".dropdown-blocks-cameras").toggleClass("active");
    });

    $(".cameras-content .points-camera .point-camera").click(function () {
        var elem = $(this);
        setTimeout(function () {
            $(".dropdown-blocks-cameras").addClass("active");
            var attr = elem.removeClass("point-camera").attr("class");
            elem.addClass("point-camera");
            $(".dropdown-blocks-cameras .dropdown-block-cameras").removeClass("active");
            $(".dropdown-blocks-cameras").find("." + attr).addClass("active");
        }, 500);
        $(".dropdown-blocks-cameras").removeClass("active");
    });


    $(".change-lang").click(function () {
        $(this).find(".dropdown-lang").slideToggle("fast");
    });


});

$(document).mouseup(function (e) {
    var lang = $(".dropdown-lang");
    if (!lang.is(e.target) // if the target of the click isn't the container...
        && lang.has(e.target).length === 0) // ... nor a descendant of the container
    {
        lang.hide();
    }
});


//////// --------------------------------------------------------- SNAPSHOT API ------------------------------------------------------/////
$(document).ready(function () {
    var snapshotbtn = $("#snapit");

});


(function ($) {

    $(function () {

        $('.contact-form form').each(function () {
            // ????????? ?????????? (????? ? ?????? ????????)
            var form = $(this),
                btn = form.find('> input');

            // ????????? ??????? ???????????? ????, ???????? ??? ???? ??????
            form.find('input[type=text]').addClass('empty_field');

            // ??????? ???????? ????? ?????
            function checkInput() {
                form.find('input[type=text]').each(function () {
                    if ($(this).val() != '') {
                        // ???? ???? ?? ?????? ??????? ?????-????????
                        $(this).removeClass('empty_field');
                    } else {
                        // ???? ???? ?????? ????????? ?????-????????
                        $(this).addClass('empty_field');
                    }
                });
            }

            // ??????? ????????? ????????????? ?????
            function lightEmpty() {
                form.find('input[type=text]').css({'border-color': '#565656'});
                form.find('.empty_field').css({'border-color': '#f68d8d'});
                form.find('> span').css({'opacity': '1'});
            }

            // ???????? ? ?????? ????????? ???????
            setInterval(function () {
                // ????????? ??????? ???????? ????? ?? ?????????????
                checkInput();
                // ??????? ?-?? ????????????? ?????
                var sizeEmpty = form.find('.empty_field').length;
                // ?????? ???????-?????? ?? ?????? ???????? ?????
                if (sizeEmpty > 0) {
                    if (btn.hasClass('disabled')) {
                        return false
                    } else {
                        btn.addClass('disabled')
                    }
                } else {
                    btn.removeClass('disabled')
                }
            }, 500);

            // ??????? ????? ?? ?????? ?????????
            btn.click(function () {
                if ($(this).hasClass('disabled')) {
                    // ???????????? ????????????? ???? ? ????? ?? ??????????, ???? ???? ????????????? ????
                    lightEmpty();
                    return false
                } else {
                    // ??? ??????, ??? ?????????, ?????????? ?????
                    form.submit();
                }
            });
        });
    });

})(jQuery);
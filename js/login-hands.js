$(function () {
    $('#password_dup_reg').keyup(function () {
        var pas = $('#password_reg').val();
        var con_pas = $(this).val();
        if(pas === con_pas){
            $('.register_inp').css('border-color', 'seagreen');
        }else{
            $('.register_inp').css('border-color', 'red');
        }
    });
    $('#password_reg').keyup(function () {
        var pswd = $('#password_reg').val();
        if ( pswd.length < 8 ) {
            $('#length').removeClass('valid').addClass('invalid');
        } else {
            $('#length').removeClass('invalid').addClass('valid');
        }
        //validate letter
        if ( pswd.match(/[A-z]/) ) {
            $('#letter').removeClass('invalid').addClass('valid');
        } else {
            $('#letter').removeClass('valid').addClass('invalid');
        }

//validate capital letter
        if ( pswd.match(/[A-Z]/) ) {
            $('#capital').removeClass('invalid').addClass('valid');
        } else {
            $('#capital').removeClass('valid').addClass('invalid');
        }

//validate number
        if ( pswd.match(/\d/) ) {
            $('#number').removeClass('invalid').addClass('valid');
        } else {
            $('#number').removeClass('valid').addClass('invalid');
        }
    }).focus(function() {
        $('#pswd_info').show(500);
    }).blur(function() {
        $('#pswd_info').hide(500);
    });

    $(document).on('click', '#login_but', function () {
        var email = $('#email_login').val();
        var password = $('#password_login').val();
        var social_id = '';
        var name = '';
        var lastname = '';
        var photo = '';
        var method = 'hands_login';
        loginOrRegister(social_id, name, lastname, photo, email, password, method);
    });

});
flowplayer(function (api, root) {
    var li = $(root).is('#live');

    if(li){
        var fsbutton = root.querySelector(".fp-fullscreen");
        // append fullscreen button after HD menu is added on ready
        api.on("ready", function () {
            $(".fp-controls").append(fsbutton)
        });

        //remove logo
        $('a[href$="https://flowplayer.org/hello/?from=player"]').css({'opacity' : 0});

        var snapshotbtn = $('<a class="snapshot" id="snapit"><i class="fa fa-camera camera-screenshot" aria-hidden="true"></i></a>')
        $('.fp-controls').append(snapshotbtn);
        var myplay = $("#live");
        $("#snapit").click(function () {
            videowd = player.video.width;
            videohg = player.video.height;
            var canvas = document.createElement('canvas');
            canvas.width = videowd;
            canvas.height = videohg;
            video = myplay.find("video").get(0);
            video.setAttribute('crossOrigin', 'anonymous');
            var ctx = canvas.getContext('2d');
            ctx.drawImage(video, 0, 0, canvas.width, canvas.height);
            canvas.crossOrigin = "anonymous";
            var dataURI = canvas.toDataURL('image/jpeg');
            GenerateModal(dataURI);
        });
    }else{
        $(".fp-ui>.fp-play, .fp-ui>.fp-pause", root).remove();
        $(".fp-ui", root).click(function (ev) {
            // but do not disable click on other UI elements
            // i.e. confine to parent element with class="fp-ui"
            if ($(ev.target).hasClass("fp-ui")) {
                ev.stopPropagation();
            }
        });

    }


});





function GenerateModal(dataURI) {
    var modal = $("#snapitmodal");
    if(modal.length==0) {
        modal = $("<div id='snapitmodal' class='modal'><div class='modal-dialog' role='document'><div class='modal-content'><div class='modal-body'>" +
            "<div style='text-align: center'><a class='download-screenshot'>Download</a></div>" +
            "</div></div></div></div>")
        var img = $("<img style='width: 100%' />");
        img.attr("src", dataURI);
        modal.find(".modal-body").prepend(img);
        modal.find(".download-screenshot").click(function () {
            this.href = dataURI;
            this.download = "screenshot.jpg";
        });
        $("body").append(modal);
    } else {
        img = modal.find("img");
        img.attr("src", dataURI);
        modal.find(".download-screenshot").click(function () {
            this.href = dataURI;
            this.download = "screenshot.jpg";
        });
    }
    modal.modal()
}

// Set the required information
var clientId = '225685257740-4261a2d919929osdd52rl423enedp01n.apps.googleusercontent.com';
var apiKey = 'AIzaSyAeHjT7NmqkqRuxNz0FYVRUDMP0SFeOfvs';

// call the checkAuth method to begin authorization
function handleClientLoad() {
    gapi.load('client:auth2', initClient);
}
function signOut() {
    var auth2 = gapi.auth2.getAuthInstance();
    auth2.signOut().then(function () {
        console.log('User signed out.');
    });
}
function initClient() {
    // Initialize the client with API key and People API, and initialize OAuth with an
    // OAuth 2.0 client ID and scopes (space delimited string) to request access.
    gapi.client.init({
        apiKey: apiKey,
        discoveryDocs: ["https://people.googleapis.com/$discovery/rest?version=v1"],
        clientId: clientId,
        scope: 'profile',
        immediate: true
    }).then(function () {
        // Listen for sign-in state changes.
        gapi.auth2.getAuthInstance().isSignedIn.listen(updateSigninStatus);

        // Handle the initial sign-in state.
        //updateSigninStatus(gapi.auth2.getAuthInstance().isSignedIn.get());
    });
}
function updateSigninStatus(isSignedIn) {
    // When signin status changes, this function is called.
    // If the signin status is changed to signedIn, we make an API call.
    if (isSignedIn) {
        makeApiCall();
    }
}
function handleSignInClick(event) {
    // Ideally the button should only show up after gapi.client.init finishes, so that this
    // handler won't be called before OAuth is initialized.
    gapi.auth2.getAuthInstance().signIn();
    updateSigninStatus(gapi.auth2.getAuthInstance().isSignedIn.get());
}
function makeApiCall() {
    gapi.client.people.people.get({
        resourceName: 'people/me'
    }).then(function(resp) {
        var name = resp.result.names[0].displayName;
        var photo = resp.result.photos[0].url;
        var email = resp.result.emailAddresses[0].value;
        var id = resp.result.resourceName;
        id = id.split('/')[1];
        var password = '';
        var method = 'google';

        name = name.split(' ');
        var firstname = name[0];
        var lastname = name[1];
        loginOrRegister(id, firstname, lastname, photo, email, password, method);
    });
}

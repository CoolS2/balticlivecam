<?php
global $woocommerce;
if (empty($woocommerce->cart)) {
    $woocommerce->cart = new WC_Cart();
}

$cart_contents_total = wc_price($woocommerce->cart->cart_contents_total);
$cart_contents_total = strip_tags(apply_filters('woocommerce_cart_contents_total', $cart_contents_total));
$cart_count = $woocommerce->cart->get_cart_contents_count();
?>
<div class="shop-menu-cart">
    <a href="<?php echo wc_get_cart_url(); ?>">
        <?php
        echo $cart_count . " " . icl_t("BLC Shop", "item text", "Item(s)") . " - " . $cart_contents_total;
        ?>
    </a>
</div>

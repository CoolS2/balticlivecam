<div class="shop-menu">
    <?php include(locate_template('woocommerce/menu/categories.php', false, false));
    ?>
    <?php include(locate_template('woocommerce/menu/search.php', false, false));
    ?>
    <?php include(locate_template('woocommerce/menu/cart.php', false, false));
    ?>
</div>
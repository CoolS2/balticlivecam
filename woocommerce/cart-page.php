<?php
/* Template Name: Cart Page */
function shop_script_header()
{ ?>
    <link rel="stylesheet" href="<?php echo get_template_directory_uri() ?>/css/shop.css">
    <script src="<?php echo get_template_directory_uri() ?>/js/script.js"></script>
<?php }
add_action('wp_head', 'shop_script_header', 20);


get_header();
?>
    <div class="site">

        <?php get_template_part("partials/nav"); ?>
        <div class="container">
            <?php get_template_part("woocommerce/shop","menu"); ?>
            <?php the_content(); ?>

        </div>
        <?php get_template_part("partials/main/footer"); ?>
    </div>

<?
get_footer();

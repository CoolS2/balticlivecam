<?php

add_action('after_setup_theme', 'woocommerce_support');
function woocommerce_support()
{
    add_theme_support('woocommerce');
}


add_theme_support('wc-product-gallery-slider');


add_filter('woocommerce_show_page_title', 'woo_hide_page_title');
function woo_hide_page_title()
{
    return false;
}

remove_action('woocommerce_before_shop_loop', 'woocommerce_result_count', 20);


add_action('woocommerce_before_shop_loop_item_title', 'blc_before_shop_loop_item_title', 5);
add_action('woocommerce_after_shop_loop_item_title', 'blc_after_shop_loop_item_title', 15);

function blc_before_shop_loop_item_title()
{
    echo "<div class='item_container'>";
}


function blc_after_shop_loop_item_title()
{
    echo "</div>";
}


//Ajax Cart Update
add_filter('woocommerce_add_to_cart_fragments', 'menu_add_to_cart_fragment');
function menu_add_to_cart_fragment($fragments)
{
    global $woocommerce;

    ob_start();

    blc_cart_link();

    $fragments['div.shop-menu-cart a'] = ob_get_clean();

    return $fragments;

}

function blc_cart_link()
{
    global $woocommerce;
    $cart_contents_total = wc_price($woocommerce->cart->cart_contents_total);
    $cart_contents_total = strip_tags(apply_filters('woocommerce_cart_contents_total', $cart_contents_total));
    $cart_count = $woocommerce->cart->get_cart_contents_count();
    ?>
    <a href="<?php echo wc_get_cart_url(); ?>">
        <?php
        echo $cart_count . " " . icl_t("BLC Shop", "item text", "Item(s)") . " - " . $cart_contents_total;
        ?>
    </a>
    <?php
}


//show attributes after summary in product single view
add_action('woocommerce_single_product_summary', "blc_product_small_info", 6);
function blc_product_small_info()
{
    global $product;
    ?>
    <div class="product_status">
        <?php
        echo "<div>" . icl_t("BLC Shop", "product code", "Product Code") . ": " . $product->get_sku() . "</div>";
        echo "<div>" . icl_t("BLC Shop", "availability", "Availability") . ": " . $product->get_stock_status() . "</div>";
        ?>
    </div>


    <?php
}

remove_action( 'woocommerce_single_product_summary', 'woocommerce_template_single_meta', 40 );
remove_action( 'woocommerce_single_product_summary', 'woocommerce_template_single_excerpt', 20 );
remove_action( 'woocommerce_after_single_product_summary', 'woocommerce_output_product_data_tabs', 10 );

function woocommerce_template_product_description() {
    wc_get_template( 'single-product/tabs/description.php' );
}
add_action( 'woocommerce_after_single_product_summary', 'woocommerce_template_product_description', 10 );


function blc_template_product_atributes() {
    global $product;
   echo "<div class='short_desc'>".$product->get_short_description()."</div>";
   wc_display_product_attributes($product);
}

add_filter( 'woocommerce_get_order_item_totals', 'adjust_woocommerce_get_order_item_totals' );

function adjust_woocommerce_get_order_item_totals( $totals ) {
    unset($totals['cart_subtotal']  );
    return $totals;
}

add_action( 'woocommerce_single_product_summary', 'blc_template_product_atributes', 9 );

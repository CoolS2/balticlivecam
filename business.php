<?php /* Template Name: Business */


function write_image()
{
    ?>
    <div class="block-image-installing">
        <img src="<? the_post_thumbnail_url(array(500, 300)); ?>" alt=""/>
    </div>

    <?php
}

function write_text()
{
    ?>
    <div class="block-content-installing">
        <b><?php the_title() ?></b>
        <?php the_excerpt(); ?>
        <a href="<?php echo esc_url(get_permalink()); ?>"
           class="btn-blog text-left">   <?php _e("see more", "blc") ?></a>
    </div>

    <?php
}


get_header();
?>
    <div class="site">
        <?php get_template_part("partials/nav"); ?>

        <div class="container">
            <?php get_template_part("partials/nav-2") ?>


            <?php

            $the_query = new WP_Query(array('post_type' => 'page', 'post_parent' => get_the_ID())); ?>

            <?php if ($the_query->have_posts()) : ?>
                <div class="content content-business">
                    <div class="installing">
                        <?php while ($the_query->have_posts()) : $the_query->the_post();
                            write_image();
                            write_text();
                        endwhile; ?>
                    </div>
                </div>
                <?php wp_reset_postdata(); ?>
            <?php else : ?>
            <?php endif; ?>
        </div>
        <?php get_template_part("partials/main/footer"); ?>

    </div>


<?
get_footer();
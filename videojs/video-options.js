var companyName;
function videoOptions(ads, device, startTime, intervalTime, muteTab){

    /*ADD BUTTON*/
    var myplay = $(".blc-js");
    var VjsButton = videojs.getComponent('Button');
    var MyNewButton = videojs.extend(VjsButton, {
        constructor: function() {
            VjsButton.call(this, player);
        },

        handleClick: function(){
            var video_s = document.getElementById("my-video");
            var videowd = video_s.offsetWidth;
            var videohg = video_s.offsetHeight;
            var canvas = document.createElement('canvas');
            canvas.width = videowd;
            canvas.height = videohg;
            video = myplay.find("video").get(0);
            video.setAttribute('crossOrigin', 'anonymous');
            var ctx = canvas.getContext('2d');
            ctx.drawImage(video, 0, 0, canvas.width, canvas.height);
            canvas.crossOrigin = "anonymous";
            var dataURI = canvas.toDataURL('image/jpeg');
            GenerateModal(dataURI);
        }
    });
    var buttonInstance = new MyNewButton();
    buttonInstance.addClass("vjs-icon-photo");
    buttonInstance.addClass("fa-camera");
    buttonInstance.addClass("fa");

    player.controlBar.addChild(buttonInstance);
    if(document.getElementById('upDwVOfAFZUh')){
        if(ads.length > 0){
            var options = {
                id: 'my-video',
                adTagUrl: nextAds()
            };
            player.ima(options);

            var currentTime = parseInt(startTime);

            setTimeout(function(){
                player.ima.requestAds();
                if(device == 'mobile') {
                    player.ima.initializeAdDisplayContainer();
                }

            },currentTime+'000');
        }
    }

    player.ready(function(){
       player.pause();
       jQuery('.vjs-poster').hide();
       jQuery('.vjs-big-play-button').show();
       jQuery('.vjs-big-play-button').on('click', function () {
           jQuery(this).hide();
           player.play();
       });
    });

    player.on('adserror', (function(err) {
        console.log(err);
        console.log(' ERROR, adname: ' + companyName);
        //sendStatistic('error', companyName);
        //document.getElementById('ima-ad-container').style.visibility = 'hidden';
        if(device == 'mobile'){
            var restTime = 50;
        }else{
            var restTime = 5;
        }
        reloadAds(restTime);
        player.play();
    }));

    player.on("adsready", function(){
        player.ima.addEventListener(google.ima.AdEvent.Type.STARTED, function(ad){
            //var adname = player.ad.w.g.adSystem;
            console.log(' STARTED, adname: ' + companyName);

            /*IE FIX START*/
            let video_height = $('.video').height();
            $('.ima-ad-container video').css('height', video_height+'px');
            /*IE FIX END*/

            $('.ima-container').hide();
            //sendStatistic('load', companyName);

            //check tab and mute
            if(muteTab == 'on'){
                $(window).on("blur focus", function(e) {
                    var prevType = $(this).data("prevType");

                    if (prevType != e.type) {   //  reduce double fire issues
                        switch (e.type) {
                            case "blur":
                                if($('#my-video_ima-mute-div').hasClass('ima-non-muted')){
                                    $('#my-video_ima-mute-div').trigger('click');
                                }
                                break;
                            case "focus":
                                if($('#my-video_ima-mute-div').hasClass('ima-muted')){
                                    $('#my-video_ima-mute-div').trigger('click');
                                }
                                break;
                        }
                    }

                    $(this).data("prevType", e.type);
                })
            }


        });
        player.ima.addEventListener(google.ima.AdEvent.Type.COMPLETE, function(ad){
            //var adname = ad.w.g.adSystem;
            console.log(' COMPLETE ' + companyName);
            reloadAds(intervalTime);
            player.play();
            $('.ima-container').show();
            //sendStatistic('complete', companyName);
        });
        player.ima.addEventListener(google.ima.AdEvent.Type.SKIPPED, function(ad){
            //var adname = ad.w.g.adSystem;
            console.log(' SKIPPED ' + companyName);
            reloadAds(intervalTime);
            player.play();
            $('.ima-container').show();
            //sendStatistic('skipped', companyName);
        });
        player.ima.addEventListener(google.ima.AdEvent.Type.IMPRESSION, function(ad){
            console.log(' IMPRESSION ' + ad);
        });
        player.ima.addEventListener(google.ima.AdEvent.Type.CLICK, function(ad){
            //var adname = ad.w.g.adSystem;
            console.log(' CLICK ' + companyName);
            //sendStatistic('click', companyName);
        });
        player.ima.addEventListener(google.ima.AdEvent.Type.AD_METADATA, function(ad){
            console.log(' METADATA ' + ad);
        });

    });
    player.on('adscanceled' , function (e) {
        console.log(e);
        console.log('canceled');
    });
    console.log('video options ready');
}


function GenerateModal(dataURI) {
    var modal = $("<div id='snapitmodal' class='modal'><div class='modal-dialog' role='document'><div class='modal-content'><div class='modal-body'>" +
        "<div style='text-align: center'><a class='download-screenshot'>Download</a></div>" +
        "</div></div></div></div>");
    var img = $("<img style='width: 100%' />");
    img.attr("src", dataURI);
    modal.find(".modal-body").prepend(img);
    modal.find(".download-screenshot").click(function () {
        this.href = dataURI;
        this.download = "screenshot.jpg";
    });
    $("body").append(modal);
    modal.modal();
}

function  reloadAds(time) {
    var currentTime = parseInt(time);
    console.log("Load next ADS after " + currentTime);
    player.ima.settings.adTagUrl = nextAds();
    setTimeout(function(){
        console.log('Loading new ads: ' + companyName);
        player.ima.requestAds();

    }, currentTime+'000');
}

/* SCRIPT FOR ADS START*/
function nextAds() {
    var ads_length = ads.length,
         count = 0,
         name;
    ads.some(function(item) {
        count++;
        name = item.name;
        companyName = item.company;
        if(item.played === 0){
            item.played = 1;
            return true;
        }
        if(count === ads_length){
            ads.some(function(item) {
                item.played = 0;
                count = 0;
            });
        }
    });
    return name;
}
/* SCRIPT FOR ADS END*/
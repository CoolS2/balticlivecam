<?php
/*  Template Name: Test3 */

?>
    <!DOCTYPE html>
    <html <?php language_attributes(); ?>>
<head>
    <link href='https://fonts.googleapis.com/css?family=EB+Garamond' rel='stylesheet' type='text/css'>
    <link href='https://fonts.googleapis.com/css?family=Open+Sans&subset=latin,cyrillic' rel='stylesheet' type='text/css'>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.3/jquery.min.js"></script>
    <script type="text/javascript" src="//code.jquery.com/jquery-migrate-1.2.1.min.js"></script>
    <meta charset="<?php bloginfo( 'charset' ); ?>">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="profile" href="http://gmpg.org/xfn/11">
    <link href="<?php echo get_template_directory_uri() ?>/ads-test/videojs/plugins/videojs-ima/videojs.ima.css" rel="stylesheet">
    <link href="<?php echo get_template_directory_uri() ?>/ads-test/videojs/video-js.min.css" rel="stylesheet">
    <link href="<?php echo get_template_directory_uri() ?>/ads-test/videojs/blc-js.css" rel="stylesheet">
    <link href="<?php echo get_template_directory_uri() ?>/ads-test/videojs/plugins/videojs-resolution-switcher/videojs-resolution-switcher.css" rel="stylesheet">
    <link href="<?php echo get_template_directory_uri() ?>/ads-test/videojs/plugins/videojs-contrib-ads/videojs-contrib-ads.css" rel="stylesheet">

</head>
    <body>

        <div class="video">
            <video id="my-video" class="video-js vjs-16-9 vjs-fluid blc-js" controls ></video>
        </div>
        <div id="snapitmodal"></div>


<script src="<?php echo get_template_directory_uri() ?>/ads-test/videojs/video.min.js"></script>
<script src="<?php echo get_template_directory_uri() ?>/ads-test/videojs/plugins/videojs-contrib-hls/videojs-contrib-hls.min.js"></script>
<script src="<?php echo get_template_directory_uri() ?>/ads-test/videojs/plugins/videojs-resolution-switcher/videojs-resolution-switcher.js"></script>
<script src="//imasdk.googleapis.com/js/sdkloader/ima3.js"></script>
<script src="<?php echo get_template_directory_uri() ?>/ads-test/videojs/plugins/videojs-contrib-ads/videojs-contrib-ads.min.js"></script>
<script src="<?php echo get_template_directory_uri() ?>/ads-test/videojs/plugins/videojs-ima/videojs.ima.js"></script>
<script src="<?php echo get_template_directory_uri() ?>/ads-test/videojs/video-options.js"></script>
    <script>
        var player;

        player = videojs('my-video', {
            controls: true,
            controlBar: {
                volumePanel: {inline: false}
            },
            plugins: {
                videoJsResolutionSwitcher: {
                    default: 'low'
                }
            },
            autoplay: true,
            preload: 'auto',
            sourceOrder: true,
            customControlsOnMobile: true,
            //nativeControlsForTouch: true,
            /*sources: [{
                src: 'http://animalslife.net/videos_for/Valga_(Estonia)/anim_Valga_1506505495_13147Trim_Part2.mp4',
                type: 'video/mp4'
            }],*/
            hls: {
                withCredentials: true
            },
            /*sources: [{
                src: 'https://edge01.balticlivecam.com/blc/RadissonDaugava/index.m3u8?token=0b13cbc36270d970f35a662d015a751a:1508941125353',
                type: 'application/x-mpegurl'
            }],*/
            techOrder: ['html5']
        }, function(){

            // Add dynamically sources via updateSrc method
            player.updateSrc([
                {
                    src: 'https://edge01.balticlivecam.com/blc/PanoTallinn/index.m3u8?token=1b93489f3d6154a8e916e7a60179ea79:1509459902319',
                    type: 'application/x-mpegurl',
                    label: '360'
                },
                {
                    src: 'http://edge01.balticlivecam.com/blc/biteVilnius/index.m3u8?token=nz7cDTmZvB3bSeMgCVL2Jp49',
                    type: 'application/x-mpegurl',
                    label: '720'
                }
            ]);
            player.currentResolution('360');
        });


       var ads = [/*{
               name : 'https://video-adserver.ibillboard.com/getAd?tagid=908c4ff6-93af-46a8-9fda-8e82ece4b56a',
               played : 0
           },
           {
               name : 'https://ssp.lkqd.net/ad?pid=365&sid=321392&output=vastvpaid&support=html5flash&execution=any&placement=&playinit=auto&volume=100&width=1920&height=1080&pageurl=<?=get_permalink()?>&contentid=1&contenttitle=Test&contenturl=<?=get_permalink()?>&rnd=<?=time()?>',
               played : 0
           },*/
           {
               name : 'http://adx.adform.net/adx/?mid=504716&t=2',
               played : 0
           }
           /*{
                name : 'https://googleads.g.doubleclick.net/pagead/ads?client=ca-video-pub-2393320645055022&slotname=1989239344/7043194055&ad_type=video&description_url=http%3A%2F%2Fbalticlivecam.com&videoad_start_delay=0',
                played : 0
            },
            {
                name : 'https://ads.adfox.ru/232367/getCode?p1=bwyon&p2=fayf&puid1=&puid2=&puid6=&puid8=&puid11=&puid22=&puid27=&puid33=&puid51=&puid52=&puid55=',
                played : 0
            },
           {
               name : '//b.adbox.lv/emiter/bx_vast.js?id=vid_mid&consumer=29635889ea291cd44&lang=lv',
               played : 0
           }*/];

        videoOptions(ads);
    </script>

        <script data-adfscript="adx.adform.net/adx/?mid=504719&rnd=33"></script>
        <script src="//s1.adform.net/banners/scripts/adx.js" async defer></script>
    </body>
    </html>
<?php

<?php
/*  Template Name: Test1 */
?>
<!DOCTYPE html>
<html <?php language_attributes(); ?>>
    <head>
        <link href='https://fonts.googleapis.com/css?family=EB+Garamond' rel='stylesheet' type='text/css'>
        <link href='https://fonts.googleapis.com/css?family=Open+Sans&subset=latin,cyrillic' rel='stylesheet' type='text/css'>
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.3/jquery.min.js"></script>
        <script type="text/javascript" src="//code.jquery.com/jquery-migrate-1.2.1.min.js"></script>

        <link rel="stylesheet" href="/flowplayer/skin/skin.css">
        <link href="/flowplayer/ad_clean.css" rel="stylesheet">
        <script src="/flowplayer/flowplayer.js"></script>
        <script src="/flowplayer/flowplayer.hlsjs.min.js"></script>
        <script src="//s0.2mdn.net/instream/html5/ima3.js"></script>
        <script src="<?php echo get_template_directory_uri() ?>/js/flowplayer_config.js"></script>
        <script src="/flowplayer/vast.min.js"></script>

        <meta charset="<?php bloginfo( 'charset' ); ?>">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <link rel="profile" href="http://gmpg.org/xfn/11">
    </head>
    <body>
    <h3>MID ROLL - SETUPAD</h3>
        <div id="player_load">
        </div>
        <div id="live" class="flowplayer">
        </div>
        <script>
            var container = document.getElementById("live"),
                timer, player;
            player = flowplayer(container,{
                live: true,
                key: "$153760428632348",
                autoplay:true,
                fullscreen:true,
                native_fullscreen:true,
                ima: {
                    start: 30,
                    period: 400000,
                    VpaidMode: 1,
                    ads: [{
                        time: 30,
                        adTag: "https://googleads.g.doubleclick.net/pagead/ads?client=ca-video-pub-7383171830614216&slotname=5700928522&ad_type=video_text_image&description_url=https%3A%2F%2Fbalticlivecam.com%2F&max_ad_duration=60000&sdmax=30000&videoad_start_delay=30000"
                    }]
                },
                clip: {
                    sources: [
                        {
                            type: "video/mp4",
                            src: '<?php echo get_template_directory_uri() ?>/ads-test/mov_bbb.mp4'
                        }
                    ]
                }

            });
// PREROLL
        </script>
    </body>
    </html>
<?php

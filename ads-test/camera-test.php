<?php
/*  Template Name: Camera test */
?>
    <!DOCTYPE html>
    <html <?php language_attributes(); ?>>
    <head>
        <link href='https://fonts.googleapis.com/css?family=EB+Garamond' rel='stylesheet' type='text/css'>
        <link href='https://fonts.googleapis.com/css?family=Open+Sans&subset=latin,cyrillic' rel='stylesheet' type='text/css'>
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.3/jquery.min.js"></script>
        <script type="text/javascript" src="//code.jquery.com/jquery-migrate-1.2.1.min.js"></script>

        <link rel="stylesheet" href="/flowplayer/skin/functional.css">
        <link href="/flowplayer/ad_clean.css" rel="stylesheet">
        <script src="/flowplayer/flowplayer.js"></script>
        <script src="/flowplayer/flowplayer.hlsjs.min.js"></script>

        <script src="<?php echo get_template_directory_uri() ?>/js/flowplayer_config.js"></script>

        <meta charset="<?php bloginfo( 'charset' ); ?>">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <link rel="profile" href="http://gmpg.org/xfn/11">
    </head>
    <body>
    <h3>Test cameras</h3>
    <div id="player_load">
    </div>
    <div id="live" class="flowplayer">
    </div>

    <script>
        var container = document.getElementById("live"),
            timer, player;

        player = flowplayer(container, {
            live: true,
            key: "$153760428632348",
            autoplay: true,
            fullscreen: true,
            native_fullscreen: true,
            clip: {
                hlsjs: {
                    // listen to hls.js ERROR
                    listeners: ["hlsError"],
                    // limit amount of hls level loading retries
                    levelLoadingMaxRetry: 2
                },
                flashls: {
                    // limit amount of retries to load hls manifests in Flash
                    manifestloadmaxretry: 2
                },
                sources: [
                    {
                        type: "application/x-mpegurl",
                        src: "https://edge01.balticlivecam.com/blc/test2/index.m3u8/?token=nS24oKq5NU8LPygkpc3ieXWvFQJVmMTu"
                    }
                ]
            }
        });
    </script>
    </body>
    </html>
<?php

<?php /* Template Name: Contacts */ ?>
<?php
get_header();
?>

    <div class="site">
        <?php get_template_part("partials/nav"); ?>
    <div class="container">
        <div class="content content-contacts">
            <div class="contact-form">
                <?php echo do_shortcode( __('[contact-form-7 id="63" title="Contact form"]','blc') ); ?>
            </div>
        </div>
            <?php
            the_content();
            ?>
    </div>

        <?php get_template_part("partials/main/footer"); ?>
    </div>


<?
get_footer();
